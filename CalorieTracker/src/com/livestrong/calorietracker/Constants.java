package com.livestrong.calorietracker;

public class Constants {

	/** Application name */
	public static final String APPLICATION_NAME = "My Plate";
	public static final double APPLICATION_VERSION = 1.1;
    public static final boolean DEBUG_MODE = false;

    /** Use this as a field name in your Model if you want to be able to get rowId*/
	public static final String ROW_ID_COLUMN = "rowId";
	
	/**log title for back*/
	public static final String LOG_MYPLATE = "BACK_LOG";
	
	/** Food image size keys */
	public static final Integer FOOD_IMAGE_SMALL 	= 60;
	public static final Integer FOOD_IMAGE_MEDIUM 	= 100;
	public static final Integer FOOD_IMAGE_LARGE 	= 190;
	
	/** Avatar image size keys */
	public static final String AVATAR_IMAGE_SMALL 	= "small";
	public static final String AVATAR_IMAGE_MEDIUM 	= "medium";
	public static final String AVATAR_IMAGE_LARGE 	= "large";	
	
}
