package com.livestrong.calorietracker.configuration;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

public class MyPlateDefaults {
    public static class ActivityLevels{

        public static float getDefault() {

            return 1.2f;
        }

        public static HashMap<Float, String> getLevels() {
            // Default values, in case we can't get them from the server on first start
            HashMap<Float, String> levels;

            levels = new HashMap<Float, String>();
            levels.put(1.2f, "Sedentary");
            levels.put(1.375f, "Light Activity");
            levels.put(1.55f, "Moderate Activity");
            levels.put(1.725f, "Very Active");
            return levels;
        }
    }

	public final static String PREFS_NAME = "LiveStrong";
	public final static String PREFS_LAST_ONLOAD_SYNC = "onloadSync";
	public final static String PREFS_LAST_SYNC_TOKEN = "lastSyncToken";
	public final static String PREFS_USERNAME = "username";
	public final static String PREFS_PASSWORD = "password";
	public final static String PREFS_ACCESS_TOKEN = "accessToken";
	public final static String PREFS_REFRESH_TOKEN = "refreshToken";
	public final static String PREFS_LAST_SYNC = "lastSync";
	
	// User configurable Preferences
	public final static String PREFS_WEIGHT_UNITS = "weightUnits";
	public final static String PREFS_DISTANCE_UNITS = "distanceUnits";
	public final static String PREFS_WATER_UNITS = "waterUnits";
	public final static String PREFS_DAILY_REMINDER = "dailyReminder";
	public final static String PREFS_DAILY_REMINDER_TIME = "dailyReminderTime";
	
	// UI Saved state Preferences
	public final static String PREFS_SELECTED_TAB = "selectedTab";
	public final static String PREFS_PROGRESS_BAR_WIDTH = "progressBarWidth";
	public final static String PREFS_DIARY_SELECTED_TAB = "diarySelectedTab";
	public final static String PREFS_DIARY_SELECTED_DATE = "diarySelectedDATE";
	public final static String PREFS_PROGRESS_SELECTED_TAB = "progressSelectedTab";
	public final static String PREFS_COMMUNITY_SELECTED_TAB = "communitySelectedTab";
	public final static String PREFS_MORE_SELECTED_TAB = "moreSelectedTab";

	public static enum WeightUnits {KILOGRAMS, POUNDS, STONES};
	public static enum DistanceUnits {METERS, MILES};
	public static enum WaterUnits {MILLILITERS, OUNCES};

    public final static double KG_PER_POUND = 0.45359237;
    public final static double POUND_PER_STONES = 14;
	private static Context context;
	
	public static void initialize(Context ctx) {
        MyPlateDefaults.context = ctx;
	}
	
	public static String getPref(String name, String defaultValue) {
		return ((String) getPref(name, (Object) defaultValue));
	}

	public static long getPref(String name, long defaultValue) {
		return ((Long) getPref(name, (Object) defaultValue)).longValue();
	}

	public static int getPref(String name, int defaultValue) {
		return ((Integer) getPref(name, (Object) defaultValue)).intValue();
	}

	public static float getPref(String name, float defaultValue) {
		return ((Float) getPref(name, defaultValue)).intValue();
	}

	public static boolean getPref(String name, boolean defaultValue) {
		return ((Boolean) getPref(name, (Object) (Object) defaultValue)).booleanValue();
	}

	public static Object getPref(String name, Object defaultValue) {
		SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		Object value = settings.getAll().get(name);
		if (value == null) {
			value = defaultValue;
		}
		return value;
	}
    public static void clearPrefs(){
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.commit();
    }

//	public static UnitsPreference getPref(String name, UnitsPreference defaultValue) {
//		return UnitsPreference.valueOf(getPref(name, defaultValue.toString()));
//	}

	public static void setPref(String name, Object value) {
		SharedPreferences settings = MyPlateDefaults.context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		if (value == null) {
			settings.edit().remove(name).commit();
		} else if (value instanceof Long) {
			settings.edit().putLong(name, (Long) value).commit();
		} else if (value instanceof String) {
			settings.edit().putString(name, (String) value).commit();
		} else if (value instanceof Integer) {
			settings.edit().putInt(name, (Integer) value).commit();
		} else if (value instanceof Float) {
			settings.edit().putFloat(name, (Float) value).commit();
		} else if (value instanceof Boolean) {
			settings.edit().putBoolean(name, (Boolean) value).commit();
		}
	}
}
