package com.livestrong.calorietracker.fragments;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;

import com.livestrong.calorietracker.R;
import com.livestrong.calorietracker.activities.CalorieTrackerBaseFragmentActivity;
import com.livestrong.calorietracker.activities.TabBarActivity;
import com.livestrong.calorietracker.back.db.ModelManager;
import com.livestrong.calorietracker.back.db.gen.Profile;
import com.livestrong.calorietracker.fragments.More.MoreAboutFragment;
import com.livestrong.calorietracker.fragments.More.MoreAccountFragment;
import com.livestrong.calorietracker.fragments.More.MoreProfileFragment;
import com.livestrong.calorietracker.fragments.More.MoreSupportFragment;


public class MoreFragment extends CalorieTrackerBaseFragmentActivity {

	LinearLayout mView;

    Fragment mCurrentFragment;

	private class UserProfileTask extends AsyncTask<Void, Void, Profile>
	{
		
		@Override
		protected Profile doInBackground(Void... params)
		{
			return ModelManager.sharedModelManager().getUserProfile();
		}
		
		protected void onPostExecute(Profile profile)
		{
			if (profile != null) 
			{
                if(mCurrentFragment instanceof MoreAccountFragment){
                    ((MoreAccountFragment)mCurrentFragment).saveProfile();
                }
                else if(mCurrentFragment instanceof MoreProfileFragment)
                    ((MoreProfileFragment)mCurrentFragment).saveProfile();
			}
		};

	};

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		//Log.d("MoreFragment", "MoreFragment - onCreate");
		// Hook up outlets
        setContentView(R.layout.fragment_more);

        this.mView 			= (LinearLayout) findViewById(R.layout.fragment_more);

        Bundle extras = getIntent().getExtras();
		int fragmentType = (Integer)extras.get(Fragment.class.getName());
		switch (fragmentType){
            case R.id.action_settings: displayFragment(new MoreAccountFragment());break;
            case R.id.action_profile: displayFragment(new MoreProfileFragment());break;
            case R.id.action_support: displayFragment(new MoreSupportFragment());break;
            case R.id.action_about: displayFragment(new MoreAboutFragment());break;
        }

        getActionBar().setDisplayHomeAsUpEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getActionBar().setHomeButtonEnabled(true);
        }
    }

    // Initialize menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            Intent intent = new Intent(this, TabBarActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);

    }
	
	@Override
	public void onPause() {
		new UserProfileTask().execute(new Void[]{});
		
		super.onPause();
	}
	
	@Override
	public void onResume() {
		//Log.d("MoreFragment", "TabBarActivity - MoreFragment - onResume");
		super.onResume();
	}
	
	@Override
	public void onDestroy() {
		//Log.d("MoreFragment", "TabBarActivity - MoreFragment - onDestroy");
		super.onDestroy();
	}
	
//	@Override
//	public void onDetach() {
//		//Log.d("MoreFragment", "TabBarActivity - MoreFragment - onDetach");
//		super.onDetach();
//	}

	protected void displayFragment(Fragment fragment){
		int index = 0;
		Boolean toTheLeft = true;
        mCurrentFragment = fragment;
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.frameLayout, fragment);
        fragmentTransaction.commit();
	}
	

}