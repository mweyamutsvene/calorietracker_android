package com.livestrong.calorietracker.fragments;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.livestrong.calorietracker.R;
import com.livestrong.calorietracker.activities.CommunityCommentsActivity;
import com.livestrong.calorietracker.adapters.CommunityListAdapter;
import com.livestrong.calorietracker.back.api.CTWebserviceRequest;
import com.livestrong.calorietracker.back.api.CTWebserviceRequest.*;

import com.livestrong.calorietracker.back.db.gen.CommunityMessage;
import com.livestrong.calorietracker.configuration.MyPlateDefaults;
import com.livestrong.calorietracker.utilities.PullToRefreshListView;

public class CommunityFragment extends Fragment implements OnItemClickListener, CTWebserviceRequestDelegate, ActionBar.TabListener {

	private ListView communityListView;
	private CommunityListAdapter communityListAdapter;
	private View view;
	private Button sendButton;
	private ImageButton composeMessageButton;
	private RelativeLayout messageContainer;
	private int messageContainerHeight;
	private ProgressBar progressBar;
	private EditText messageEditText;
    int mSelectedTab;
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (container == null) {
			// We have different layouts, and in one of them this
			// fragment's containing frame doesn't exist. The fragment
			// may still be created from its saved state, but there is
			// no reason to try to create its view hierarchy because it
			// won't be displayed. Note this is not needed -- we could
			// just run the code below, where we would create and return
			// the view hierarchy; it would just never be used.
			return null;
		}
		this.view = (LinearLayout) inflater.inflate(R.layout.fragment_community, container, false);
		
		// Hook up UI outlets
		this.messageContainer = (RelativeLayout) this.view.findViewById(R.id.messageContainer);
		this.progressBar = (ProgressBar) this.view.findViewById(R.id.progressBar);
		this.messageEditText = (EditText) this.view.findViewById(R.id.messageEditText);
		
		// Initialize views
		this.messageContainerHeight = dpToPixels(57);
		this.progressBar.setVisibility(View.INVISIBLE);
		
		this.communityListView = (ListView) view.findViewById(R.id.communityListView);
		if (this.communityListAdapter == null){
			this.communityListAdapter = new CommunityListAdapter(getActivity(), this.communityListView);	
		} else {
			this.communityListAdapter.setListView(this.communityListView);
		}
		this.communityListView.setAdapter(this.communityListAdapter);
		this.communityListView.setOnItemClickListener(this);

		((PullToRefreshListView) this.communityListView).setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
			@Override
			public void onRefresh() {
                if (CTWebserviceRequest.isLoggedIn()){
                    if (mSelectedTab == 0){
                        communityListAdapter.reloadCommunityMessages();
                    } else {
                            communityListAdapter.reloadUserMessages();

                    }
                }
                else {
                    ((PullToRefreshListView) communityListView).onRefreshComplete();
                }

			}
		});
				
		this.initializeButtons();



		return view;
	}

    @Override
    public void onResume() {
        super.onResume();
        this.getActivity().getActionBar().setTitle("Community");
        ActionBar bar = this.getActivity().getActionBar();
        bar.removeAllTabs();
        bar.addTab(bar.newTab().setText(R.string.community).setTabListener(this));
        bar.addTab(bar.newTab().setText(R.string.myMessages).setTabListener(this));
        bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        this.setHasOptionsMenu(true);

        Integer selectedTab = (Integer) MyPlateDefaults.getPref(MyPlateDefaults.PREFS_COMMUNITY_SELECTED_TAB, 0);
        bar.selectTab(bar.getTabAt(selectedTab));

    }


	private void initializeButtons() {

        //Send button sends message to community
		this.sendButton = (Button) this.view.findViewById(R.id.sendButton);
		this.sendButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (CTWebserviceRequest.isLoggedIn()){
					String message = CommunityFragment.this.messageEditText.getText().toString();
					if (message.length() > 0){
						progressBar.setVisibility(View.VISIBLE);
						sendButton.setVisibility(View.INVISIBLE);
						messageEditText.setEnabled(false);
						hideKeyboard();

                        CTWebserviceRequest request = CTWebserviceRequest.postMessage(message);
                        request.delegate = CommunityFragment.this;
					}
				} else {
					new AlertDialog.Builder(getActivity())
				      .setMessage("You must be signed in to post messages.")
				      .setNeutralButton(android.R.string.ok,
				         new DialogInterface.OnClickListener() {
				         public void onClick(DialogInterface dialog, int whichButton){}
				         })
				      .show();
				}
			}
		});
	}



	private void loadCommunityMessages() {
		this.communityListAdapter.loadCommunityMessages(1);
		MyPlateDefaults.setPref(MyPlateDefaults.PREFS_COMMUNITY_SELECTED_TAB, 0);
	}

	private void loadUserMessages() {
		if (CTWebserviceRequest.isLoggedIn() == false){
			new AlertDialog.Builder(getActivity())
		      .setMessage("Viewing your messages requires you to be logged in to your LIVESTRONG account")
		      .setTitle("")
		      .setNeutralButton(android.R.string.ok,
		         new DialogInterface.OnClickListener() {
		         public void onClick(DialogInterface dialog, int whichButton){}
		         })
		      .show();
			return;
		}
		
		this.communityListAdapter.loadUserMessages(1);
		MyPlateDefaults.setPref(MyPlateDefaults.PREFS_COMMUNITY_SELECTED_TAB, 1);
	}
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
		// We subtrack one here because of the PullToRefresh cell that increments the size list by 1
		CommunityMessage message = this.communityListAdapter.getItem(position - 1);
		if (message == null){
			return;
		}
		
		Intent intent = new Intent(getActivity(), CommunityCommentsActivity.class);
		intent.putExtra(message.getClass().getName(), message);
		startActivity(intent);
	}
	

	

	
	private int dpToPixels(int dp){
		Resources r = getResources();
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
	}
	
	private void hideKeyboard(){
		// Hide Keyboard
		InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(this.messageEditText.getWindowToken(), 0);
	}

    @Override
    public void dataReceived(CTWebserviceRequest request, CTWebserviceRequestType methodCalled, Object data) {
        this.progressBar.setVisibility(View.INVISIBLE);
        sendButton.setVisibility(View.VISIBLE);
        this.messageEditText.setEnabled(true);
        this.messageEditText.setText("");

        if (mSelectedTab == 0){
            this.communityListAdapter.reloadCommunityMessages();
        } else{
            this.communityListAdapter.reloadUserMessages();
        }
    }

    @Override
    public boolean errorOccurred(CTWebserviceRequest request, CTWebserviceRequestType methodCalled, Exception error, String errorMessage) {
        return false;
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        this.mSelectedTab = tab.getPosition();
        switch(mSelectedTab){
            case 0:loadCommunityMessages();break;
            case 1:loadUserMessages();break;
        }
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }
}
