package com.livestrong.calorietracker.fragments;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.livestrong.calorietracker.MyPlateApplication;
import com.livestrong.calorietracker.R;
import com.livestrong.calorietracker.activities.DiarySelectorActivities.AddExerciseActivity;
import com.livestrong.calorietracker.activities.DiarySelectorActivities.AddFoodActivity;
import com.livestrong.calorietracker.activities.DiarySelectorActivities.AddWaterActivity;
import com.livestrong.calorietracker.activities.DiarySelectorActivities.AddWeightActivity;
import com.livestrong.calorietracker.activities.TrackActivity;
import com.livestrong.calorietracker.adapters.DiaryAdapter;
import com.livestrong.calorietracker.back.db.gen.DiaryEntry;
import com.livestrong.calorietracker.utilities.PinnedHeaderListView;
import com.livestrong.calorietracker.utilities.Utils;


public class DiaryListFragment extends Fragment implements OnItemClickListener {
	
	private static final SimpleDateFormat dateFormatter = new SimpleDateFormat("d MMMM, yyyy");
	
	private PinnedHeaderListView listView;
	private DiaryAdapter adapter;
	private ImageButton backBtn, forwardBtn;
	private Button trackNowButton;
	protected Date selectedDate;
	private TextView dateTextView;
	private LinearLayout messageContainer;  
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (container == null) {
	        // We have different layouts, and in one of them this
	        // fragment's containing frame doesn't exist.  The fragment
	        // may still be created from its saved state, but there is
	        // no reason to try to create its view hierarchy because it
	        // won't be displayed.  Note this is not needed -- we could
	        // just run the code below, where we would create and return
	        // the view hierarchy; it would just never be used.
	        return null;
	    }
		
		View view = (LinearLayout)inflater.inflate(R.layout.fragment_diary_list, container, false);
    
		// Hooking up outlets
		this.backBtn 		= (ImageButton) view.findViewById(R.id.backButton);
		this.forwardBtn 	= (ImageButton) view.findViewById(R.id.forwardButton);
		this.trackNowButton = (Button) view.findViewById(R.id.trackNowButton);
		this.dateTextView 	= (TextView) view.findViewById(R.id.dateTextView);
		this.messageContainer = (LinearLayout) view.findViewById(R.id.messageContainer);
		
		// Initialize ListView
		this.listView = (PinnedHeaderListView)view.findViewById(android.R.id.list);
	    this.listView.setPinnedHeaderView(LayoutInflater.from(getActivity()).inflate(R.layout.list_item_diary_header, listView, false));
	    this.listView.setDividerHeight(0);
	  
		this.adapter = new DiaryAdapter(getActivity());
	    this.listView.setAdapter(adapter);
	    this.listView.setOnScrollListener(adapter);
	    this.listView.setOnItemClickListener(this);

		// Initialize date & datelabel
		if (this.selectedDate == null){
			this.setSelectedDate(new Date());
		}
		this.refreshDateLabel();
	    
	    this.initializeButtons();
        setHasOptionsMenu(true);


        return view;

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item){

    // Handle action buttons
        switch(item.getItemId()) {
            case R.id.action_profile:{
                Intent intent = new Intent(this.getActivity(), MoreFragment.class);
                intent.putExtra(Fragment.class.getName(),R.id.action_profile);
                startActivity(intent);
                return true;
            }
            case R.id.action_about:{
                Intent intent = new Intent(this.getActivity(), MoreFragment.class);
                intent.putExtra(Fragment.class.getName(),R.id.action_about);
                startActivity(intent);
                return true;
            }
            case R.id.action_settings:{
                Intent intent = new Intent(this.getActivity(), MoreFragment.class);
                intent.putExtra(Fragment.class.getName(),R.id.action_settings);
                startActivity(intent);
                return true;
            }
            case R.id.action_support:{
                Intent intent = new Intent(this.getActivity(), MoreFragment.class);
                intent.putExtra(Fragment.class.getName(),R.id.action_support);
                startActivity(intent);
                return true;
            }
            case R.id.action_track:{
                Intent intent = new Intent(getActivity(), TrackActivity.class);
                intent.putExtra(Date.class.getName(),selectedDate);
                startActivity(intent);
                return true;
            }
        }
        return  super.onOptionsItemSelected(item);

    }
	
	@Override
	public void onResume() {
		super.onResume();

        if (this.adapter != null && this.selectedDate != null){
			this.adapter.setDate(this.selectedDate);			
		}	
		
		// Show or hide Track Now button 
		if (this.messageContainer != null){
			if (this.adapter != null && this.adapter.getCount() == 0){
				this.messageContainer.setVisibility(View.VISIBLE);
			} else {
				this.messageContainer.setVisibility(View.INVISIBLE);
			}	
		}
	}
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		// Diary entry item selected
		DiaryEntry clickedEntry = (DiaryEntry) this.adapter.getItem(position);
        if(clickedEntry == null){
            return;
        }

		if (DiaryEntry.DiaryEntryType.values()[clickedEntry.getDiaryType()] == DiaryEntry.DiaryEntryType.DIARY_FOOD_ENTRY) {
			Intent intent = new Intent(MyPlateApplication.getContext(), AddFoodActivity.class);
			intent.putExtra(DiaryEntry.class.getName(), clickedEntry);
			startActivity(intent);
		}
		else if (DiaryEntry.DiaryEntryType.values()[clickedEntry.getDiaryType()] == DiaryEntry.DiaryEntryType.DIARY_EXERCISE_ENTRY) {
			Intent intent = new Intent(MyPlateApplication.getContext(), AddExerciseActivity.class);
			intent.putExtra(DiaryEntry.class.getName(), clickedEntry);
			startActivity(intent);
		}
        else if (DiaryEntry.DiaryEntryType.values()[clickedEntry.getDiaryType()] == DiaryEntry.DiaryEntryType.DIARY_WATER_ENTRY) {
			Intent intent = new Intent(MyPlateApplication.getContext(), AddWaterActivity.class);
			intent.putExtra(DiaryEntry.class.getName(), clickedEntry);
			startActivity(intent);
		}
        else if (DiaryEntry.DiaryEntryType.values()[clickedEntry.getDiaryType()] == DiaryEntry.DiaryEntryType.DIARY_WEIGHT_ENTRY) {
			Intent intent = new Intent(MyPlateApplication.getContext(), AddWeightActivity.class);
			intent.putExtra(DiaryEntry.class.getName(), clickedEntry);
			startActivity(intent);
		}

	}
	
	private void initializeButtons(){
		OnClickListener onClickListener = new OnClickListener() {
			@Override
			public void onClick(View button) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(selectedDate);

				int btnId = button.getId();
				switch (btnId) {
					case R.id.backButton:
						calendar.add(Calendar.DATE, -1);  // subtract one day
						break;
					case R.id.forwardButton:
						if (!Utils.isToday(selectedDate)){
							calendar.add(Calendar.DATE, 1);  // add one day
						}
						break;
				}

				setSelectedDate(calendar.getTime());
			}
		};

		this.backBtn.setOnClickListener(onClickListener);
		this.forwardBtn.setOnClickListener(onClickListener);

        //Empty screen shows the track button
		this.trackNowButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
                Intent intent = new Intent(getActivity(), TrackActivity.class);
                intent.putExtra(Date.class.getName(),selectedDate);
                startActivity(intent);
			}
		});
	}
	
	protected void setSelectedDate(Date date){
		this.selectedDate = date;
		this.refreshDateLabel();
		this.adapter.setDate(this.selectedDate);
		this.listView.setSelectionAfterHeaderView();
		if (this.adapter.getCount() == 0){
			this.messageContainer.setVisibility(View.VISIBLE);
		} else {
			this.messageContainer.setVisibility(View.INVISIBLE);
		}
	}
	
	protected Date getSelectedDate(){
		return this.selectedDate;
	}
	
	private void refreshDateLabel(){
		if (dateTextView == null){
			return;
		}
        this.forwardBtn.setVisibility(View.VISIBLE);
        if (Utils.isToday(this.selectedDate)) {
			this.dateTextView.setText(R.string.today);
            this.forwardBtn.setVisibility(View.INVISIBLE);

        }
        else if (Utils.isYesterday(this.selectedDate)) {
			this.dateTextView.setText(R.string.yesterday);
		}
        else {
			this.dateTextView.setText(dateFormatter.format(this.selectedDate));
		}
	}
}
