package com.livestrong.calorietracker.fragments;

import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.*;

import com.livestrong.calorietracker.MyPlateApplication;
import com.livestrong.calorietracker.R;
import com.livestrong.calorietracker.activities.DiarySelectorActivities.AddWaterActivity;
import com.livestrong.calorietracker.activities.DiarySelectorActivities.ExerciseSelectorActivity;
import com.livestrong.calorietracker.activities.DiarySelectorActivities.FoodSelectorActivity;
import com.livestrong.calorietracker.animations.WidthAnimation;
import com.livestrong.calorietracker.back.db.ModelManager;
import com.livestrong.calorietracker.back.db.gen.DiaryEntry;
import com.livestrong.calorietracker.back.db.gen.DiaryEntry.DiaryEntryCategoryEnum;

import com.livestrong.calorietracker.back.db.gen.Profile;
import com.livestrong.calorietracker.configuration.MyPlateDefaults;


public class MyPlateFragment extends Fragment implements ModelManager.ModelManagerDelegate {
	private final static Class<?> NEXT_ACTIVITY_FOOD     = FoodSelectorActivity.class;
    private final static Class<?> NEXT_ACTIVITY_EXERCISE = ExerciseSelectorActivity.class;
	private final static Class<?> NEXT_ACTIVITY_WATER    = AddWaterActivity.class;

	View view;
	Button breakfastBtn, lunchBtn, dinnerBtn, snacksBtn, exerciseBtn, waterBtn;
	ImageView progressBar;
	Integer progressBarWidth;
	TextView caloriesConsumedTextView, calorieGoalTextView;
    RelativeLayout progressBarContainer;
    int mProgressContainerWidth;

    //private UserProgress _userProgress;
	
	private class UserProgressTask extends AsyncTask<Void, Void, Profile>
	{
        private float currentCalorieIntake;
		protected void onPreExecute(){
			MyPlateFragment.this.view.findViewById(R.id.progressBar1).setVisibility(View.VISIBLE);
		}



		@Override
		protected Profile doInBackground(Void... params){
            Map<DiaryEntry.DiaryEntryCategoryEnum, Float>diaryEntry = ModelManager.getDiarySummaryPerType(new Date());
            Float breakfastCalories = diaryEntry.get(DiaryEntryCategoryEnum.CTDiaryEntryTypeBreakfast);
            Float lunchCalories = diaryEntry.get(DiaryEntryCategoryEnum.CTDiaryEntryTypeLunch);
            Float dinnerCalories = diaryEntry.get(DiaryEntryCategoryEnum.CTDiaryEntryTypeDinner);
            Float snacksCalories = diaryEntry.get(DiaryEntryCategoryEnum.CTDiaryEntryTypeSnacks);
            Float exerciseCalories = diaryEntry.get(DiaryEntryCategoryEnum.CTDiaryEntryTypeExercise);

            currentCalorieIntake = Math.round(breakfastCalories) + Math.round(lunchCalories) + Math.round(dinnerCalories) + Math.round(snacksCalories) + Math.round(exerciseCalories);
            //currentCalorieIntake = Math.round(currentCalorieIntake);
            Profile user = ModelManager.sharedModelManager().getUserProfile();
			return user;
		}
        private String getProgress(Integer goal, Boolean isOverGoal) {
            NumberFormat format = NumberFormat.getInstance();
            if (isOverGoal) {
                return format.format(currentCalorieIntake - goal) + " Over Goal";
            }
            return format.format(goal - currentCalorieIntake) + " Remaining Today";
        }
        private String getDailyCaloriesGoal(Integer goal){
            NumberFormat format = NumberFormat.getInstance();

            return format.format(goal);
        }
		protected void onPostExecute(Profile user)
		{


			if (user != null)
			{
                int calorieGoals = user.getCaloriesGoal();
                float progressBarFill = (float)currentCalorieIntake/(float)calorieGoals;
                Boolean overGoal = currentCalorieIntake > calorieGoals;

				// Set Progress bar width
				FragmentActivity activity = getActivity();
				if (null != activity)
				{
                    int containerWidth =    MyPlateFragment.this.progressBarContainer.getWidth();
                    if(containerWidth == 0){
                        containerWidth = 380;
                    }
                    int width = (int) (containerWidth * progressBarFill);
                    if (width < 0) {
                        width = 0;
                    }

                    if (progressBarWidth != width) {
                        animateProgressBarChange(width,overGoal);
                    }
                    else {
                        if (overGoal) {
                            progressBar.setImageResource(R.drawable.progress_foreground_red);
                            progressBar.getLayoutParams().width = progressBarContainer.getWidth();
                        } else {
                            progressBar.setImageResource(R.drawable.progress_foreground);
                            progressBar.getLayoutParams().width = width;
                        }

                        progressBar.requestLayout();
                    }

                    // Update TextViews
                    caloriesConsumedTextView.setText(this.getProgress(calorieGoals, overGoal));
                    calorieGoalTextView.setText(this.getDailyCaloriesGoal(calorieGoals));
                    MyPlateFragment.this.view.findViewById(
                            R.id.progressBar1).setVisibility(View.GONE);

                }
			}
		}
	};
	
	/** (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (container == null) {
            // We have different layouts, and in one of them this
            // fragment's containing frame doesn't exist.  The fragment
            // may still be created from its saved state, but there is
            // no reason to try to create its view hierarchy because it
            // won't be displayed.  Note this is not needed -- we could
            // just run the code below, where we would create and return
            // the view hierarchy; it would just never be used.
            return null;
        }
		
		progressBarWidth = (Integer) MyPlateDefaults.getPref(MyPlateDefaults.PREFS_PROGRESS_BAR_WIDTH, 0);
		
		// Hook up outlets
		this.view 						= (LinearLayout) inflater.inflate(R.layout.fragment_my_plate, container, false);
		this.progressBarContainer       = (RelativeLayout) this.view.findViewById(R.id.progress_track_layout);
        this.progressBar 				= (ImageView) this.view.findViewById(R.id.progressForegroundImageView);
        this.breakfastBtn 				= (Button) this.view.findViewById(R.id.breakfastButton);
        this.lunchBtn 					= (Button) this.view.findViewById(R.id.lunchButton);
        this.dinnerBtn 					= (Button) this.view.findViewById(R.id.dinnerButton);
        this.snacksBtn 					= (Button) this.view.findViewById(R.id.snacksButton);
        this.exerciseBtn 				= (Button) this.view.findViewById(R.id.exerciseButton);
        this.waterBtn 					= (Button) this.view.findViewById(R.id.waterButton);
		this.caloriesConsumedTextView	= (TextView) this.view.findViewById(R.id.caloriesConsumedTextView);
		this.calorieGoalTextView		= (TextView) this.view.findViewById(R.id.calorieGoalTextView);
		
        this.initializeButtons();


        this.mProgressContainerWidth = 380;
        this.progressBarContainer.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                progressBarContainer.removeOnLayoutChangeListener(this);
                refreshViewState();

            }
        });

        // Register to receive messages.
        // This is just like [[NSNotificationCenter defaultCenter] addObserver:...]
        // We are registering an observer (mMessageReceiver) to receive Intents
        // with actions named "custom-event-name".
        LocalBroadcastManager.getInstance(MyPlateApplication.getContext()).registerReceiver(mMessageReceiver,
                new IntentFilter("syncComplete"));
        LocalBroadcastManager.getInstance(MyPlateApplication.getContext()).registerReceiver(mMessageReceiver,
                new IntentFilter("syncFailed"));
        return view;
	}

    // Our handler for received Intents. This will be called whenever an Intent
    // with an action named "custom-event-name" is broadcasted.
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().toString().equals("syncFailed")){

                return;
            }

            MyPlateDefaults.setPref(MyPlateDefaults.PREFS_LAST_SYNC,new Date().getTime());
            MyPlateDefaults.setPref(MyPlateDefaults.PREFS_LAST_ONLOAD_SYNC,new Date().getTime());

            refreshViewState();
        }
    };

	public void initializeButtons(){
		OnClickListener onClickListener = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				int id = v.getId();
                DiaryEntryCategoryEnum timeOfDay = null;
				Class<?> nextActivity = null;
				switch(id){
					case R.id.breakfastButton:
                        timeOfDay = DiaryEntryCategoryEnum.CTDiaryEntryTypeBreakfast;
						nextActivity = NEXT_ACTIVITY_FOOD;
						break;
					case R.id.lunchButton:
                        timeOfDay = DiaryEntryCategoryEnum.CTDiaryEntryTypeLunch;
						nextActivity = NEXT_ACTIVITY_FOOD;
						break;
					case R.id.dinnerButton:
                        timeOfDay = DiaryEntryCategoryEnum.CTDiaryEntryTypeDinner;
						nextActivity = NEXT_ACTIVITY_FOOD;
						break;
					case R.id.snacksButton:
                        timeOfDay = DiaryEntryCategoryEnum.CTDiaryEntryTypeSnacks;
						nextActivity = NEXT_ACTIVITY_FOOD;
						break;
					case R.id.exerciseButton:
                        timeOfDay = DiaryEntryCategoryEnum.CTDiaryEntryTypeExercise;
                        nextActivity = NEXT_ACTIVITY_EXERCISE;
						break;
					case R.id.waterButton:
                        timeOfDay = DiaryEntryCategoryEnum.CTDiaryEntryTypeWater;
                        nextActivity = NEXT_ACTIVITY_WATER;
						break;
				}
				if (nextActivity != null){
					Intent intent = new Intent(getActivity(), nextActivity);
                    intent.putExtra("TimeOfDay",timeOfDay.ordinal());
                    intent.putExtra(Date.class.getName(),new Date());
					startActivity(intent);
				}
			}
		};
		
		this.breakfastBtn.setOnClickListener(onClickListener);
		this.lunchBtn.setOnClickListener(onClickListener);
		this.dinnerBtn.setOnClickListener(onClickListener);
		this.snacksBtn.setOnClickListener(onClickListener);
		this.exerciseBtn.setOnClickListener(onClickListener);
		this.waterBtn.setOnClickListener(onClickListener);
        this.setHasOptionsMenu(true);
	}


	@Override
	public void onResume() {
		super.onResume();

        getActivity().getActionBar().setTitle("My Plate");
        ActionBar bar = this.getActivity().getActionBar();
        bar.removeAllTabs();
        bar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        refreshViewState();

        Log.d("MyPlateFragment", "onResume");

        MyPlateApplication app = (MyPlateApplication) getActivity().getApplication();
        if (app.wasInBackground()) {
        	app.hasBeenLoaded();

        	// Diary sync etc.
        	long lastOnloadSync = MyPlateDefaults.getPref(MyPlateDefaults.PREFS_LAST_ONLOAD_SYNC, (long) 0);
        	Calendar yesterday = Calendar.getInstance();
        	yesterday.add(Calendar.DATE, -1);

            Profile userProfile = ModelManager.sharedModelManager().getUserProfile();
            if(userProfile == null){
                return;
            }

            String username = ModelManager.sharedModelManager().getUserProfile().getUsername();


        	if (lastOnloadSync < yesterday.getTime().getTime() && username != null) {
        		ModelManager.sharedModelManager().synchronizeDataWithServer();
                ModelManager.delegate = this;
        	}
        }

	}
	
	@Override
	public void onStop() {
        MyPlateDefaults.setPref(MyPlateDefaults.PREFS_PROGRESS_BAR_WIDTH, this.progressBarWidth);
		
		super.onStop();
	}
    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);

        int containerWidth =    this.progressBarContainer.getWidth();
        containerWidth =    this.progressBarContainer.getWidth();
    }

    @Override
    public void onDestroy() {
        // Unregister since the activity is about to be closed.
        // This is somewhat like [[NSNotificationCenter defaultCenter] removeObserver:name:object:]
        LocalBroadcastManager.getInstance(this.getActivity()).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

	private void refreshViewState(){
		if (!isAdded() && !isVisible()){
			return; // Do not have access to Activity so must return 
		}
		
		Map<DiaryEntry.DiaryEntryCategoryEnum, Float>diaryEntry = ModelManager.getDiarySummaryPerType(new Date());

		String defaultStr = getString(R.string.track);
		Float breakfastCalories = diaryEntry.get(DiaryEntryCategoryEnum.CTDiaryEntryTypeBreakfast);

        if (breakfastCalories == 0.0f){
			this.breakfastBtn.setText(String.format(getString(R.string.btn_track_breakfast_format), defaultStr));
            this.breakfastBtn.setBackgroundResource(R.drawable.btn_track_grey_selector);
		} else {
			this.breakfastBtn.setText(String.format(getString(R.string.btn_track_breakfast_format), Math.round(breakfastCalories)));
            this.breakfastBtn.setBackgroundResource(R.drawable.btn_track_blue_selector);

        }

		Float lunchCalories = diaryEntry.get(DiaryEntryCategoryEnum.CTDiaryEntryTypeLunch);
		if (lunchCalories == 0.0f){
			this.lunchBtn.setText(String.format(getString(R.string.btn_track_lunch_format), defaultStr));
            this.lunchBtn.setBackgroundResource(R.drawable.btn_track_grey_selector);

        } else {
			this.lunchBtn.setText(String.format(getString(R.string.btn_track_lunch_format),  Math.round(lunchCalories)));
			this.lunchBtn.setBackgroundResource(R.drawable.btn_track_blue_selector);
		}

		Float dinnerCalories = diaryEntry.get(DiaryEntryCategoryEnum.CTDiaryEntryTypeDinner);
		if (dinnerCalories == 0.0f){
			this.dinnerBtn.setText(String.format(getString(R.string.btn_track_dinner_format), defaultStr));
            this.dinnerBtn.setBackgroundResource(R.drawable.btn_track_grey_selector);

        } else {
			this.dinnerBtn.setText(String.format(getString(R.string.btn_track_dinner_format),  Math.round(dinnerCalories)));
			this.dinnerBtn.setBackgroundResource(R.drawable.btn_track_blue_selector);
		}

		Float snacksCalories = diaryEntry.get(DiaryEntryCategoryEnum.CTDiaryEntryTypeSnacks);
		if (snacksCalories == 0.0f){
			this.snacksBtn.setText(String.format(getString(R.string.btn_track_snacks_format), defaultStr));
            this.snacksBtn.setBackgroundResource(R.drawable.btn_track_grey_selector);

        } else {
			this.snacksBtn.setText(String.format(getString(R.string.btn_track_snacks_format),  Math.round(snacksCalories)));
			this.snacksBtn.setBackgroundResource(R.drawable.btn_track_blue_selector);
		}

		Float exerciseCalories = diaryEntry.get(DiaryEntryCategoryEnum.CTDiaryEntryTypeExercise);
		if (exerciseCalories == 0.0f){
			this.exerciseBtn.setText(String.format(getString(R.string.btn_track_exercise_format), defaultStr));
            this.exerciseBtn.setBackgroundResource(R.drawable.btn_track_grey_selector);

        } else {
			this.exerciseBtn.setText(String.format(getString(R.string.btn_track_exercise_format),  Math.round(exerciseCalories)));
			this.exerciseBtn.setBackgroundResource(R.drawable.btn_track_orange_selector);
		}

		Float waterOunces = diaryEntry.get(DiaryEntryCategoryEnum.CTDiaryEntryTypeWater);
        MyPlateDefaults.WaterUnits waterUnits = (MyPlateDefaults.WaterUnits)MyPlateDefaults.getPref(MyPlateDefaults.PREFS_WATER_UNITS, MyPlateDefaults.WaterUnits.OUNCES);
        String modifier = "oz";
        if(waterUnits == MyPlateDefaults.WaterUnits.MILLILITERS){
            modifier = "ml";
        }

		if (waterOunces == 0.0f){
			this.waterBtn.setText(String.format(getString(R.string.btn_track_water_format), defaultStr));
            this.waterBtn.setBackgroundResource(R.drawable.btn_track_grey_selector);

        } else {
			this.waterBtn.setText(String.format(getString(R.string.btn_track_water_format), waterOunces.intValue())+modifier);
			this.waterBtn.setBackgroundResource(R.drawable.btn_track_blue_selector);
		}

		new UserProgressTask().execute(new Void[]{});
		
	}

	private void animateProgressBarChange(int targetWidth, final boolean isOverGoal){
		if (!isOverGoal){
			this.progressBar.setImageResource(R.drawable.progress_foreground);
		}

		WidthAnimation animation = new WidthAnimation(this.progressBar, this.progressBarWidth, targetWidth);
		animation.setDuration(400);
		animation.setStartOffset(300);
		animation.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationEnd(Animation animation) {
				if (isOverGoal){
					progressBar.setImageResource(R.drawable.progress_foreground_red);
				}
			}
			@Override
			public void onAnimationRepeat(Animation animation) {}

			@Override
			public void onAnimationStart(Animation animation) {}
		});
		this.progressBar.startAnimation(animation);

		this.progressBarWidth = targetWidth;
    }

    @Override
    public void syncCompleted(ModelManager modelManager, Boolean success) {
       if(success == false){
           new AlertDialog.Builder(getActivity())
                   .setMessage("Please ensure you are connected to the internet.")
                   .setTitle("Unable to sync at this time")
                   .setNeutralButton(android.R.string.ok,
                           new DialogInterface.OnClickListener() {
                               public void onClick(DialogInterface dialog, int whichButton){}
                           })
                   .show();
           return;
       }

        MyPlateDefaults.setPref(MyPlateDefaults.PREFS_LAST_ONLOAD_SYNC,new Date().getTime());
        MyPlateDefaults.setPref(MyPlateDefaults.PREFS_LAST_SYNC,new Date().getTime());

        this.refreshViewState();
    }

    @Override
    public void syncPercentCompleted(ModelManager modelManager, float percentComplete) {

    }

}
