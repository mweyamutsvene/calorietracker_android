package com.livestrong.calorietracker.fragments.More;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeMap;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.livestrong.calorietracker.R;
import com.livestrong.calorietracker.back.db.ModelManager;
import com.livestrong.calorietracker.back.db.gen.Profile;
import com.livestrong.calorietracker.configuration.MyPlateDefaults;


public class MoreProfileFragment extends Fragment {

    private LinearLayout view;
    private Profile userProfile;
    private EditText birthdayEditText, heightEditText, feetEditText, inchesEditText, weightEditText, calorieGoalEditText;
    private Spinner genderSpinner, activityLevelSpinner, weightGoalSpinner;
    private CheckBox automaticCalorieGoalCheckBox;
    private LinearLayout calorieGoalContainer;
    private ScrollView scrollView;
    private TextView weightUnitsTextView;
    private TreeMap<Double, String> weightGoalsMap;

    private class UserProfileTask extends AsyncTask<Runnable, Void, Profile> {
        private Runnable mCallback;
        @Override
        protected Profile doInBackground(Runnable... callbacks){
            if(null != callbacks && callbacks.length > 0){
                mCallback = callbacks[0];
            }
            return ModelManager.sharedModelManager().getUserProfile();
        }

        protected void onPostExecute(Profile profile){
            if (profile != null) {
                userProfile = profile;
                mCallback.run();
                return;
            }

            createNewUserProfile();
            mCallback.run();

        }

    }

    public MoreProfileFragment(){
        new UserProfileTask().execute(new Runnable[]{
                new Runnable(){
                    public void run() {
                    }
                }});
    }

    public void createNewUserProfile(){
        this.userProfile = new Profile();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            // We have different layouts, and in one of them this
            // fragment's containing frame doesn't exist.  The fragment
            // may still be created from its saved state, but there is
            // no reason to try to create its view hierarchy because it
            // won't be displayed.  Note this is not needed -- we could
            // just run the code below, where we would create and return
            // the view hierarchy; it would just never be used.
            return null;
        }

        // Hook up outlets
        this.view 					= (LinearLayout) inflater.inflate(R.layout.fragment_more_profile, container, false);
        this.birthdayEditText 		= (EditText) this.view.findViewById(R.id.birthdayEditText);
        this.heightEditText 		= (EditText) this.view.findViewById(R.id.heightEditText);
        this.feetEditText 			= (EditText) this.view.findViewById(R.id.feetEditText);
        this.inchesEditText 		= (EditText) this.view.findViewById(R.id.inchesEditText);
        this.weightEditText 		= (EditText) this.view.findViewById(R.id.weightEditText);
        this.weightUnitsTextView 	= (TextView) this.view.findViewById(R.id.weightUnitsTextView);
        this.calorieGoalEditText	= (EditText) this.view.findViewById(R.id.calorieGoalEditText);
        this.genderSpinner 			= (Spinner) this.view.findViewById(R.id.genderSpinner);
        this.activityLevelSpinner 	= (Spinner) this.view.findViewById(R.id.activityLevelSpinner);
        this.weightGoalSpinner 		= (Spinner) this.view.findViewById(R.id.weightGoalSpinner);
        this.calorieGoalContainer 	= (LinearLayout) this.view.findViewById(R.id.calorieGoalContainer);
        this.automaticCalorieGoalCheckBox = (CheckBox) this.view.findViewById(R.id.automaticCalorieGoalCheckBox);
        this.scrollView 			= (ScrollView) this.view.findViewById(R.id.scrollView);

        this.getActivity().getActionBar().setTitle(R.string.profile);

        return this.view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("MoreProfile", "key RESUME");
            new UserProfileTask().execute(new Runnable[]
                {
                        new Runnable()
                        {
                            public void run()
                            {
                                if (view != null){
                                    initializeFragmentBasedOnPreferences();
                                }
                                initalizeCheckBox();
                                initializeEditTexts();
                                initializeSpinners();
                            }
                        }
                });
    }

    private void initializeFragmentBasedOnPreferences(){

        //Check which metric type to use for height
        MyPlateDefaults.DistanceUnits distanceUnits = MyPlateDefaults.DistanceUnits.valueOf(MyPlateDefaults.getPref(MyPlateDefaults.PREFS_DISTANCE_UNITS, MyPlateDefaults.DistanceUnits.MILES).toString());

        //Figure out which height cell to show
        LinearLayout heightCell;
        if (distanceUnits == MyPlateDefaults.DistanceUnits.METERS){
            heightCell = (LinearLayout) this.view.findViewById(R.id.heightImperialContainer);
        } else {
            heightCell = (LinearLayout) this.view.findViewById(R.id.heightMetricContainer);
        }

        heightCell.getLayoutParams().height = 0;
        heightCell.requestLayout();

        //Check which metric type to use for weight
        MyPlateDefaults.WeightUnits weightUnits = MyPlateDefaults.WeightUnits.valueOf(MyPlateDefaults.getPref(MyPlateDefaults.PREFS_WEIGHT_UNITS, MyPlateDefaults.WeightUnits.POUNDS).toString());
        if (weightUnits == MyPlateDefaults.WeightUnits.KILOGRAMS){
            this.weightUnitsTextView.setText("kgs");
        } else if (weightUnits == MyPlateDefaults.WeightUnits.POUNDS){
            this.weightUnitsTextView.setText("lbs");
        } else {
            this.weightUnitsTextView.setText("st");
        }
    }

    private void initializeEditTexts(){
        if (this.userProfile != null){
            this.weightEditText.setText(this.userProfile.getWeightForSelectedUnits());
            this.heightEditText.setText(this.userProfile.getMetricHeight() + "");
            this.feetEditText.setText(this.userProfile.getFeet() + "");
            this.inchesEditText.setText(this.userProfile.getInches() + "");
            this.calorieGoalEditText.setText(this.userProfile.getCaloriesGoal() + "");
            this.birthdayEditText.setText(this.userProfile.getAge()+ "");
        }
    }

    private void initializeSpinners(){
        ArrayAdapter<CharSequence> genderAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.gender, R.layout.spinner_item);
        genderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.genderSpinner.setAdapter(genderAdapter);

        if(this.userProfile == null){
             return;
        }
        Profile.Gender gender = Profile.Gender.values()[userProfile.getGender()];
        if (gender == Profile.Gender.GENDER_MALE){
            this.genderSpinner.setSelection(0);
        } else if (gender ==  Profile.Gender.GENDER_FEMALE){
            this.genderSpinner.setSelection(1);
        }

        //Get values for the Activity Level
        HashMap<Float, String> levels = MyPlateDefaults.ActivityLevels.getLevels();
        Set<Float> keys =  levels.keySet();
        ArrayList<Float> sortedKeys = new ArrayList<Float>(keys);
        Collections.sort(sortedKeys);

        CharSequence levelsStrings[] = new CharSequence[levels.values().size()];
        levelsStrings = levels.values().toArray(levelsStrings);
        int i = 0;
        int selectedIndex = 0;
        for (Float key:sortedKeys){
            levelsStrings[i] = levels.get(key);
            // equals for double numbers with 3 decimals.....
            if (Math.round(this.userProfile.getActivityLevel()*1000) == Math.round(key*1000)) {
                selectedIndex = i;
            }
            i++;
        }

        ArrayAdapter<CharSequence> activityLevelAdapter = new ArrayAdapter<CharSequence>(getActivity(), R.layout.spinner_item, levelsStrings);
        activityLevelAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.activityLevelSpinner.setAdapter(activityLevelAdapter);
        this.activityLevelSpinner.setSelection(selectedIndex);


        this.weightGoalsMap = this.userProfile.getWeightGoals();
        Set<Double>weightKeys = weightGoalsMap.keySet();
        ArrayList<Double> sortedWeightKeys = new ArrayList<Double>(weightKeys);
        Collections.sort(sortedWeightKeys);

        String weightStrings[] = new String[weightGoalsMap.size()];
        weightStrings = weightGoalsMap.values().toArray(weightStrings);
        i = 0;
        selectedIndex = 0;
        for (Double key : sortedWeightKeys){
            weightStrings[i] = weightGoalsMap.get(key);
            if (this.userProfile.getGoal() == (double)key){
                selectedIndex = i;
            }
            i++;
        }

        ArrayAdapter<CharSequence> weightGoalAdapter = new ArrayAdapter<CharSequence>(getActivity(), R.layout.spinner_item, weightStrings);
        weightGoalAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.weightGoalSpinner.setAdapter(weightGoalAdapter);
        this.weightGoalSpinner.setSelection(selectedIndex);
    }





    private void initalizeCheckBox(){
        if (this.userProfile != null && this.userProfile.getMode() == (Profile.GoalMode.GOAL_MODE_CALCULATED.ordinal())) {
            this.automaticCalorieGoalCheckBox.setChecked(true);
            this.calorieGoalContainer.getLayoutParams().height = 0;
        } else {
            this.automaticCalorieGoalCheckBox.setChecked(false);
            this.calorieGoalContainer.getLayoutParams().height = ViewPager.LayoutParams.WRAP_CONTENT;
        }
        this.automaticCalorieGoalCheckBox.requestLayout();
        this.calorieGoalContainer.requestLayout();

        this.automaticCalorieGoalCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    calorieGoalContainer.getLayoutParams().height = 0;
                } else {
                    calorieGoalContainer.getLayoutParams().height = ViewPager.LayoutParams.WRAP_CONTENT;
                }
                calorieGoalContainer.requestLayout();
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        scrollView.fullScroll(ScrollView.FOCUS_DOWN);
                    }
                }, 100);
            }
        });
    }

    public Boolean saveProfile(){
        if (this.userProfile == null || this.heightEditText == null || this.feetEditText == null ||
                this.inchesEditText == null || this.genderSpinner == null || this.weightEditText == null ||
                this.automaticCalorieGoalCheckBox == null || this.activityLevelSpinner == null  ||
                this.weightGoalSpinner == null){
            return false;
        }

        //Save age
        if(birthdayEditText.getText().toString().length() == 0){
            //No Birthday added;
            //TODO: add alert specifying this is required
            return false;

        }
        if(Integer.parseInt(birthdayEditText.getText().toString()) < 18){
            //App is only for 18 and older
            //TODO: add alert specifying this is required
            return false;
        }
        this.userProfile.setAge(Integer.parseInt(birthdayEditText.getText().toString()));

        // Save height
        MyPlateDefaults.DistanceUnits distanceUnits =(MyPlateDefaults.DistanceUnits) MyPlateDefaults.getPref(MyPlateDefaults.PREFS_DISTANCE_UNITS, MyPlateDefaults.DistanceUnits.MILES);
        if (distanceUnits == MyPlateDefaults.DistanceUnits.METERS){
            String heightString = this.heightEditText.getText().toString();
            heightString = heightString.replace(",", ".");
            if (heightString.length() > 0){
                this.userProfile.editHeight(Double.parseDouble(heightString));
            }
        } else {
            String feetString = this.feetEditText.getText().toString();
            feetString = feetString.replace(",", ".");
            String inchesString = this.inchesEditText.getText().toString();
            inchesString = inchesString.replace(",", ".");

            if (feetString.length() == 0){
                feetString = "0";
            }
            if (inchesString.length() == 0){
                inchesString = "0";
            }
            this.userProfile.editHeight(Double.parseDouble(feetString), Double.parseDouble(inchesString));
        }

        // Save gender
        if (this.genderSpinner.getSelectedItemPosition() == 0){
            this.userProfile.editGender(Profile.Gender.GENDER_MALE);
        } else {
            this.userProfile.editGender(Profile.Gender.GENDER_FEMALE);
        }

        // Save Weight
        String weightString = this.weightEditText.getText().toString();
        weightString = weightString.replace(",", ".");
        if (weightString.length() > 0){
            this.userProfile.editWeight(Double.parseDouble(weightString));
        }

        // Save Automatic calorie goal settings
        if (this.automaticCalorieGoalCheckBox.isChecked()){
            this.userProfile.editMode(Profile.GoalMode.GOAL_MODE_CALCULATED);
        } else {
            this.userProfile.editMode(Profile.GoalMode.GOAL_MODE_OVERRIDDEN);
            String goalStr = this.calorieGoalEditText.getText().toString();
            if (goalStr.length() > 0){
                this.userProfile.editCalories(Double.parseDouble(goalStr));
            }
        }

        // Save ActivityLevel
        HashMap<Float, String> levels = MyPlateDefaults.ActivityLevels.getLevels();
        Set<Float>keys =  levels.keySet();
        for (Float key:keys){
            if (levels.get(key).equals(this.activityLevelSpinner.getSelectedItem())){
                this.userProfile.editActivityLevel((double) key);
                break;
            }
        }

        // Save Weight Goal
        Set<Double>weightKeys =  this.weightGoalsMap.keySet();
        for (Double key:weightKeys){
            if (this.weightGoalsMap.get(key).equals(this.weightGoalSpinner.getSelectedItem())){
                this.userProfile.editGoal(key);
                break;
            }
        }

        ModelManager.sharedModelManager().saveUserProfile(this.userProfile);
        return true;
    }

    @Override
    public void onPause() {
        hideKeyboard();
        super.onPause();
    }

    private void hideKeyboard(){
        if (heightEditText == null){
            return;
        }
        // Hide Keyboard
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(heightEditText.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(feetEditText.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(inchesEditText.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(weightEditText.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(calorieGoalEditText.getWindowToken(), 0);
    }
}

