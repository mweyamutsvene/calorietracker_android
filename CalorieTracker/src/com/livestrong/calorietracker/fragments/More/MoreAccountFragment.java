package com.livestrong.calorietracker.fragments.More;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager.LayoutParams;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.livestrong.calorietracker.MyPlateApplication;
import com.livestrong.calorietracker.R;
import com.livestrong.calorietracker.activities.LoginActivity;
import com.livestrong.calorietracker.back.api.CTWebserviceRequest;
import com.livestrong.calorietracker.back.db.ModelManager;
import com.livestrong.calorietracker.configuration.MyPlateDefaults;
import com.livestrong.calorietracker.utilities.NotificationReceiver;


public class MoreAccountFragment extends Fragment implements ModelManager.ModelManagerDelegate {
	
	LinearLayout view, reminderTimeContainer, connectContainer, syncContainer;
	CheckBox dailyReminderCheckBox;
	EditText reminderTimeEditText;
	Spinner weightSpinner, distancesSpinner, waterSpinner;
	ScrollView scrollView;
	Dialog dialog;
	Date reminderTime;
	Button syncButton;
	ProgressBar syncProgressBar;
	TextView lastSyncTextView;
	int rowHeight = 0;

    @Override
    public void syncCompleted(ModelManager modelManager, Boolean success) {
        if(success == false){
            new AlertDialog.Builder(getActivity())
                    .setMessage("Please ensure you are connected to the internet.")
                    .setTitle("Unable to sync at this time")
                    .setNeutralButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton){}
                            })
                    .show();

        }
        this.syncProgressBar.setVisibility(View.INVISIBLE);
        this.syncButton.setEnabled(true);

        this.refreshSyncTextView();
    }

    @Override
    public void syncPercentCompleted(ModelManager modelManager, float percentComplete) {

    }

    private class UserRefreshTask  extends AsyncTask<Void, Void, Void>
	{		
		@Override
		protected Void doInBackground(Void... params) 
		{
			ModelManager.sharedModelManager().synchronizeDataWithServer();
            ModelManager.delegate = MoreAccountFragment.this;
			
			return null;
		}

	};
    @Override
    public void onDestroy() {
        // Unregister since the activity is about to be closed.
        // This is somewhat like [[NSNotificationCenter defaultCenter] removeObserver:name:object:]
        ModelManager.delegate = null;
        super.onDestroy();
    }

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (container == null) {
	        // We have different layouts, and in one of them this
	        // fragment's containing frame doesn't exist.  The fragment
	        // may still be created from its saved state, but there is
	        // no reason to try to create its view hierarchy because it
	        // won't be displayed.  Note this is not needed -- we could
	        // just run the code below, where we would create and return
	        // the view hierarchy; it would just never be used.
	        return null;
	    }
		
		// Hook up outlets
		this.view = (LinearLayout) inflater.inflate(R.layout.fragment_more_account, container, false);
		this.connectContainer 		= (LinearLayout) this.view.findViewById(R.id.connectContainer);
		this.dailyReminderCheckBox 	= (CheckBox) this.view.findViewById(R.id.dailyReminderCheckBox);
		this.reminderTimeEditText 	= (EditText) this.view.findViewById(R.id.reminderTimeEditText);
		this.weightSpinner 			= (Spinner) this.view.findViewById(R.id.weightSpinner);
		this.distancesSpinner		= (Spinner) this.view.findViewById(R.id.distancesSpinner);
		this.waterSpinner			= (Spinner) this.view.findViewById(R.id.waterSpinner);
		this.reminderTimeContainer	= (LinearLayout) this.view.findViewById(R.id.reminderTimeContainer);
		this.scrollView				= (ScrollView) this.view.findViewById(R.id.scrollView);
		this.syncButton 			= (Button) this.view.findViewById(R.id.syncNowButton);
		this.syncContainer			= (LinearLayout) this.view.findViewById(R.id.syncContainer);
		this.lastSyncTextView		= (TextView) this.view.findViewById(R.id.lastSyncTextView);
		this.syncProgressBar		= (ProgressBar) this.view.findViewById(R.id.syncProgressBar);
		this.syncProgressBar.setVisibility(View.INVISIBLE);
		
		long savedTime = MyPlateDefaults.getPref(MyPlateDefaults.PREFS_DAILY_REMINDER_TIME, (long) 0);
		if (savedTime != 0){
			this.reminderTime = new Date(savedTime);
		} else {
			this.reminderTime = new Date();	
		}		
		
		this.refreshReminderEditText();
		this.initializeEditTexts();
		this.initializeButtons();
		this.initializeSpinners();
		this.initalizeCheckBox();
		this.refreshSyncTextView();
		this.refreshLayout();

        this.getActivity().getActionBar().setTitle(R.string.account);

        // Register to receive messages.
        // This is just like [[NSNotificationCenter defaultCenter] addObserver:...]
        // We are registering an observer (mMessageReceiver) to receive Intents
        // with actions named "custom-event-name".

        return this.view;

    }

	
	private void refreshLayout(){
		if (rowHeight == 0){
			rowHeight = this.connectContainer.getLayoutParams().height;
		}
		if (CTWebserviceRequest.isLoggedIn()){
			this.connectContainer.getLayoutParams().height = 0;
			this.syncContainer.getLayoutParams().height = rowHeight;
		} else {
			this.connectContainer.getLayoutParams().height = rowHeight;
			this.syncContainer.getLayoutParams().height = 0;
		}
		this.connectContainer.requestLayout();
		this.syncContainer.requestLayout();
	}
	
	private void initializeButtons(){
		Button connectButton = (Button)this.view.findViewById(R.id.connectButton);
		connectButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(), LoginActivity.class);
				intent.putExtra(LoginActivity.INTENT_APP_OFFLINE_MODE, true);
				startActivityForResult(intent, 1);
			}
		});
		
		this.syncButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				syncButton.setEnabled(false);
				syncProgressBar.setVisibility(View.VISIBLE);

                ModelManager.sharedModelManager().synchronizeDataWithServer();
                ModelManager.delegate = MoreAccountFragment.this;
			}
		});
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if (resultCode == Activity.RESULT_OK){
			this.refreshLayout();
		}
	}
	
	private void initializeSpinners(){
		ArrayAdapter<CharSequence> weightAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.weightUnits, R.layout.spinner_item);
		weightAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		this.weightSpinner.setAdapter(weightAdapter);
		MyPlateDefaults.WeightUnits weightUnits = (MyPlateDefaults.WeightUnits)MyPlateDefaults.getPref(MyPlateDefaults.PREFS_WEIGHT_UNITS, MyPlateDefaults.WeightUnits.POUNDS);
		if (weightUnits == MyPlateDefaults.WeightUnits.KILOGRAMS){
			this.weightSpinner.setSelection(0);
		} else if (weightUnits == MyPlateDefaults.WeightUnits.POUNDS) {
			this.weightSpinner.setSelection(1);
		} else {
			this.weightSpinner.setSelection(2);
		}
		
		ArrayAdapter<CharSequence> distancesAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.distanceUnits, R.layout.spinner_item);
		distancesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		this.distancesSpinner.setAdapter(distancesAdapter);
		MyPlateDefaults.DistanceUnits distanceUnits = (MyPlateDefaults.DistanceUnits)MyPlateDefaults.getPref(MyPlateDefaults.PREFS_DISTANCE_UNITS, MyPlateDefaults.DistanceUnits.MILES);
		if (distanceUnits == MyPlateDefaults.DistanceUnits.METERS){
			this.distancesSpinner.setSelection(0);
		} else if (distanceUnits == MyPlateDefaults.DistanceUnits.MILES){
			this.distancesSpinner.setSelection(1);
		}
		
		ArrayAdapter<CharSequence> waterAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.waterUnits, R.layout.spinner_item);
		waterAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		this.waterSpinner.setAdapter(waterAdapter);
		MyPlateDefaults.WaterUnits waterUnits = (MyPlateDefaults.WaterUnits)MyPlateDefaults.getPref(MyPlateDefaults.PREFS_WATER_UNITS, MyPlateDefaults.WaterUnits.OUNCES);
		if (waterUnits == MyPlateDefaults.WaterUnits.MILLILITERS){
			this.waterSpinner.setSelection(0);
		} else {
			this.waterSpinner.setSelection(1);
		}
		
	}
	
	private void initalizeCheckBox(){		
		Boolean dailyReminder = MyPlateDefaults.getPref(MyPlateDefaults.PREFS_DAILY_REMINDER, false);
		this.dailyReminderCheckBox.setChecked(dailyReminder);		
		if (!dailyReminder){
			reminderTimeContainer.getLayoutParams().height = 0;
		}
		
		this.dailyReminderCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (!isChecked){
					reminderTimeContainer.getLayoutParams().height = 0;
					cancelReminder();
				} else {
					reminderTimeContainer.getLayoutParams().height = LayoutParams.WRAP_CONTENT;
					setDailyReminder();
				}
				reminderTimeContainer.requestLayout();
				
				final Handler handler = new Handler();
				handler.postDelayed(new Runnable() {
				  @Override
				  public void run() {
					  scrollView.fullScroll(ScrollView.FOCUS_DOWN);
				  }
				}, 100);
			}
		});
	}
	
	private void initializeEditTexts(){
		reminderTimeEditText.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (dialog == null){
					dialog = new Dialog(getActivity());
					dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					dialog.setContentView(R.layout.dialog_time); 
					final TimePicker timePicker = (TimePicker) dialog.findViewById(R.id.timePicker1);
					timePicker.setCurrentHour(reminderTime.getHours());
					timePicker.setCurrentMinute(reminderTime.getMinutes());
					
					Button cancelButton = (Button) dialog.findViewById(R.id.cancelButton);
					cancelButton.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
				            dialog.cancel();
						}
					});	
										
					Button doneButton = (Button) dialog.findViewById(R.id.doneButton);
					doneButton.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							reminderTime.setHours(timePicker.getCurrentHour());
							reminderTime.setMinutes(timePicker.getCurrentMinute());

							MyPlateDefaults.setPref(MyPlateDefaults.PREFS_DAILY_REMINDER_TIME, reminderTime.getTime());
							
							refreshReminderEditText();
							
							setDailyReminder();
							
							dialog.dismiss();
						}
					});
				}
				
				if (!dialog.isShowing()){
					dialog.show();	
				}
				
				return true;
			}
		});
	}
	
	private void refreshReminderEditText(){
		SimpleDateFormat formatter = new SimpleDateFormat("h:mm a");
		this.reminderTimeEditText.setText(formatter.format(this.reminderTime));
	}
	
	public void saveProfile(){
		if (this.weightSpinner == null || this.distancesSpinner == null || this.waterSpinner == null || this.dailyReminderCheckBox == null){
			return;
		}
		
		// Save Weight Units
		int position = this.weightSpinner.getSelectedItemPosition();
		if (position == 0){ // Kilograms selected
            MyPlateDefaults.setPref(MyPlateDefaults.PREFS_WEIGHT_UNITS, MyPlateDefaults.WeightUnits.KILOGRAMS);
		} else if (position == 1){ // Pounds selected
            MyPlateDefaults.setPref(MyPlateDefaults.PREFS_WEIGHT_UNITS, MyPlateDefaults.WeightUnits.POUNDS);
		} else { // Stones selected
            MyPlateDefaults.setPref(MyPlateDefaults.PREFS_WEIGHT_UNITS, MyPlateDefaults.WeightUnits.STONES);
		}
		
		// Save Distance Units
		position = this.distancesSpinner.getSelectedItemPosition();
		if (position == 0){ // Kilograms selected
            MyPlateDefaults.setPref(MyPlateDefaults.PREFS_DISTANCE_UNITS, MyPlateDefaults.DistanceUnits.METERS);
		} else if (position == 1){ // Pounds selected
            MyPlateDefaults.setPref(MyPlateDefaults.PREFS_DISTANCE_UNITS, MyPlateDefaults.DistanceUnits.MILES);
		} 
		
		// Save Water Units
		position = this.waterSpinner.getSelectedItemPosition();
		if (position == 0){ // Kilograms selected
            MyPlateDefaults.setPref(MyPlateDefaults.PREFS_WATER_UNITS, MyPlateDefaults.WaterUnits.MILLILITERS);
		} else if (position == 1){ // Pounds selected
            MyPlateDefaults.setPref(MyPlateDefaults.PREFS_WATER_UNITS, MyPlateDefaults.WaterUnits.OUNCES);
		}		
		
		// Save Dialy Reminder status
		if (this.dailyReminderCheckBox.isChecked()){
            MyPlateDefaults.setPref(MyPlateDefaults.PREFS_DAILY_REMINDER, true);
		} else {
            MyPlateDefaults.setPref(MyPlateDefaults.PREFS_DAILY_REMINDER, false);
		}
	}
	
	@Override
	public void onPause() {
		// Save data
		this.saveProfile();	
		
		super.onPause();
	}


	private void refreshSyncTextView(){
		long time = MyPlateDefaults.getPref(MyPlateDefaults.PREFS_LAST_SYNC, (long) 0);
		if (time != 0){
			Date lastSync = new Date(time);
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy h:mm:ss a");
			this.lastSyncTextView.setText("Last Sync: " + dateFormat.format(lastSync));
		} else {
			this.lastSyncTextView.setText("Last Sync: ");
		}
	}
	
	private void setDailyReminder(){		
        //---use the AlarmManager to trigger an alarm---
        AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Activity.ALARM_SERVICE);                 

        //---get current date and time---
        Calendar calendar = Calendar.getInstance();       
        
        //---sets the time for the alarm to trigger---
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, reminderTime.getHours());
        calendar.set(Calendar.MINUTE, reminderTime.getMinutes());
        calendar.set(Calendar.SECOND, 0);

        //---PendingIntent to launch activity when the alarm triggers-
        PendingIntent pendingIntent = getDailyReminderIntent();

        //---sets the alarm to trigger---
        alarmManager.cancel(pendingIntent);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
	}
	
	private void cancelReminder(){
		AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Activity.ALARM_SERVICE);
		alarmManager.cancel(getDailyReminderIntent());
	}
	
	private PendingIntent getDailyReminderIntent(){
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
            	getActivity().getBaseContext(), 0, 
                new Intent(getActivity(), NotificationReceiver.class),
                0); 
        return pendingIntent;
	}
}
