package com.livestrong.calorietracker.fragments;

import java.util.Date;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.livestrong.calorietracker.R;


public class DiaryFragment extends Fragment implements ActionBar.TabListener {
	
	private View view;
	DiaryCalendarFragment calendarFragment;
	DiaryListFragment listFragment;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Log.d("D", "onCreateView");
		if (container == null) {
	        // We have different layouts, and in one of them this
	        // fragment's containing frame doesn't exist.  The fragment
	        // may still be created from its saved state, but there is
	        // no reason to try to create its view hierarchy because it
	        // won't be displayed.  Note this is not needed -- we could
	        // just run the code below, where we would create and return
	        // the view hierarchy; it would just never be used.
	        return null;
	    }
		
		this.view = (LinearLayout)inflater.inflate(R.layout.fragment_diary, container, false);
		this.calendarFragment = new DiaryCalendarFragment(this);
		this.listFragment = new DiaryListFragment();
        return this.view;
	}
	
	@Override
	public void onResume() {
		super.onResume();
        getActivity().getActionBar().setTitle("Diary");
        ActionBar bar = this.getActivity().getActionBar();
        bar.removeAllTabs();
        bar.addTab(bar.newTab().setText(R.string.monthly).setTabListener(this),false);
        bar.addTab(bar.newTab().setText(R.string.daily).setTabListener(this),true);
        bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        this.setHasOptionsMenu(true);
	}
	
	protected void displayDiaryListFragment(Boolean animated, Date date){
		FragmentManager fragmentManager = getActivity().getSupportFragmentManager();		
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		if (animated){
			fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
		}
		
		if (date != null){
			this.listFragment.selectedDate = date;
		}
		
		fragmentTransaction.replace(R.id.diaryFrameLayout, this.listFragment);
		fragmentTransaction.commit();

        if(getActivity().getActionBar().getSelectedTab() != getActivity().getActionBar().getTabAt(1)){
            getActivity().getActionBar().selectTab(getActivity().getActionBar().getTabAt(1));
        }
		

//		DataHelper.setPref(DataHelper.PREFS_DIARY_SELECTED_TAB, 1);
	}
	
	protected void displayDiaryCalendarFragment(Boolean animated, Date date){
		FragmentManager fragmentManager = getActivity().getSupportFragmentManager();		
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		if (animated){
			fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
		}
		if (date != null){
			this.calendarFragment.setDate(date);
		}
				
		fragmentTransaction.replace(R.id.diaryFrameLayout, this.calendarFragment);
		fragmentTransaction.commit();

//		DataHelper.setPref(DataHelper.PREFS_DIARY_SELECTED_TAB, 0);
	}

    @Override
    public void onTabSelected(ActionBar.Tab tab, android.app.FragmentTransaction fragmentTransaction) {
        int buttonId = tab.getPosition();
        switch (buttonId) {
            case 0:
                displayDiaryCalendarFragment(false, listFragment.getSelectedDate());
                break;
            case 1:
                displayDiaryListFragment(false, null);
                break;
        }
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, android.app.FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, android.app.FragmentTransaction fragmentTransaction) {

    }
}
