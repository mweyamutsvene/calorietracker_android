package com.livestrong.calorietracker;

import java.util.Stack;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.livestrong.calorietracker.back.db.ModelManager;
import com.livestrong.calorietracker.configuration.MyPlateDefaults;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class MyPlateApplication extends Application {
	private static Context context;

	// Values that relate to the current session.

	// Detect app backgrounding
	Handler timingHandler = new Handler();
	private boolean backgrounded = true;
	private static Stack<Activity> activityStack = new Stack<Activity>();
	//================================================================================
    // Overrides
    //================================================================================

	@Override
	public void onCreate() {

		//Instantiate Singletons here
		super.onCreate();
		context = this;
		MyPlateDefaults.initialize(context);
		ModelManager.initializeModelManager(context);

        MyPlateDefaults.setPref("test","test");

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://www.google.com", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String response) {
                System.out.println(response);
            }
            @Override
            public void onFailure(java.lang.Throwable throwable, java.lang.String s)
            {
                System.out.println(s);
                Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
            }
        });


	}

	@Override
	public void onTerminate() {
		Log.d("#TADA", "end");
	}

	//================================================================================
    // Accessors
    //================================================================================

	public static Context getContext() {
		return context;
	}
	
	public static Context getFrontMostActivity() {
		if (activityStack.size() == 0) {
			return getContext();
		}
		return activityStack.lastElement();
	}

	//================================================================================
	// Public Background Methods
    // The following methods are use to check if the app is backgrounded,
	// and sync the diary when it is.
    //================================================================================
	
	/*
	 * Pushes an Activity object to the stack of active Activities
	 * 
	 * @param activity the activity to be added to the stack of active Activities
	 */
	public void plusActivity(Activity activity) {
		activityStack.push(activity);
	}

	/*
	 * Pops an Activity object from the stack of active Activities
	 * 
	 */
	public void minusActivity() {
		activityStack.pop();
		//Clear timingHandler Callbacks
		timingHandler.removeCallbacks(checkBackgroundTask);
		
		//Check if we are backgrounded in 2000
		timingHandler.postDelayed(checkBackgroundTask, 2000);
	}

	/*
	 * Determines if the application was in the background
	 * 
	 * @return		boolean of whether this app was in background
	 */
	public boolean wasInBackground() {
		return this.backgrounded;
	}

	/*
	 * Resets the background state
	 * 
	 */
	public void hasBeenLoaded() {
		this.backgrounded = false;
	}
	
	//================================================================================
    // Private Background Methods
    //================================================================================
	private void checkIfBackgrounded() {
		if (activityStack.size() == 0) {
			//App is now backgrounded - no more Activities on stack
			this.backgrounded = true;
            ModelManager.sharedModelManager().synchronizeDataWithServer();

		}
	}
	private Runnable checkBackgroundTask = new Runnable() {
		@Override
		public void run() {
			checkIfBackgrounded();
		}
	};
	
	
}
