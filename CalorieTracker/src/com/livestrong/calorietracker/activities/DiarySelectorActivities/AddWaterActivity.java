package com.livestrong.calorietracker.activities.DiarySelectorActivities;



import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.livestrong.calorietracker.R;
import com.livestrong.calorietracker.activities.CalorieTrackerBaseActivity;
import com.livestrong.calorietracker.activities.TabBarActivity;
import com.livestrong.calorietracker.back.db.ModelManager;
import com.livestrong.calorietracker.back.db.gen.DiaryEntry;
import com.livestrong.calorietracker.back.db.gen.DiaryEntry.*;
import com.livestrong.calorietracker.configuration.MyPlateDefaults;
import com.livestrong.calorietracker.utilities.SimpleDate;
import com.livestrong.calorietracker.utilities.Utils;

import java.util.Date;


public class AddWaterActivity extends CalorieTrackerBaseActivity implements NumberPicker.OnValueChangeListener {
	
	private DiaryEntry diaryEntry;
	private NumberPicker waterPicker;
	MyPlateDefaults.WaterUnits unitsPreference;
    private DiaryEntryCategoryEnum mTimeOfDay;
    private Date mDate;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        if (savedInstanceState != null) {
        	// The activity was destroyed, and is about to be started again. You need to restore your activity state from savedInstanceState.
            // UI elements states are restored automatically by super.onCreate()
        }

        // The activity is being created; create views, bind data to lists, etc.
        setContentView(R.layout.activity_add_water);
        
        this.initializePickers();

        // Fetch WaterDiaryEntry from Intent Extras
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
    		this.diaryEntry = (DiaryEntry) extras.get(DiaryEntry.class.getName());
            this.mTimeOfDay = DiaryEntry.DiaryEntryCategoryEnum.values()[getIntent().getExtras().getInt("TimeOfDay")];
            this.mDate = (Date)getIntent().getExtras().get(Date.class.getName());

            if (this.diaryEntry != null) {
                this.diaryEntry = DiaryEntry.getDiary(this.diaryEntry.getRemoteId(), ModelManager.sharedModelManager().daoSession.getDiaryEntryDao());
                this.mTimeOfDay = DiaryEntry.DiaryEntryCategoryEnum.values()[this.diaryEntry.getCategory()];
                this.mDate = this.diaryEntry.getEntryDate();

                setPickers(this.diaryEntry.getOuncesConsumed());
        	}

        }

        Button iDrankThisButton = (Button) findViewById(R.id.iDrankThisButton);
        Button deleteButton = (Button) findViewById(R.id.deleteButton);

        if (this.diaryEntry != null) {
        	iDrankThisButton.setText("Update");
        	deleteButton.setVisibility(View.VISIBLE);
    	} else {
    		iDrankThisButton.setText("I Drank This");
        	deleteButton.setVisibility(View.GONE);
    	}

        TextView selectedDateTextView = (TextView) findViewById(R.id.timeOfDayTextView);
        selectedDateTextView.setText(Utils.getPrettyDate(mDate));

        final String buttonText = iDrankThisButton.getText().toString();
        iDrankThisButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				
				double ounces = getPickerOunces();

                DiaryEntry waterDiary = ModelManager.getWaterEntryForDate(mDate);
                if(waterDiary == null){
                    waterDiary = new DiaryEntry();
                    waterDiary.setDiaryType(DiaryEntryType.DIARY_WATER_ENTRY.ordinal());
                    waterDiary.setCategory(mTimeOfDay.ordinal());
                    waterDiary.setEntryDate(new SimpleDate(mDate));
                    waterDiary.setDateModified(mDate);
                    waterDiary.setDateCreated(mDate);
                    waterDiary.setOuncesConsumed((float)ounces);
                    waterDiary.setIsSynchronized(false);
                    ModelManager.daoSession.getDiaryEntryDao().insert(waterDiary);
                }
                else{
                    if(buttonText.equals("Update")){
                        waterDiary.setOuncesConsumed((float)(ounces));
                    }
                    else{
                        waterDiary.setOuncesConsumed((float)(ounces)+waterDiary.getOuncesConsumed());
                    }
                    waterDiary.setIsSynchronized(false);
                    ModelManager.daoSession.getDiaryEntryDao().update(waterDiary);
                }

	            finish();
			}
		});

    	deleteButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
                ModelManager.sharedModelManager().deleteDiaryEntry(AddWaterActivity.this.diaryEntry);
	            finish();
			}
		});

        getActionBar().setDisplayHomeAsUpEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getActionBar().setHomeButtonEnabled(true);
        }
    }

    // Initialize menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            Intent intent = new Intent(this, TabBarActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);

    }

	private double getPickerOunces() {
		int waterAmount;
		int selectedIndex = this.waterPicker.getValue();
		switch (this.unitsPreference) {
			case MILLILITERS:
				waterAmount = selectedIndex * 50;
				return waterAmount * DiaryEntry.ML_PER_OUNCE;
			default:
				waterAmount = selectedIndex;
				return (double) waterAmount;
		}
	}

	private void setPickers(double ounces) {
		switch (this.unitsPreference) {
			case MILLILITERS:
				this.waterPicker.setValue((int)ounces/50);
				break;
			default:
				this.waterPicker.setValue((int)ounces);
				break;
		}
	}
	
	private void initializePickers() {
		this.waterPicker = (NumberPicker) findViewById(R.id.waterPicker);
        this.waterPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);


        this.waterPicker.setFocusable(false);
		TextView unitsTv = (TextView) findViewById(R.id.unitsTextView);

		String[] waterValues;
		this.unitsPreference = (MyPlateDefaults.WaterUnits)MyPlateDefaults.getPref(MyPlateDefaults.PREFS_WATER_UNITS, MyPlateDefaults.WaterUnits.OUNCES);
		switch (unitsPreference) {
			case MILLILITERS:
                waterValues = new String[21];
                this.waterPicker.setMinValue(0);
                this.waterPicker.setMaxValue(20);
                for(int i = 0; i<= 20; i++){
                    waterValues[i] = (i*50)+"";
                }
                this.waterPicker.setDisplayedValues(waterValues);
				unitsTv.setText("ml");
                this.waterPicker.setValue(6);

                break;
			default:
                waterValues = new String[101];
                this.waterPicker.setMinValue(0);
                this.waterPicker.setMaxValue(100);
                for(int i = 0; i<= 100; i++){
                    waterValues[i] = (i)+"";
                }
                this.waterPicker.setDisplayedValues(waterValues);
                this.waterPicker.setValue(8);

                unitsTv.setText("ounces");
				break;
		}


    }
	
    @Override
    protected void onStart() {
        super.onStart();
        // The activity is about to become visible.
        // -> onResume()
    }

    @Override
    protected void onResume() {
        super.onResume();
        // The activity has become visible (it is now "resumed").
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    	// Called before making the activity vulnerable to destruction; save your activity state in outState.
        // UI elements states are saved automatically by super.onSaveInstanceState()
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Another activity is taking focus (this activity is about to be "paused"); commit unsaved changes to persistent data, etc.
        // -> onStop()
    }

    @Override
    protected void onStop() {
        super.onStop();
        // The activity is no longer visible (it is now "stopped")
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        // The activity was stopped, and is about to be started again. It was not destroyed, so all members are intact.
        // -> onStart()
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // The activity is about to be destroyed.
        if (isFinishing()) {
        	// Someone called finish()
        } else {
        	// System is temporarily destroying this instance of the activity to save space
        }
    }

    @Override
    public void onValueChange(NumberPicker numberPicker, int i, int i2) {

    }
}
