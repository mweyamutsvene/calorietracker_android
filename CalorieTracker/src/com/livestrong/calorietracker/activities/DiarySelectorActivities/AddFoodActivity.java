package com.livestrong.calorietracker.activities.DiarySelectorActivities;

import java.util.Date;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.*;

import com.livestrong.calorietracker.R;
import com.livestrong.calorietracker.activities.CalorieTrackerBaseActivity;
import com.livestrong.calorietracker.activities.NutritionFactsActivity;
import com.livestrong.calorietracker.activities.TabBarActivity;
import com.livestrong.calorietracker.back.db.ModelManager;
import com.livestrong.calorietracker.back.db.gen.DiaryEntry;
import com.livestrong.calorietracker.back.db.gen.DiaryEntry.DiaryEntryCategoryEnum;

import com.livestrong.calorietracker.back.db.gen.Food;
import com.livestrong.calorietracker.utilities.ImageLoader;
import com.livestrong.calorietracker.utilities.SimpleDate;
import com.livestrong.calorietracker.utilities.Utils;


public class AddFoodActivity extends CalorieTrackerBaseActivity implements NumberPicker.OnValueChangeListener {
	
	public static String INTENT_FOOD_NAME = "foodName";
	
	private Food food;
	private DiaryEntry diaryEntry;
	private NumberPicker servingsPicker, servingsFractionPicker;
	private Button iAteThisButton;
	private ProgressBar progressBar;
    private int mCurrentServings;
    private DiaryEntryCategoryEnum mTimeOfDay;
    private Date mDate;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        if (savedInstanceState != null) {
        	// The activity was destroyed, and is about to be started again. You need to restore your activity state from savedInstanceState.
            // UI elements states are restored automatically by super.onCreate()
        }

        // The activity is being created; create views, bind data to lists, etc.
        setContentView(R.layout.activity_add_food);
        
        this.iAteThisButton = (Button) findViewById(R.id.iAteThisButton);
        this.progressBar = (ProgressBar) findViewById(R.id.progressBar);
        
        // Fetch Food from Intent Extras
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
        	this.food = (Food) extras.get(Food.class.getName());
            this.mTimeOfDay = DiaryEntry.DiaryEntryCategoryEnum.values()[getIntent().getExtras().getInt("TimeOfDay")];
            this.mDate = (Date)getIntent().getExtras().get(Date.class.getName());
            if(mDate == null){
                this.mDate = new Date();
            }
            // smallFood has the following data members populated: item_title,cals,serving_size,food_id,images.100,images,verification
        	
        	if (this.food != null) {
        		this.diaryEntry = null;
        		if (food.isCustom()) {
				 } else {
					 this.iAteThisButton.setVisibility(View.INVISIBLE);
					 this.progressBar.setVisibility(View.VISIBLE);
				 }
                updateFoodInfo(food);

            } else {
        		this.diaryEntry = (DiaryEntry) extras.get(DiaryEntry.class.getName());
       		    this.diaryEntry = DiaryEntry.getDiary(this.diaryEntry.getRemoteId(), ModelManager.sharedModelManager().daoSession.getDiaryEntryDao());
        		this.food = this.diaryEntry.getFood();
                this.mDate = this.diaryEntry.getEntryDate();

                this.mTimeOfDay = DiaryEntry.DiaryEntryCategoryEnum.values()[this.diaryEntry.getCategory()];
                updateFoodInfo(food);
        	}


            ImageLoader imageLoader = new ImageLoader(this);
	    	ImageView imageView = (ImageView)findViewById(R.id.foodImageView);
	    	// Load food image
			switch (mTimeOfDay) {
				case CTDiaryEntryTypeBreakfast:
					imageView.setImageResource(R.drawable.icon_breakfast);
					break;
				case CTDiaryEntryTypeLunch:
					imageView.setImageResource(R.drawable.icon_lunch);				
					break;
				case CTDiaryEntryTypeDinner:
					imageView.setImageResource(R.drawable.icon_dinner);
					break;
				case CTDiaryEntryTypeSnacks:
					imageView.setImageResource(R.drawable.icon_snacks);
					break;
			}
	    	imageLoader.DisplayImage(this.food.getSmallImage(), imageView);
        	
	    	// Load textViews
	    	TextView tv;
	    	if (this.diaryEntry != null) {
	        	tv = (TextView)findViewById(R.id.foodNameTextView);
		    	tv.setText(this.diaryEntry.getTitle());
		    	
		    	tv = (TextView) findViewById(R.id.foodDescriptionTextView);
		    	tv.setText(this.diaryEntry.getDescription());

		    	tv = (TextView) findViewById(R.id.caloriesTextView);
		    	tv.setText(Math.round(this.diaryEntry.getCalories())+"");
		    	
		    	
	    	} else {
	        	tv = (TextView)findViewById(R.id.foodNameTextView);
		    	tv.setText(this.food.getTitle());
		    	
		    	tv = (TextView) findViewById(R.id.foodDescriptionTextView);
		    	tv.setText(this.food.getDescription());

		    	tv = (TextView) findViewById(R.id.caloriesTextView);
		    	tv.setText(this.food.getCalories()+"");
	    	}

	    	tv = (TextView) findViewById(R.id.servingSizeTextView);
	    	tv.setText(this.food.getServingSize());
	    	
	    	tv = (TextView) findViewById(R.id.dateTextView);
	    	tv.setText(Utils.getPrettyDate(mDate));
	    	
	    	tv = (TextView) findViewById(R.id.timeOfDayTextView);
	    	tv.setText(Utils.getPrettyDate(mDate));

	        this.initializeButtons();       
        }
        getActionBar().setDisplayHomeAsUpEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getActionBar().setHomeButtonEnabled(true);
        }
    }

    // Initialize menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            Intent intent = new Intent(this, TabBarActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);

    }

	private void initializeButtons(){
        Button deleteButton = (Button) findViewById(R.id.deleteButton);
        Button nutritionFactsButton = (Button) findViewById(R.id.nutritionFactsButton);

		if (this.diaryEntry != null) {
			iAteThisButton.setText("Update");
        	deleteButton.setVisibility(View.VISIBLE);
    	} else {
    		iAteThisButton.setText("I Ate This");
        	deleteButton.setVisibility(View.GONE);
    	}

		//Create new Diary option
		iAteThisButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
                //Create new dialog for creating diary
				final Dialog dialog = new Dialog(AddFoodActivity.this);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(R.layout.dialog_add_food);

                //Get current servings if we are modifying a diary Entry
				AddFoodActivity.this.initializePickers(dialog);
				if (AddFoodActivity.this.diaryEntry != null){
					setPickers(AddFoodActivity.this.diaryEntry.getServings());
				}

				Button doneBtn = (Button) dialog.findViewById(R.id.doneButton);
				doneBtn.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						double pickerServings = getPickerServings();
						if (AddFoodActivity.this.diaryEntry == null){
                            //Create new Entry
							if (pickerServings > 0.0){
                                DiaryEntry foodDiaryEntry = new DiaryEntry();
                                foodDiaryEntry.setFood(food);
                                foodDiaryEntry.setDiaryType(DiaryEntry.DiaryEntryType.DIARY_FOOD_ENTRY.ordinal());
                                foodDiaryEntry.setIsSynchronized(false);
                                foodDiaryEntry.setCategory(mTimeOfDay.ordinal());
                                foodDiaryEntry.setServings((float)pickerServings);
                                foodDiaryEntry.setEntryDate(new SimpleDate(mDate));
                                foodDiaryEntry.setDateModified(mDate);
                                foodDiaryEntry.setDateCreated(mDate);
								ModelManager.daoSession.getDiaryEntryDao().insert(foodDiaryEntry);

                                foodDiaryEntry.updateConsumption();
                                ModelManager.daoSession.getDiaryEntryDao().update(foodDiaryEntry);
                            }
						} else {
                            //Modify Current Entry
							if (pickerServings == 0.0){
                                //Delete Entry
                                ModelManager.sharedModelManager().deleteDiaryEntry(diaryEntry);
							} else {
                                //Update Entry
                                AddFoodActivity.this.diaryEntry.setServings((float)pickerServings);
                                AddFoodActivity.this.diaryEntry.updateConsumption();
                                AddFoodActivity.this.diaryEntry.setIsSynchronized(false);

                                ModelManager.sharedModelManager().updateDiaryEntry(diaryEntry);
							}
						}
                        //Add result intent
                        if (pickerServings > 0.0){
							Intent resultIntent = new Intent();
							resultIntent.putExtra(AddFoodActivity.INTENT_FOOD_NAME, AddFoodActivity.this.food.getTitle());
							setResult(Activity.RESULT_OK, resultIntent);
						}

                        //Finish Activity
	    	            dialog.cancel();
	    	            finish();
					}
				});

                //Handle cancel
				Button cancelBtn = (Button) dialog.findViewById(R.id.cancelButton);
				cancelBtn.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.cancel();
					}
				});

				dialog.show();
			}
		});
		
    	deleteButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
                ModelManager.sharedModelManager().deleteDiaryEntry(AddFoodActivity.this.diaryEntry);
	            finish();
			}
		});

        nutritionFactsButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(AddFoodActivity.this, NutritionFactsActivity.class);
				intent.putExtra(AddFoodActivity.this.food.getClass().getName(), AddFoodActivity.this.food);
				startActivity(intent);
			}
		});
	}
	
	private double getPickerServings() {
		int selectedIndex = this.servingsPicker.getValue();
		Integer servings = (Integer) DiaryEntry.servingsPickerValues.values().toArray()[selectedIndex];

		selectedIndex = this.servingsFractionPicker.getValue();
		Double servingsFraction = (Double) DiaryEntry.servingsFractionPickerValues.values().toArray()[selectedIndex];

		return servings + servingsFraction;
	}

	private void setPickers(double servings) {
		this.servingsPicker.setValue(this.diaryEntry.getServingsWholeIndex());
		this.servingsFractionPicker.setValue(this.diaryEntry.getServingsFractionIndex());
	}

	private void initializePickers(Dialog dialog){
		// Hook up outlets
		this.servingsPicker = (NumberPicker) dialog.findViewById(R.id.servingsPicker);
        this.servingsFractionPicker = (NumberPicker) dialog.findViewById(R.id.servingsFractionPicker);

		String[] servingValues = DiaryEntry.servingsPickerValues.keySet().toArray(new String[DiaryEntry.servingsPickerValues.size()]);


        this.servingsPicker.setMaxValue(servingValues.length - 1);
        this.servingsPicker.setMinValue(0);
        this.servingsPicker.setValue(1);
        this.servingsPicker.setDisplayedValues(servingValues);
        this.servingsPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        this.servingsPicker.setOnValueChangedListener(this);

        String[] servingFractionValues = DiaryEntry.servingsFractionPickerValues.keySet().toArray(new String[diaryEntry.servingsFractionPickerValues.size()]);
        this.servingsFractionPicker.setMaxValue(servingFractionValues.length - 1);
        this.servingsFractionPicker.setMinValue(0);
        this.servingsFractionPicker.setDisplayedValues(servingFractionValues);
        this.servingsFractionPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        this.servingsFractionPicker.setOnValueChangedListener(this);

	}
	
    @Override
    protected void onStart() {
        super.onStart();
        // The activity is about to become visible.
        // -> onResume()
    }

    @Override
    protected void onResume() {
        super.onResume();
        // The activity has become visible (it is now "resumed").
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    	// Called before making the activity vulnerable to destruction; save your activity state in outState.
        // UI elements states are saved automatically by super.onSaveInstanceState()
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Another activity is taking focus (this activity is about to be "paused"); commit unsaved changes to persistent data, etc.
        // -> onStop()
    }

    @Override
    protected void onStop() {
        super.onStop();
        // The activity is no longer visible (it is now "stopped")
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        // The activity was stopped, and is about to be started again. It was not destroyed, so all members are intact.
        // -> onStart()
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // The activity is about to be destroyed.
        if (isFinishing()) {
        	// Someone called finish()
        } else {
        	// System is temporarily destroying this instance  the activityupdateFoodInfocFoodride
        }
    }
    public void updateFoodInfo(Food food) {
        this.iAteThisButton.setVisibility(View.VISIBLE);
        this.progressBar.setVisibility(View.INVISIBLE);

        this.food = food;

        TextView tv = (TextView)findViewById(R.id.fromCalsTextView);
        tv.setText(this.food.getCaloriesFromFat() + "");
        tv =(TextView)findViewById(R.id.fatTextView);
        tv.setText(Math.round(this.food.getFat())+"");
        tv = (TextView)findViewById(R.id.carbsTextView);
        tv.setText(Math.round(this.food.getCarbs())+"");
        tv = (TextView)findViewById(R.id.proteinTextView);
        tv.setText(Math.round(this.food.getProtein())+"");

    }

    @Override
    public void onValueChange(NumberPicker numberPicker, int i, int i2) {

    }


//	@Override
//	public void onChanged(NumberPicker picker, int oldVal, int newVal) {
//
//		if(picker == servingsPicker)
//		{
//			if(newVal == 0)
//			{
//				//this.servingsFractionPicker.setCurrent(1);
//			}
//			_currentServings = newVal;
//		}
//		if(_currentServings  == 0 && picker == servingsFractionPicker)
//		{
//			if(newVal == 0)
//			{
//				//this.servingsFractionPicker.setCurrent(1);
//			}
//		}
//	}
}
