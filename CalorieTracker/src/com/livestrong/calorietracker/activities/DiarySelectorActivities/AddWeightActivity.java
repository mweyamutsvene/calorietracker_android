package com.livestrong.calorietracker.activities.DiarySelectorActivities;

import java.text.DecimalFormat;
import java.util.Date;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.livestrong.calorietracker.R;
import com.livestrong.calorietracker.activities.CalorieTrackerBaseActivity;
import com.livestrong.calorietracker.activities.TabBarActivity;
import com.livestrong.calorietracker.back.db.ModelManager;
import com.livestrong.calorietracker.back.db.gen.DiaryEntry;
import com.livestrong.calorietracker.configuration.MyPlateDefaults;
import com.livestrong.calorietracker.utilities.SimpleDate;
import com.livestrong.calorietracker.utilities.Utils;

public class AddWeightActivity extends CalorieTrackerBaseActivity {
	
	private DiaryEntry diaryEntry;
    MyPlateDefaults.WeightUnits unitsPreference;
    private Date mDate;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        if (savedInstanceState != null) {
        	// The activity was destroyed, and is about to be started again. You need to restore your activity state from savedInstanceState.
            // UI elements states are restored automatically by super.onCreate()
        }

        // The activity is being created; create views, bind data to lists, etc.
        setContentView(R.layout.activity_add_weight);

        // Fetch WaterDiaryEntry from Intent Extras
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
    		this.diaryEntry = (DiaryEntry) extras.get(DiaryEntry.class.getName());
            this.mDate = (Date)getIntent().getExtras().get(Date.class.getName());

        }

        final EditText weightEditText = (EditText) findViewById(R.id.weightEditText);
        TextView weightUnitsTextView = (TextView) findViewById(R.id.weightUnitsTextView);
        this.unitsPreference = (MyPlateDefaults.WeightUnits)(MyPlateDefaults.getPref(MyPlateDefaults.PREFS_WEIGHT_UNITS, MyPlateDefaults.WeightUnits.POUNDS));
		if (unitsPreference == MyPlateDefaults.WeightUnits.KILOGRAMS){
			weightUnitsTextView.setText("kg");
		} else if (unitsPreference == MyPlateDefaults.WeightUnits.POUNDS){
			weightUnitsTextView.setText("lbs");
		} else {
			weightUnitsTextView.setText("st");
		}
		
        Button trackButton = (Button) findViewById(R.id.trackButton);
        Button deleteButton = (Button) findViewById(R.id.deleteButton);

        if (this.diaryEntry != null) {
        	trackButton.setText("Update Weight");
        	deleteButton.setVisibility(View.VISIBLE);
        	DecimalFormat decimalFormat = new DecimalFormat("#.##");
        	weightEditText.setText(decimalFormat.format(this.diaryEntry.getWeightForSelectedUnits()));
            this.mDate = this.diaryEntry.getEntryDate();

        } else {
    		trackButton.setText("Track Weight");
        	deleteButton.setVisibility(View.GONE);
    	}

        TextView selectedDateTextView = (TextView) findViewById(R.id.timeOfDayTextView);
        selectedDateTextView.setText(Utils.getPrettyDate(mDate));
        
        trackButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				String weightString = weightEditText.getText().toString();
				if (weightString.equals("")){
					weightString = "0";
				}
				double weight = Double.parseDouble(weightString);

		    	// Not coming from the Diary; but maybe we already have a Weight diary entry for this day?
				DiaryEntry weightEntry = ModelManager.getWeightEntryForDate(mDate);
                if(weightEntry == null){
                    weightEntry = new DiaryEntry();
                    weightEntry.setDiaryType(DiaryEntry.DiaryEntryType.DIARY_WEIGHT_ENTRY.ordinal());
                    weightEntry.setCategory(DiaryEntry.DiaryEntryCategoryEnum.CTDiaryEntryTypeWeighIn.ordinal());
                    weightEntry.setEntryDate(new SimpleDate(mDate));
                    weightEntry.setDateModified(mDate);
                    weightEntry.setDateCreated(mDate);
                    weightEntry.setWeight((float)weight);
                    weightEntry.setIsSynchronized(false);
                    ModelManager.daoSession.getDiaryEntryDao().insert(weightEntry);
                }
                else{
                    weightEntry.setIsSynchronized(false);
                    weightEntry.setWeight((float) (weight));
                    ModelManager.daoSession.getDiaryEntryDao().update(weightEntry);
                }

                ModelManager.sharedModelManager().updateWeightWithLatestEntry();
				setResult(Activity.RESULT_OK);
	            finish();
			}
		});

    	deleteButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
	            ModelManager.sharedModelManager().deleteDiaryEntry(AddWeightActivity.this.diaryEntry);
	            finish();
			}
		});

        getActionBar().setDisplayHomeAsUpEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getActionBar().setHomeButtonEnabled(true);
        }
    }

    // Initialize menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            Intent intent = new Intent(this, TabBarActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);

    }
	
    @Override
    protected void onStart() {
        super.onStart();
        // The activity is about to become visible.
        // -> onResume()
    }

    @Override
    protected void onResume() {
        super.onResume();
        // The activity has become visible (it is now "resumed").
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    	// Called before making the activity vulnerable to destruction; save your activity state in outState.
        // UI elements states are saved automatically by super.onSaveInstanceState()
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Another activity is taking focus (this activity is about to be "paused"); commit unsaved changes to persistent data, etc.
        // -> onStop()
    }

    @Override
    protected void onStop() {
        super.onStop();
        // The activity is no longer visible (it is now "stopped")
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        // The activity was stopped, and is about to be started again. It was not destroyed, so all members are intact.
        // -> onStart()
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // The activity is about to be destroyed.
        if (isFinishing()) {
        	// Someone called finish()
        } else {
        	// System is temporarily destroying this instance of the activity to save space
        }
    }
}
