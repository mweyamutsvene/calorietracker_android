package com.livestrong.calorietracker.activities.DiarySelectorActivities;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.livestrong.calorietracker.MyPlateApplication;
import com.livestrong.calorietracker.R;
import com.livestrong.calorietracker.Views.ClearableEditText;
import com.livestrong.calorietracker.activities.CalorieTrackerBaseActivity;
import com.livestrong.calorietracker.activities.TabBarActivity;
import com.livestrong.calorietracker.adapters.ExerciseSearchSelectorAdaptor;
import com.livestrong.calorietracker.adapters.ExerciseSelectorAdapter;
import com.livestrong.calorietracker.animations.DropDownAnimation;
import com.livestrong.calorietracker.back.api.CTWebserviceRequest;
import com.livestrong.calorietracker.back.db.gen.DiaryEntry;
import com.livestrong.calorietracker.back.db.gen.Exercise;

import java.util.Date;


public class ExerciseSelectorActivity extends CalorieTrackerBaseActivity implements OnItemClickListener, ActionBar.TabListener {
	
	ListView list;
	private ExerciseSelectorAdapter exerciseSelectorAdapter;
    private ExerciseSearchSelectorAdaptor searchSelectorAdapter;

    private EditText searchEditText;
	private LinearLayout toolBarContainer, messageContainer;
	private int toolBarHeight;
	private Button  addManualButton;
	private TextView messageTextView;
	private Boolean isSearching = false;
	private Date mDate;
	protected void onCreate(Bundle savedInstanceState){
		 super.onCreate(savedInstanceState);
	        
        if (savedInstanceState != null) {
        	// The activity was destroyed, and is about to be started again. You need to restore your activity state from savedInstanceState.
            // UI elements states are restored automatically by super.onCreate()
        }

        // The activity is being created; create views, bind data to lists, etc.
        setContentView(R.layout.activity_exercise_selector);
        
        // Hook up outlets
        this.list 				    = (ListView)findViewById(R.id.listView);
        this.searchEditText 	    = ((ClearableEditText)findViewById(R.id.edit_text_clearable)).getEditText();
        this.toolBarContainer	    = (LinearLayout)findViewById(R.id.toolBarContainer);
        this.addManualButton		= (Button)findViewById(R.id.addManualButton);
        this.messageTextView 		= (TextView)findViewById(R.id.messageTextView);
        this.messageContainer 		= (LinearLayout)findViewById(R.id.messageContainer);

        this.mDate = (Date)getIntent().getExtras().get(Date.class.getName());

        // Initialize list view
        this.list.setOnItemClickListener(this);
        this.exerciseSelectorAdapter = new ExerciseSelectorAdapter(this, null){
            @Override
            public void notifyDataSetChanged(){
                super.notifyDataSetChanged();
                if (ExerciseSelectorActivity.this.exerciseSelectorAdapter == null){
                    return;
                }

                if (ExerciseSelectorActivity.this.exerciseSelectorAdapter.getCount() == 0){
                    ExerciseSelectorActivity.this.messageContainer.setVisibility(View.VISIBLE);
                } else {
                    ExerciseSelectorActivity.this.messageContainer.setVisibility(View.INVISIBLE);
                }
            }
        };
        this.list.setAdapter(this.exerciseSelectorAdapter);
        this.list.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyboard();
                return false;
            }
        });
        this.searchSelectorAdapter = new ExerciseSearchSelectorAdaptor(this, null);

        ActionBar bar = getActionBar();
        bar.setDisplayHomeAsUpEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            bar.setHomeButtonEnabled(true);
        }

        bar.removeAllTabs();
        bar.addTab(bar.newTab().setText(R.string.recentlyPerformed).setTabListener(this));
        bar.addTab(bar.newTab().setText(R.string.frequentlyPerformed).setTabListener(this));
        bar.addTab(bar.newTab().setText(R.string.custom_exercises).setTabListener(this));
        bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);



        this.initializeSearchBar();
        this.initializeButtons();
        loadListFromSelectedButton();

    }
    @Override
    public void onResume() {
        super.onResume();
    }
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if (resultCode == Activity.RESULT_OK){
			if (this.searchEditText.getText().length() > 0){
				this.searchEditText.setText("");
			} else {
				loadListFromSelectedButton();
			}
			
			String exerciseName = data.getExtras().getString(AddExerciseActivity.INTENT_EXERCISE_NAME);
			this.displayNotification(exerciseName + " was added to your diary.");
			
			setResult(Activity.RESULT_OK);
		}
	}

	// Initialize menu
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		
		menu.add(0, 1, 0, "Add Exercise Manually").setIcon(android.R.drawable.ic_menu_add);
		
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            Intent intent = new Intent(this, TabBarActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return true;
        }


		
		this.showCreateExerciseActivity();
		
		return super.onOptionsItemSelected(item);
	}
	
	private void showCreateExerciseActivity(){
		Intent intent = new Intent(this, CreateExerciseActivity.class);
        intent.putExtra(Date.class.getName(),this.mDate);
        intent.putExtra("TimeOfDay", DiaryEntry.DiaryEntryCategoryEnum.CTDiaryEntryTypeExercise);

        startActivityForResult(intent, 1);
	}
	
	private void initializeSearchBar(){
		this.searchEditText.setHint("Search for exercise...");
		this.searchEditText.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
		
        this.searchEditText.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {				
				if (s.length() == 0){
					// Hide toolbar when field is empty
					showToolBar();
                    getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

                    // Reload list with the appropriate data depending on the selected button
                    list.setAdapter(exerciseSelectorAdapter);
                    exerciseSelectorAdapter.loadExercisesFromLocalSearch("");
                    loadListFromSelectedButton();

                    isSearching = false;
				} else {
					hideToolBar();
                    getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
                    list.setAdapter(searchSelectorAdapter);

                    isSearching = true;
					// Perform local exercise search
					String searchStr = String.valueOf(s);
					ExerciseSelectorActivity.this.searchSelectorAdapter.loadExerciseFromLocalSearch(searchStr);
				}
			}

			@Override
			public void afterTextChanged(Editable s) {	}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
		});
        
        this.searchEditText.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {				
				if (event.getAction() == KeyEvent.ACTION_DOWN) {
					if (keyCode == KeyEvent.KEYCODE_ENTER) {
                        list.setAdapter(exerciseSelectorAdapter);
                        if(searchEditText.getText().length() == 0){
                            hideKeyboard();
                            return true;
                        }
                        // Search for value in Edit Field
						performServerSearch();
						
						// Hide Keyboard
						hideKeyboard();
						
						return true;
					} 
				}
				return false;
			}
        });
	}
	
	private void initializeButtons(){
		
		this.addManualButton.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				showCreateExerciseActivity();
			}
		});
	}
	
	private void loadListFromSelectedButton(){
        switch (getActionBar().getSelectedTab().getPosition()){
            case 0: this.exerciseSelectorAdapter.loadRecentlyPerformed();break;
            case 1: this.exerciseSelectorAdapter.loadFrequentlyPerformed(); break;
            case 2: this.exerciseSelectorAdapter.loadCustomExercises();break;
        }

		
		if (this.exerciseSelectorAdapter.getCount() == 0){
			this.messageContainer.setVisibility(View.VISIBLE);
		} else {
			this.messageContainer.setVisibility(View.INVISIBLE);
		}
	}

	private void performServerSearch(){
		if (CTWebserviceRequest.isOnline(MyPlateApplication.getContext())){
			String searchString = searchEditText.getText().toString();
			this.exerciseSelectorAdapter.loadExercisesFromServerSearch(searchString);
		}
	}
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
        if(this.list.getAdapter() == this.searchSelectorAdapter){
            searchEditText.setText((String)this.searchSelectorAdapter.getItem(position));
            list.setAdapter(exerciseSelectorAdapter);
            this.performServerSearch();
            return;
        }

        if (this.exerciseSelectorAdapter.showNoResultsMessage || this.exerciseSelectorAdapter.isLoading()){
			return;
		}
		
		// If user is searchign
		if (this.isSearching){
			Object object = this.exerciseSelectorAdapter.getItem(position);
			if (object == null && this.exerciseSelectorAdapter.isShowingSearchOnlinePrompt()){ // If user selects online search list item
				this.performServerSearch();
				return;
			} 
		}
	
		// Regular exercise item selected
		Exercise exercise = (Exercise) this.exerciseSelectorAdapter.getItem(position);
		Intent intent = new Intent(this, AddExerciseActivity.class);
		intent.putExtra(exercise.getClass().getName(), exercise);
        intent.putExtra(Date.class.getName(),this.mDate);

        startActivityForResult(intent, 1);
	}
	
	public void hideToolBar() {
		if (this.toolBarContainer.getHeight() == 0){
			return;
		}
		
		isSearching = true;
		
		this.toolBarHeight = this.toolBarContainer.getHeight();
		DropDownAnimation animation = new DropDownAnimation(this.toolBarContainer, 0);
		this.toolBarContainer.startAnimation(animation);

        this.messageContainer.setVisibility(View.INVISIBLE);

    }
	
	public void showToolBar() {
		Log.d("ExerciseSelector","showToolbar");
		if (this.toolBarContainer.getHeight() != 0){
			return;
		}
		
		isSearching = false;
		
		DropDownAnimation animation = new DropDownAnimation(this.toolBarContainer, this.toolBarHeight);
		this.toolBarContainer.startAnimation(animation);
	}
	
	private void hideKeyboard(){
		// Hide Keyboard
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(searchEditText.getWindowToken(), 0);
		searchEditText.clearFocus();
	}
	
	private void displayNotification(String notificationString){
		Context context = getApplicationContext();
		int duration = Toast.LENGTH_SHORT;
		
		Toast toast = Toast.makeText(context, notificationString, duration);
		toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 0);
		toast.show();
	}

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        int btnId = tab.getPosition();
        switch (btnId){
            case 0:
                exerciseSelectorAdapter.loadRecentlyPerformed();
                messageTextView.setText("No recently performed exercises.");
                break;
            case 1:
                exerciseSelectorAdapter.loadFrequentlyPerformed();
                messageTextView.setText("No frequently performed exercises.");
                break;
            case 2:
                exerciseSelectorAdapter.loadCustomExercises();
                messageTextView.setText("No custom exercises.");
                break;


        }
        loadListFromSelectedButton();
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }
}