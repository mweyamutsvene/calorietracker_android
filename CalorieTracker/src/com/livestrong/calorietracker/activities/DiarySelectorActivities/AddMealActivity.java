package com.livestrong.calorietracker.activities.DiarySelectorActivities;

import java.util.Collection;
import java.util.Date;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;

import com.livestrong.calorietracker.R;
import com.livestrong.calorietracker.activities.CalorieTrackerBaseActivity;
import com.livestrong.calorietracker.activities.WelcomeActivity;
import com.livestrong.calorietracker.adapters.MealListAdapter;
import com.livestrong.calorietracker.back.db.ModelManager;
import com.livestrong.calorietracker.back.db.gen.DiaryEntry;
import com.livestrong.calorietracker.back.db.gen.DiaryEntry.*;
import com.livestrong.calorietracker.back.db.gen.Meal;
import com.livestrong.calorietracker.back.db.gen.MealItem;
import com.livestrong.calorietracker.utilities.SimpleDate;


public class AddMealActivity extends CalorieTrackerBaseActivity {
	
	private Meal meal;
    private DiaryEntryCategoryEnum mTimeOfDay;
    private Date mDate;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        if (savedInstanceState != null) {
        	// The activity was destroyed, and is about to be started again. You need to restore your activity state from savedInstanceState.
            // UI elements states are restored automatically by super.onCreate()
        }

        // The activity is being created; create views, bind data to lists, etc.
        setContentView(R.layout.activity_add_meal);

        // Fetch Meal from Intent Extras
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
        	Meal mealObject = (Meal) extras.get(Meal.class.getName());
            this.meal = Meal.getMeal(mealObject.getRemoteId(),ModelManager.daoSession.getMealDao());
            this.mDate = (Date)getIntent().getExtras().get(Date.class.getName());

        }
        this.mTimeOfDay = DiaryEntryCategoryEnum.values()[getIntent().getExtras().getInt("TimeOfDay")];

        MealListAdapter listAdapter = new MealListAdapter(this, this.meal,mTimeOfDay);
        ListView listView = (ListView) findViewById(R.id.mealListView);
        listView.setAdapter(listAdapter);
        
        Button iAteThisButton = (Button) findViewById(R.id.doneButton);
        iAteThisButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View buttonView) {
				if (AddMealActivity.this.meal != null){
					Collection<MealItem> mealItems = AddMealActivity.this.meal.getMealItemList();
					
					//Date workingDate = MyPlateApplication.getWorkingDateStamp();
					
					for (MealItem item: mealItems){
                        DiaryEntry foodDiaryEntry = new DiaryEntry();
                        foodDiaryEntry.setFood(item.getFood());
                        foodDiaryEntry.setDiaryType(DiaryEntry.DiaryEntryType.DIARY_FOOD_ENTRY.ordinal());
                        foodDiaryEntry.setCategory(mTimeOfDay.ordinal());
                        foodDiaryEntry.setServings(item.getServings());
                        foodDiaryEntry.setEntryDate(new SimpleDate(mDate));
                        foodDiaryEntry.setDateModified(mDate);
                        foodDiaryEntry.setDateCreated(mDate);
                        foodDiaryEntry.setIsSynchronized(false);
                        ModelManager.daoSession.getDiaryEntryDao().insert(foodDiaryEntry);

                        foodDiaryEntry.updateConsumption();
                        ModelManager.daoSession.getDiaryEntryDao().update(foodDiaryEntry);
					}
					
					Intent resultIntent = new Intent();
					resultIntent.putExtra(AddFoodActivity.INTENT_FOOD_NAME, meal.getTitle());
					
					setResult(Activity.RESULT_OK, resultIntent);
					
    	            finish();	
				}			
			}
		});
        getActionBar().setDisplayHomeAsUpEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getActionBar().setHomeButtonEnabled(true);
        }
    }

    // Initialize menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            Intent intent = new Intent(this, WelcomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);

    }
	
    @Override
    protected void onStart() {
        super.onStart();
        // The activity is about to become visible.
        // -> onResume()
    }

    @Override
    protected void onResume() {
        super.onResume();
        // The activity has become visible (it is now "resumed").
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    	// Called before making the activity vulnerable to destruction; save your activity state in outState.
        // UI elements states are saved automatically by super.onSaveInstanceState()
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Another activity is taking focus (this activity is about to be "paused"); commit unsaved changes to persistent data, etc.
        // -> onStop()
    }

    @Override
    protected void onStop() {
        super.onStop();
        // The activity is no longer visible (it is now "stopped")
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        // The activity was stopped, and is about to be started again. It was not destroyed, so all members are intact.
        // -> onStart()
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // The activity is about to be destroyed.
        if (isFinishing()) {
        	// Someone called finish()
        } else {
        	// System is temporarily destroying this instance of the activity to save space
        }
    }
}
