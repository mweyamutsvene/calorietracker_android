package com.livestrong.calorietracker.activities.DiarySelectorActivities;

import java.util.Date;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;


import com.livestrong.calorietracker.R;
import com.livestrong.calorietracker.activities.CalorieTrackerBaseActivity;
import com.livestrong.calorietracker.activities.TabBarActivity;
import com.livestrong.calorietracker.back.db.ModelManager;
import com.livestrong.calorietracker.back.db.gen.DiaryEntry;
import com.livestrong.calorietracker.back.db.gen.Exercise;
import com.livestrong.calorietracker.utilities.ImageLoader;
import com.livestrong.calorietracker.utilities.SimpleDate;
import com.livestrong.calorietracker.utilities.Utils;


public class AddExerciseActivity extends CalorieTrackerBaseActivity implements NumberPicker.OnValueChangeListener {
	
	public static String INTENT_EXERCISE_NAME = "exerciseName";
	
	private Exercise exercise;
	private DiaryEntry diaryEntry;
	private NumberPicker hoursPicker, minutesPicker;
	private TextView caloriesTextView;
    private Date mDate;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        if (savedInstanceState != null) {
        	// The activity was destroyed, and is about to be started again. You need to restore your activity state from savedInstanceState.
            // UI elements states are restored automatically by super.onCreate()
        }

        // The activity is being created; create views, bind data to lists, etc.
        setContentView(R.layout.activity_add_exercise);

        this.initializePickers();
        
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
        	this.exercise = (Exercise) extras.get(Exercise.class.getName());
        	this.caloriesTextView = (TextView)findViewById(R.id.caloriesTextView);
            this.mDate = (Date)getIntent().getExtras().get(Date.class.getName());

            if (this.exercise != null) {
        		this.diaryEntry = null;
		    	caloriesTextView.setText(Math.round(this.exercise.getCalsPerHour()) + "");
        	} else {
        		this.diaryEntry = (DiaryEntry) extras.get(DiaryEntry.class.getName());
                this.diaryEntry = DiaryEntry.getDiary(this.diaryEntry.getRemoteId(), ModelManager.sharedModelManager().daoSession.getDiaryEntryDao());
                this.mDate = this.diaryEntry.getEntryDate();

                this.exercise = this.diaryEntry.getExercise();
        		setPickers((int) Math.round(this.diaryEntry.getDuration()));
        		caloriesTextView.setText(this.diaryEntry.getCalories().intValue() + "");
        	}

	    	TextView tv = (TextView) findViewById(R.id.exerciseNameTextView);
	    	if (this.diaryEntry != null) {
		    	tv.setText(this.diaryEntry.getTitle());
	    	} else {
		    	tv.setText(this.exercise.getTitle());
	    	}
	    	
	    	tv = (TextView) findViewById(R.id.descriptionTextView);
	    	if (this.diaryEntry != null) {
		    	tv.setText(this.diaryEntry.getCalories().intValue() + " calories per hour");
	    	} else {
		    	tv.setText(this.exercise.getCalsPerHourWithUnits());
	    	}

	    	tv = (TextView) findViewById(R.id.timeOfDayTextView);
            tv.setText(Utils.getPrettyDate(mDate));

	    	ImageLoader imageLoader = new ImageLoader(this);
	    	ImageView imageView = (ImageView)findViewById(R.id.foodImageView);
	    	imageView.setImageResource(R.drawable.icon_fitness);			
	    	imageLoader.DisplayImage(this.exercise.getSmallImage(), imageView);
	    	
	        Button iDidThisButton = (Button) findViewById(R.id.iDidThisButton);
	        Button deleteButton = (Button) findViewById(R.id.deleteButton);

	        if (this.diaryEntry != null) {
	        	iDidThisButton.setText("Update");
	        	deleteButton.setVisibility(View.VISIBLE);
	    	} else {
	    		iDidThisButton.setText("I Did This");
	        	deleteButton.setVisibility(View.GONE);
	    	}

	        iDidThisButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {

					double pickerMinutes = getPickersMinutes();

					if (AddExerciseActivity.this.diaryEntry != null) {
						if (pickerMinutes == 0.0){
							//DataHelper.deleteDiaryEntry(AddExerciseActivity.this.diaryEntry, AddExerciseActivity.this);
						} else {
							AddExerciseActivity.this.diaryEntry.setDuration(getPickersMinutes());
                            AddExerciseActivity.this.diaryEntry.updateConsumption();
                            AddExerciseActivity.this.diaryEntry.setIsSynchronized(false);

                            ModelManager.daoSession.getDiaryEntryDao().update(AddExerciseActivity.this.diaryEntry);

                        }
					} else {
                        DiaryEntry exerciseDiaryEntry = new DiaryEntry();
                        exerciseDiaryEntry.setDiaryType(DiaryEntry.DiaryEntryType.DIARY_EXERCISE_ENTRY.ordinal());
                        exerciseDiaryEntry.setCategory(DiaryEntry.DiaryEntryCategoryEnum.CTDiaryEntryTypeExercise.ordinal());
                        exerciseDiaryEntry.setDuration(getPickersMinutes());
                        exerciseDiaryEntry.setEntryDate(new SimpleDate(mDate));
                        exerciseDiaryEntry.setDateModified(mDate);
                        exerciseDiaryEntry.setDateCreated(mDate);
                        exerciseDiaryEntry.setExercise(exercise);
                        exerciseDiaryEntry.setIsSynchronized(false);
                        ModelManager.daoSession.getDiaryEntryDao().insert(exerciseDiaryEntry);

                        exerciseDiaryEntry.updateConsumption();
                        ModelManager.daoSession.getDiaryEntryDao().update(exerciseDiaryEntry);

					}

					if (pickerMinutes > 0.0){
						Intent resultIntent = new Intent();
						resultIntent.putExtra(AddExerciseActivity.INTENT_EXERCISE_NAME, AddExerciseActivity.this.exercise.getTitle());
						setResult(Activity.RESULT_OK, resultIntent);
					}

					finish();
				}
			});

        	deleteButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
                    ModelManager.sharedModelManager().deleteDiaryEntry(AddExerciseActivity.this.diaryEntry);
		            finish();
				}
			});

        }
        getActionBar().setDisplayHomeAsUpEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getActionBar().setHomeButtonEnabled(true);
        }
    }

    // Initialize menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            Intent intent = new Intent(this, TabBarActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);

    }

	private double getPickersMinutes() {
		int hours = this.hoursPicker.getValue();
		int minutes = this.minutesPicker.getValue();
		return 60 * hours + minutes;
	}

	private void setPickers(int minutes) {
		int hours = (int) Math.floor(minutes / 60);
		minutes -= hours * 60;
		this.hoursPicker.setValue(hours);
		this.minutesPicker.setValue(minutes);
	}
	
	private void updateCaloriesLabel(){
		double minutes = getPickersMinutes();
		double cals = minutes * exercise.getCalsPerHour() / 60.0;
		caloriesTextView.setText(Math.round(cals) + "");
	}

	private void initializePickers() {
		// Hook up outlets
		this.hoursPicker = (NumberPicker) findViewById(R.id.hoursPicker);
        this.minutesPicker = (NumberPicker) findViewById(R.id.minutesPicker);
        this.hoursPicker.setOnValueChangedListener(this);
        this.minutesPicker.setOnValueChangedListener(this);

        this.hoursPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        this.minutesPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        this.hoursPicker.setMinValue(0);
        this.hoursPicker.setMaxValue(23);
        this.hoursPicker.setFocusable(false);
        this.hoursPicker.setValue(1);

        this.minutesPicker.setMinValue(0);
        this.minutesPicker.setMaxValue(59);
        this.minutesPicker.setFocusable(false);
        this.minutesPicker.setValue(0);
	}
	
    @Override
    protected void onStart() {
        super.onStart();
        // The activity is about to become visible.
        // -> onResume()
    }

    @Override
    protected void onResume() {
        super.onResume();
        // The activity has become visible (it is now "resumed").
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    	// Called before making the activity vulnerable to destruction; save your activity state in outState.
        // UI elements states are saved automatically by super.onSaveInstanceState()
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Another activity is taking focus (this activity is about to be "paused"); commit unsaved changes to persistent data, etc.
        // -> onStop()
    }

    @Override
    protected void onStop() {
        super.onStop();
        // The activity is no longer visible (it is now "stopped")
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        // The activity was stopped, and is about to be started again. It was not destroyed, so all members are intact.
        // -> onStart()
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // The activity is about to be destroyed.
        if (isFinishing()) {
        	// Someone called finish()
        } else {
        	// System is temporarily destroying this instance of the activity to save space
        }
    }

    @Override
    public void onValueChange(NumberPicker numberPicker, int i, int i2) {
        updateCaloriesLabel();

    }
}
