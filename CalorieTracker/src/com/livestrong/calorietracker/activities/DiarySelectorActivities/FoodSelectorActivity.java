package com.livestrong.calorietracker.activities.DiarySelectorActivities;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.*;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.livestrong.calorietracker.MyPlateApplication;
import com.livestrong.calorietracker.R;
import com.livestrong.calorietracker.Views.ClearableEditText;
import com.livestrong.calorietracker.activities.CalorieTrackerBaseActivity;
import com.livestrong.calorietracker.activities.TabBarActivity;
import com.livestrong.calorietracker.adapters.FoodSearchSelectorAdaptor;
import com.livestrong.calorietracker.adapters.FoodSelectorAdapter;
import com.livestrong.calorietracker.animations.DropDownAnimation;
import com.livestrong.calorietracker.back.api.CTWebserviceRequest;
import com.livestrong.calorietracker.back.db.ModelManager;
import com.livestrong.calorietracker.back.db.gen.DiaryEntry.DiaryEntryCategoryEnum;
import com.livestrong.calorietracker.back.db.gen.Food;
import com.livestrong.calorietracker.back.db.gen.Meal;

import java.util.Date;


public class FoodSelectorActivity extends CalorieTrackerBaseActivity implements OnItemClickListener, ActionBar.TabListener {
	
	ListView list;
	private FoodSelectorAdapter foodSelectorAdapter;
    private FoodSearchSelectorAdaptor searchSelectorAdapter;

    private EditText searchEditText;
	private LinearLayout toolBarContainer, messageContainer;
	private int toolBarHeight = -1;
	private Button addManualButton;
	private TextView timeOfDayTextView, messageTextView;
	private Boolean isSearching = false;
    private  DiaryEntryCategoryEnum mTimeOfDay;
    private Date mDate;

    private String getWorkingTimeOfDayString(DiaryEntryCategoryEnum timeOfDay){
        switch (timeOfDay){
            case CTDiaryEntryTypeBreakfast:
                return "Breakfast";
            case CTDiaryEntryTypeLunch:
                return "Lunch";
            case CTDiaryEntryTypeDinner:
                return "Dinner";
            default: return "Snacks";
        }
    }
	protected void onCreate(Bundle savedInstanceState){
		 super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
        	// The activity was destroyed, and is about to be started again. You need to restore your activity state from savedInstanceState.
            // UI elements states are restored automatically by super.onCreate()
        }

        // The activity is being created; create views, bind data to lists, etc.
        setContentView(R.layout.activity_food_selector);

        // Hook up outlets
        this.list 				= (ListView) findViewById(R.id.listView);
        this.searchEditText 	= ((ClearableEditText) findViewById(R.id.editTextClearable)).getEditText();
        this.toolBarContainer	= (LinearLayout) findViewById(R.id.toolBarContainer);

        this.addManualButton	= (Button) findViewById(R.id.addManualButton);

        this.timeOfDayTextView	= (TextView) findViewById(R.id.timeOfDayTextView);
        this.messageTextView 	= (TextView) findViewById(R.id.messageTextView);
        this.messageContainer 	= (LinearLayout) findViewById(R.id.messageContainer);

        // Initialize Text Views
        this.mTimeOfDay = DiaryEntryCategoryEnum.values()[getIntent().getExtras().getInt("TimeOfDay")];
        this.timeOfDayTextView.setText(this.getWorkingTimeOfDayString(mTimeOfDay));
        this.mDate = (Date)getIntent().getExtras().get(Date.class.getName());

        // Initialize list view
        this.list.setOnItemClickListener(this);

        this.foodSelectorAdapter = new FoodSelectorAdapter(this, mTimeOfDay){
            @Override
            public void notifyDataSetChanged(){
                super.notifyDataSetChanged();
                if (FoodSelectorActivity.this.foodSelectorAdapter == null){
                    return;
                }

                if (FoodSelectorActivity.this.foodSelectorAdapter.getCount() == 0){
                    FoodSelectorActivity.this.messageContainer.setVisibility(View.VISIBLE);
                } else {
                    FoodSelectorActivity.this.messageContainer.setVisibility(View.INVISIBLE);
                }
            }
        };

        this.searchSelectorAdapter = new FoodSearchSelectorAdaptor(this, mTimeOfDay);
        this.list.setAdapter(this.foodSelectorAdapter);
        this.list.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				hideKeyboard();
				searchEditText.clearFocus();
				return false;
			}
		});

        this.searchEditText.clearFocus();

        ActionBar bar = getActionBar();
        bar.setDisplayHomeAsUpEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getActionBar().setHomeButtonEnabled(true);
        }

        bar.removeAllTabs();
        bar.addTab(bar.newTab().setText(R.string.recentlyEaten).setTabListener(this));
        bar.addTab(bar.newTab().setText(R.string.frequentlyEaten).setTabListener(this));
        bar.addTab(bar.newTab().setText(R.string.myMeals).setTabListener(this));
        bar.addTab(bar.newTab().setText(R.string.customFoods).setTabListener(this));
        bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);


        this.initializeSearchBar();
        this.initializeTimeOfDayButtons();
        this.loadListFromSelectedButton();
    }
    @Override
    public void onResume() {
        super.onResume();
    }


	// Initialize menu
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);

        menu.add(0, 1, 0, "Add Food Manually").setIcon(android.R.drawable.ic_menu_add);
		
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		super.onOptionsItemSelected(item);
        if(item.getItemId() == android.R.id.home){
            Intent intent = new Intent(this, TabBarActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return true;
        }
		if(item.getItemId() == 1){
		    this.showAddManualActivity();
        }

		return true;
	}
	
	private void initializeSearchBar(){
		this.searchEditText.setHint("Search for food...");
		this.searchEditText.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
		
        this.searchEditText.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {				
				if (s.length() == 0){
					// Hide toolbar when field is empty
					showToolBar();
					isSearching = false;

                    getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

                    // Reload list with the appropriate data depending on the selected button
                    list.setAdapter(foodSelectorAdapter);
                    foodSelectorAdapter.loadFoodFromLocalSearch("");
                    loadListFromSelectedButton();
				} else {
					hideToolBar();
					
					isSearching = true;
					
					// Perform local food search
                    list.setAdapter(searchSelectorAdapter);
                    getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
                    String searchStr = String.valueOf(s);
                    searchSelectorAdapter.loadFoodFromLocalSearch(searchStr);

				}
			}

			@Override
			public void afterTextChanged(Editable s) {	}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
		});
        
        this.searchEditText.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {				
				if (event.getAction() == KeyEvent.ACTION_DOWN) {
					if (keyCode == KeyEvent.KEYCODE_ENTER) {
                        list.setAdapter(foodSelectorAdapter);

                        if(searchEditText.getText().length() == 0){
                            hideKeyboard();
                            return true;
                        }
						// Search for value in Edit Field
						FoodSelectorActivity.this.performServerSearch();
						// Hide Keyboard
						hideKeyboard();
						
						return true;
					} 
				}
				return false;
			}
        });
	}
	

	private void initializeTimeOfDayButtons(){

		this.addManualButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				showAddManualActivity();
			}
		});
	}
	
	private void loadListFromSelectedButton(){
        switch (getActionBar().getSelectedTab().getPosition()){
            case 0: this.foodSelectorAdapter.loadRecentlyEaten(); break;
            case 1: this.foodSelectorAdapter.loadFrequentlyEaten();break;
            case 2: this.foodSelectorAdapter.loadMyMeals();break;
            case 3: this.foodSelectorAdapter.loadCustomFoods();break;
        }
		
		if (this.foodSelectorAdapter.getCount() == 0){
			this.messageContainer.setVisibility(View.VISIBLE);
		} else {
			this.messageContainer.setVisibility(View.INVISIBLE);
		}
	}
	
	private void showAddManualActivity(){
		Intent intent = new Intent(FoodSelectorActivity.this, CreateFoodActivity.class);
        intent.putExtra(Date.class.getName(),this.mDate);
        intent.putExtra("TimeOfDay",this.mTimeOfDay.ordinal());
        startActivityForResult(intent, 1);
	}
	
	private void performServerSearch(){
		if (CTWebserviceRequest.isOnline(MyPlateApplication.getContext())){
			String searchString = searchEditText.getText().toString();
			foodSelectorAdapter.loadFoodFromServerSearch(searchString);
		}
	}
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
        if(this.list.getAdapter() == this.searchSelectorAdapter){
            searchEditText.setText((String)this.searchSelectorAdapter.getItem(position));
            list.setAdapter(foodSelectorAdapter);
            this.performServerSearch();
            return;
        }

		if (this.foodSelectorAdapter.showNoResultsMessage || this.foodSelectorAdapter.isLoading()){
			return;
		}
		
		// If User is searching
		if (this.isSearching){
			Object object = this.foodSelectorAdapter.getItem(position);
			if (object == null && this.foodSelectorAdapter.isShowingSearchOnlinePrompt()){
				this.performServerSearch();
				return;
			} else {
				Food food = (Food) object;
				Intent intent = new Intent(this, AddFoodActivity.class);
				intent.putExtra(food.getClass().getName(), food);
                intent.putExtra("TimeOfDay",this.mTimeOfDay.ordinal());
                intent.putExtra(Date.class.getName(),this.mDate);
                startActivityForResult(intent, 1);
				return;
			}	
		} 
	    int selectedTab = getActionBar().getSelectedTab().getPosition();
        // Regular food item selected
		if (selectedTab == 0 ||selectedTab == 1 || selectedTab == 3){
			Food food = (Food) this.foodSelectorAdapter.getItem(position);
			Intent intent = new Intent(this, AddFoodActivity.class);
			intent.putExtra(food.getClass().getName(), food);
            intent.putExtra("TimeOfDay",this.mTimeOfDay.ordinal());
            intent.putExtra(Date.class.getName(),this.mDate);
            startActivityForResult(intent, 1);

		}
        else if (selectedTab == 2){
            Meal meal = (Meal) this.foodSelectorAdapter.getItem(position);
            meal = Meal.getMeal(meal.getRemoteId(),ModelManager.daoSession.getMealDao());
            Intent intent = new Intent(this, AddMealActivity.class);
            intent.putExtra(meal.getClass().getName(), meal);
            intent.putExtra("TimeOfDay",this.mTimeOfDay.ordinal());
            intent.putExtra(Date.class.getName(),this.mDate);

            startActivityForResult(intent, 1);

		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if (resultCode == Activity.RESULT_OK){
			if (this.searchEditText.getText().length() > 0){
				this.searchEditText.setText("");
			} else {
				loadListFromSelectedButton();
			}
			
			String foodName = data.getExtras().getString(AddFoodActivity.INTENT_FOOD_NAME);
			this.displayNotification(foodName + " was added to your diary.");

			setResult(Activity.RESULT_OK);
            finish();
		}
	}
	
	public void hideToolBar() {
		if (this.toolBarContainer.getHeight() == 0){
			return;
		}

		if (this.toolBarHeight == -1){
			this.toolBarHeight = this.toolBarContainer.getHeight();
		}
		
		isSearching = true;
		
		DropDownAnimation animation = new DropDownAnimation(this.toolBarContainer, 0);
		animation.setDuration(300);
		this.toolBarContainer.startAnimation(animation);
		
		this.messageContainer.setVisibility(View.INVISIBLE);
	}
	
	public void showToolBar() {
		if (this.toolBarContainer.getHeight() != 0){
			return;
		}
		
		isSearching = false;
		
		DropDownAnimation animation = new DropDownAnimation(this.toolBarContainer, this.toolBarHeight);
		animation.setDuration(300);
		this.toolBarContainer.startAnimation(animation);
	}
	
	private void hideKeyboard(){
		// Hide Keyboard
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(searchEditText.getWindowToken(), 0);
		searchEditText.clearFocus();
	}
	
	private void displayNotification(String notificationString){
		Context context = getApplicationContext();
		int duration = Toast.LENGTH_SHORT;
		
		Toast toast = Toast.makeText(context, notificationString, duration);
		toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 0);
		toast.show();
	}

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        int btnId = tab.getPosition();
        switch (btnId){
            case 0:
                foodSelectorAdapter.loadRecentlyEaten();
                messageTextView.setText("No recently eaten food items.");
                break;
            case 1:
                foodSelectorAdapter.loadFrequentlyEaten();
                messageTextView.setText("No frequently eaten food items.");
                break;
            case 2:
                foodSelectorAdapter.loadMyMeals();
                messageTextView.setText("No meals.");
                break;
            case 3:
                messageTextView.setText("No custom foods.");
                break;
        }
        loadListFromSelectedButton();
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }
}