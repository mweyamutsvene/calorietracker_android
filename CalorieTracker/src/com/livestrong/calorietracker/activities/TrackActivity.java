package com.livestrong.calorietracker.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

import com.livestrong.calorietracker.R;
import com.livestrong.calorietracker.activities.DiarySelectorActivities.AddWaterActivity;
import com.livestrong.calorietracker.activities.DiarySelectorActivities.AddWeightActivity;
import com.livestrong.calorietracker.activities.DiarySelectorActivities.ExerciseSelectorActivity;
import com.livestrong.calorietracker.activities.DiarySelectorActivities.FoodSelectorActivity;
import com.livestrong.calorietracker.back.db.gen.DiaryEntry;

import java.util.Date;


public class TrackActivity extends CalorieTrackerBaseFragmentActivity {
	
	private LinearLayout breakfastItem, lunchItem, dinnerItem, snacksItem, exerciseItem, waterItem, weightItem;
    private  Date mDate;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        if (savedInstanceState != null) {
        	// The activity was destroyed, and is about to be started again. You need to restore your activity state from savedInstanceState.
            // UI elements states are restored automatically by super.onCreate()
        }

        // The activity is being created; create views, bind data to lists, etc.
        setContentView(R.layout.activity_track);
        
        // Hook up Outlets
        this.breakfastItem 	= (LinearLayout) findViewById(R.id.breakfastItem);
        this.lunchItem 		= (LinearLayout) findViewById(R.id.lunchItem);
        this.dinnerItem 	= (LinearLayout) findViewById(R.id.dinnerItem);
        this.snacksItem 	= (LinearLayout) findViewById(R.id.snacksItem);
        this.waterItem 		= (LinearLayout) findViewById(R.id.waterItem);
        this.exerciseItem 	= (LinearLayout) findViewById(R.id.exerciseItem);
        this.weightItem 	= (LinearLayout) findViewById(R.id.weightItem);

        this.mDate = (Date)getIntent().getExtras().get(Date.class.getName());

        OnClickListener onClickListener = new OnClickListener() {
			@Override
			public void onClick(View view) {				
				Intent intent = null;
                DiaryEntry.DiaryEntryCategoryEnum timeOfDay = null;

                switch (view.getId()) {
					case R.id.breakfastItem:
						intent = new Intent(TrackActivity.this, FoodSelectorActivity.class);
                        timeOfDay = DiaryEntry.DiaryEntryCategoryEnum.CTDiaryEntryTypeBreakfast;
						break;
					case R.id.lunchItem:
						intent = new Intent(TrackActivity.this, FoodSelectorActivity.class);
                        timeOfDay = DiaryEntry.DiaryEntryCategoryEnum.CTDiaryEntryTypeLunch;
						break;
					case R.id.dinnerItem:
						intent = new Intent(TrackActivity.this, FoodSelectorActivity.class);
                        timeOfDay = DiaryEntry.DiaryEntryCategoryEnum.CTDiaryEntryTypeDinner;
						break;
					case R.id.snacksItem:
						intent = new Intent(TrackActivity.this, FoodSelectorActivity.class);
                        timeOfDay = DiaryEntry.DiaryEntryCategoryEnum.CTDiaryEntryTypeSnacks;
						break;
					case R.id.exerciseItem:
						intent = new Intent(TrackActivity.this, ExerciseSelectorActivity.class);
                        timeOfDay = DiaryEntry.DiaryEntryCategoryEnum.CTDiaryEntryTypeExercise;
                        break;
					case R.id.waterItem:
						intent = new Intent(TrackActivity.this, AddWaterActivity.class);
                        timeOfDay = DiaryEntry.DiaryEntryCategoryEnum.CTDiaryEntryTypeWater;
                        break;
					case R.id.weightItem:
						intent = new Intent(TrackActivity.this, AddWeightActivity.class);
                        timeOfDay = DiaryEntry.DiaryEntryCategoryEnum.CTDiaryEntryTypeWeighIn;
                        break;
				}
				if (intent != null){
                    intent.putExtra("TimeOfDay",timeOfDay.ordinal());
                    intent.putExtra(Date.class.getName(), mDate);
                    startActivity(intent);

                    finish();
				}
            }
		};
		
		this.breakfastItem.setOnClickListener(onClickListener);
		this.lunchItem.setOnClickListener(onClickListener);
		this.dinnerItem.setOnClickListener(onClickListener);
		this.snacksItem.setOnClickListener(onClickListener);
		this.waterItem.setOnClickListener(onClickListener);
		this.exerciseItem.setOnClickListener(onClickListener);
		this.weightItem.setOnClickListener(onClickListener);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getActionBar().setHomeButtonEnabled(true);
        }
    }

    // Initialize menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            Intent intent = new Intent(this, TabBarActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);

    }
}
