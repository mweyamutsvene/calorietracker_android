package com.livestrong.calorietracker.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.livestrong.calorietracker.R;
import com.livestrong.calorietracker.back.api.CTWebserviceRequest;
import com.livestrong.calorietracker.back.api.CTWebserviceRequest.CTWebserviceRequestDelegate;
import com.livestrong.calorietracker.back.api.CTWebserviceRequest.CTWebserviceRequestType;
import com.livestrong.calorietracker.back.db.ModelManager;
import com.livestrong.calorietracker.back.db.gen.Profile;



public class WelcomeActivity extends CalorieTrackerBaseActivity implements CTWebserviceRequestDelegate{
	private class UserProfileTask extends AsyncTask<Void, Void, Profile>
	{
		protected void onPreExecute() 
		{
			WelcomeActivity.this.toggleUserLoginContent(true);
		}

        @Override
		protected Profile doInBackground(Void... params)
		{
			return ModelManager.sharedModelManager().getUserProfile();
		}
		
		protected void onPostExecute(Profile profile)
		{
			if (profile != null) {
	        	Intent intent = new Intent(WelcomeActivity.this, TabBarActivity.class);
				startActivity(intent);

				finish();
				return;
	        }
			else
			{
				WelcomeActivity.this.toggleUserLoginContent(false);
			}
		};

	};
	
	
	CTWebserviceRequest request; //Current request being tracked
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_welcome);

        Button yesButton = (Button) findViewById(R.id.yesButton);
        yesButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(WelcomeActivity.this, LoginActivity.class);
				startActivityForResult(intent, 0);
			}
		});
        
        Button notNowButton = (Button) findViewById(R.id.notNowButton);
        notNowButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(WelcomeActivity.this, ProfileActivity.class);
				startActivityForResult(intent, 0);
			}
		});
        WelcomeActivity.this.toggleUserLoginContent(true);


	}
    @Override
    public void onResume() {
        super.onResume();
        new UserProfileTask().execute(new Void[]{});

    }

	@Override
	public void dataReceived(CTWebserviceRequest request,
			CTWebserviceRequestType methodCalled, Object data) {
		
		ModelManager.sharedModelManager().synchronizeDataWithServer();
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean errorOccurred(CTWebserviceRequest request,
			CTWebserviceRequestType methodCalled, Exception error,
			String errorMessage) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK){
			finish();
		}
		super.onActivityResult(requestCode, resultCode, data);
		
		
	}
	
	protected void toggleUserLoginContent(boolean showProgress) 
	{
		WelcomeActivity.this.findViewById(R.id.headerTextView).setVisibility(showProgress ? View.GONE : View.VISIBLE);
		WelcomeActivity.this.findViewById(R.id.yesNoLayout).setVisibility(showProgress ? View.GONE : View.VISIBLE);
		
		WelcomeActivity.this.findViewById(R.id.progressBar1).setVisibility(showProgress ? View.VISIBLE : View.GONE);
		WelcomeActivity.this.findViewById(R.id.progressText).setVisibility(showProgress ? View.VISIBLE : View.GONE);
	}
	

}
