package com.livestrong.calorietracker.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.livestrong.calorietracker.MyPlateApplication;
import com.livestrong.calorietracker.R;
import com.livestrong.calorietracker.adapters.CommunityCommentsAdapter;
import com.livestrong.calorietracker.back.api.CTWebserviceRequest;
import com.livestrong.calorietracker.back.db.gen.CommunityMessage;


public class CommunityCommentsActivity extends CalorieTrackerBaseActivity implements CTWebserviceRequest.CTWebserviceRequestDelegate {

	private ListView list;
	private CommunityMessage message;
	private CommunityCommentsAdapter adapter;
	private Button sendButton;
	private ProgressBar progressBar;
	private EditText messageEditText;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (savedInstanceState != null) {
			// The activity was destroyed, and is about to be started again. You need to restore your activity state from savedInstanceState.
			// UI elements states are restored automatically by super.onCreate()
		}

		// The activity is being created; create views, bind data to lists, etc.
		setContentView(R.layout.activity_community_comments);

		this.list = (ListView) findViewById(R.id.communityListView);
		this.sendButton = (Button) findViewById(R.id.sendButton);
		this.progressBar = (ProgressBar) findViewById(R.id.progressBar);
		this.messageEditText = (EditText) findViewById(R.id.messageEditText);
		
		this.progressBar.setVisibility(View.INVISIBLE);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			this.message = (CommunityMessage) extras.get(CommunityMessage.class.getName());
			setNumComments(this.message.getNumberOfComments());

			this.adapter = new CommunityCommentsAdapter(this, this.list, this.message);
			this.list.setAdapter(this.adapter);

			this.adapter.loadComments(1);
		}

		this.list.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				hideKeyboard();
				return false;
			}
		});
		
		
		// This is used to removed a banding effect caused when drawing gradients in list view items
		getWindow().setFormat(PixelFormat.RGBA_8888);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_DITHER);

		this.initializeButtons();

        getActionBar().setDisplayHomeAsUpEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getActionBar().setHomeButtonEnabled(true);
        }
    }

    // Initialize menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if(item.getItemId() == android.R.id.home){
            Intent intent = new Intent(this, TabBarActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return true;
        }


        return true;
    }

	private void setNumComments(int numComments) {
		TextView headerTextView = (TextView) findViewById(R.id.headerTextView);
		headerTextView.setText(numComments + " COMMENT" + (numComments != 1 ? "S" : ""));
	}

	private void initializeButtons() {
		sendButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View button) {
				sendMessage();
			}
		});
	}

	private void sendMessage() {
		if (CTWebserviceRequest.isLoggedIn() == false){
			new AlertDialog.Builder(MyPlateApplication.getFrontMostActivity())
		      .setMessage("You must be signed in to post a message.")
		      .setTitle(R.string.error)
		      .setNeutralButton(android.R.string.ok,
		         new DialogInterface.OnClickListener() {
		         public void onClick(DialogInterface dialog, int whichButton){}
		         })
		      .show();
			
			return;			
		}
				
		this.sendButton.setVisibility(View.INVISIBLE);
		this.progressBar.setVisibility(View.VISIBLE);
		this.messageEditText.setEnabled(false);
		CTWebserviceRequest.postComment(this.messageEditText.getText().toString(),this.message.getPostId()+"").delegate = this;

		this.hideKeyboard();
	}
	
	private void hideKeyboard(){
		if (this.messageEditText.hasFocus()){
			// Hide Keyboard
			InputMethodManager imm = (InputMethodManager) this.getSystemService(INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(this.messageEditText.getWindowToken(), 0);
			// Clear focus
			this.messageEditText.clearFocus();	
		}
	}

    @Override
    public void dataReceived(CTWebserviceRequest request, CTWebserviceRequest.CTWebserviceRequestType methodCalled, Object data) {
            int numComments = this.message.getNumberOfComments();
            this.message.setNumberOfComments(++numComments);
            this.setNumComments(numComments);
            this.adapter.reloadComments();
            this.progressBar.setVisibility(View.INVISIBLE);
            this.sendButton.setVisibility(View.VISIBLE);
            this.messageEditText.setEnabled(true);
            this.messageEditText.setText("");
    }

    @Override
    public boolean errorOccurred(CTWebserviceRequest request, CTWebserviceRequest.CTWebserviceRequestType methodCalled, Exception error, String errorMessage) {
        return false;
    }
}
