package com.livestrong.calorietracker.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.livestrong.calorietracker.R;
import com.livestrong.calorietracker.back.db.ModelManager;
import com.livestrong.calorietracker.back.db.gen.Profile;
import com.livestrong.calorietracker.fragments.More.MoreProfileFragment;

public class ProfileActivity extends CalorieTrackerBaseFragmentActivity {

    private MoreProfileFragment profileFragment;

    private class UserProfileTask  extends AsyncTask<Void, Void, Profile>{
		@Override
		protected Profile doInBackground(Void... params){
			return ModelManager.sharedModelManager().getUserProfile();
		}

		protected void onPostExecute(Profile profile){
			// load profile fragment
	        FragmentManager fragmentManager = getSupportFragmentManager();
			FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
			fragmentTransaction.replace(R.id.frameLayout, profileFragment);
			fragmentTransaction.commit();

			Button doneButton = (Button) findViewById(R.id.doneButton);
			doneButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
                    boolean success = profileFragment.saveProfile();
                    if(!success){
                        //TODO: show alert specifying that the profile cant be saved with incorrect information.
                        return;
                    }

					Intent intent = new Intent(ProfileActivity.this, TabBarActivity.class);
					startActivity(intent);
					setResult(RESULT_OK);
					finish();
				}
			});

			 if (profile == null){
		         // Use chose to not log in
                 profileFragment.createNewUserProfile();
	         }
			 else{
//	        	profile.setProfileDefaults();
	         }
		};

	};
	

	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        if (savedInstanceState != null) {
        	// The activity was destroyed, and is about to be started again. You need to restore your activity state from savedInstanceState.
            // UI elements states are restored automatically by super.onCreate()
        }

        // The activity is being created; create views, bind data to lists, etc.
        setContentView(R.layout.activity_profile);

        profileFragment = new MoreProfileFragment();

        // load profile fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, profileFragment);
        fragmentTransaction.commit();

        Button doneButton = (Button) findViewById(R.id.doneButton);
        doneButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                boolean success = profileFragment.saveProfile();
                if(!success){
                    //TODO: show alert specifying that the profile cant be saved with incorrect information.
                    return;
                }

                Intent intent = new Intent(ProfileActivity.this, TabBarActivity.class);
                startActivity(intent);
                setResult(RESULT_OK);
                finish();
            }
        });

        getActionBar().setDisplayHomeAsUpEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getActionBar().setHomeButtonEnabled(true);
        }

    };

    // Initialize menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            Intent intent = new Intent(this, WelcomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);

    }
}
