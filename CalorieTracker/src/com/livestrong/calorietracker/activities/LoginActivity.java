package com.livestrong.calorietracker.activities;



import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.livestrong.calorietracker.MyPlateApplication;
import com.livestrong.calorietracker.back.api.CTWebserviceRequest;
import com.livestrong.calorietracker.back.api.CTWebserviceRequest.CTWebserviceRequestDelegate;
import com.livestrong.calorietracker.back.api.CTWebserviceRequest.CTWebserviceRequestType;
import com.livestrong.calorietracker.back.db.ModelManager;
import com.livestrong.calorietracker.back.db.ModelManager.ModelManagerDelegate;
import com.livestrong.calorietracker.configuration.MyPlateDefaults;
import com.livestrong.calorietracker.utilities.JSONSafeObject;
import com.livestrong.calorietracker.R;


public class LoginActivity extends CalorieTrackerBaseActivity implements CTWebserviceRequestDelegate, ModelManagerDelegate {
	
	public static String INTENT_APP_OFFLINE_MODE = "appOfflineMode";
	
	protected String username;
	protected String password;
	private ProgressBar progressBar;
	private TextView loadingTextView;
	private Button loginButton;
	private Button registerButton;
	private Boolean appOfflineMode; // app is being used in offline mode (user is using app but has never signed in)
	private EditText usernameEditText, passwordEditText;
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
               
        if (savedInstanceState != null) {
        	// The activity was destroyed, and is about to be started again. You need to restore your activity state from savedInstanceState.
            // UI elements states are restored automatically by super.onCreate()
        }

        this.appOfflineMode = false;
        Bundle extras = getIntent().getExtras();
        if (extras != null){
        	this.appOfflineMode = extras.getBoolean(INTENT_APP_OFFLINE_MODE);
        }
        
        // The activity is being created; create views, bind data to lists, etc.
        setContentView(R.layout.activity_login);

		this.progressBar = (ProgressBar) findViewById(R.id.progressBar);
		this.loadingTextView = (TextView) findViewById(R.id.loadingTextView);
		this.progressBar.setVisibility(View.INVISIBLE);
		this.loadingTextView.setVisibility(View.INVISIBLE);
		
		this.usernameEditText = (EditText) findViewById(R.id.usernameEditText);
		this.passwordEditText = (EditText) findViewById(R.id.passwordEditText);
		this.passwordEditText.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {				
				if (event.getAction() == KeyEvent.ACTION_DOWN) {
					if (keyCode == KeyEvent.KEYCODE_ENTER) {
						initiateLogin();					
						return true;
					} 
				}
				return false;
			}
        });
		
		//Login 
        this.loginButton = (Button) findViewById(R.id.loginButton);
        this.loginButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				initiateLogin();
			}
		});
        
        //Show Register Activity
       this.registerButton = (Button) findViewById(R.id.registerButton);
       this.registerButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
				startActivityForResult(intent, 0);
			}
		});
        getActionBar().setDisplayHomeAsUpEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getActionBar().setHomeButtonEnabled(true);
        }
    }

    // Initialize menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            Intent intent = new Intent(this, WelcomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);

    }
	
	private void hideKeyboard(){
		// Hide Keyboard
		InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(usernameEditText.getWindowToken(), 0);
		imm.hideSoftInputFromWindow(passwordEditText.getWindowToken(), 0);
		
		this.usernameEditText.clearFocus();
		this.passwordEditText.clearFocus();
	}
	
	private void initiateLogin(){
		if (CTWebserviceRequest.isOnline(MyPlateApplication.getContext()) == false){
			new AlertDialog.Builder(LoginActivity.this)
		      .setMessage("Please ensure you are connected to the internet.")
		      .setTitle("No Internet Connection")
		      .setNeutralButton(android.R.string.ok,
		         new DialogInterface.OnClickListener() {
		         public void onClick(DialogInterface dialog, int whichButton){}
		         })
		      .show();
			return;
		}
		
		
		this.progressBar.setVisibility(View.VISIBLE);
		this.loginButton.setVisibility(View.GONE);
		this.loadingTextView.setVisibility(View.VISIBLE);
		this.loadingTextView.setText("Authenticating...");
		
		this.username = this.usernameEditText.getText().toString();
		this.password = this.passwordEditText.getText().toString();

		hideKeyboard();
		
	
		CTWebserviceRequest.login(this.username, this.password).delegate = this;


	}

    @Override
    protected void onStart() {
        super.onStart();
        // The activity is about to become visible.
        // -> onResume()
    }

    @Override
    protected void onResume() {
        super.onResume();
        // The activity has become visible (it is now "resumed").
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    	// Called before making the activity vulnerable to destruction; save your activity state in outState.
        // UI elements states are saved automatically by super.onSaveInstanceState()
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Another activity is taking focus (this activity is about to be "paused"); commit unsaved changes to persistent data, etc.
        // -> onStop()
    }

    @Override
    protected void onStop() {
        super.onStop();
        
        // The activity is no longer visible (it is now "stopped")
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        // The activity was stopped, and is about to be started again. It was not destroyed, so all members are intact.
        // -> onStart()
    }

    @Override
    protected void onDestroy() {
		ModelManager.sharedModelManager().delegate = null;

        super.onDestroy();
        // The activity is about to be destroyed.
        if (isFinishing()) {
        	// Someone called finish()
        } else {
        	// System is temporarily destroying this instance of the activity to save space
        }
    }

	@Override
	public void dataReceived(CTWebserviceRequest request,
			CTWebserviceRequestType methodCalled, Object data) {
		
	 if (methodCalled == CTWebserviceRequestType.REQUEST_POST_LOGIN) {
			JSONObject responseObject = (JSONObject)data;
			if(JSONSafeObject.safeGetBoolean("success", responseObject)){
				//Save Credentials
				MyPlateDefaults.setPref(MyPlateDefaults.PREFS_USERNAME, this.username);
				MyPlateDefaults.setPref(MyPlateDefaults.PREFS_PASSWORD, this.password);
				
				//Begin Sync
				LoginActivity.this.loadingTextView.setText("Syncing...");
				ModelManager.delegate = this;
                MyPlateDefaults.setPref(MyPlateDefaults.PREFS_LAST_SYNC,null);

                ModelManager.sharedModelManager().synchronizeDataWithServer();
			}
			else{
				// Auth failed
				this.progressBar.setVisibility(View.GONE);
				this.loadingTextView.setVisibility(View.INVISIBLE);
				this.loginButton.setVisibility(View.VISIBLE);
				
				try {
					new AlertDialog.Builder(LoginActivity.this)
					  .setMessage(responseObject.getString("message"))
					  .setNeutralButton(android.R.string.ok,
					     new DialogInterface.OnClickListener() {
					     public void onClick(DialogInterface dialog, int whichButton){}
					     })
					  .show();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	
				return;
			}
		}		
	}

	@Override
	public boolean errorOccurred(CTWebserviceRequest request,
			CTWebserviceRequestType methodCalled, Exception error,
			String errorMessage)
    {
		// TODO Auto-generated method stub
        this.progressBar.setVisibility(View.GONE);
        this.loadingTextView.setVisibility(View.INVISIBLE);
        this.loginButton.setVisibility(View.VISIBLE);

        new AlertDialog.Builder(LoginActivity.this)
                .setMessage("We are unable to connect to our server at this time, please check your connection and try again later.")
                .setNeutralButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton){}
                        })
                .show();

        return false;
	}

	@Override
	public void syncPercentCompleted(ModelManager modelManager, float percentComplete) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void syncCompleted(ModelManager modelManager, Boolean success) {
        if(success == false){
            new AlertDialog.Builder(this)
                    .setMessage("Please ensure you are connected to the internet.")
                    .setTitle("Unable to sync at this time")
                    .setNeutralButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton){}
                            })
                    .show();
        }
		modelManager.delegate = null;

		// Done with initial data load.
		this.progressBar.setVisibility(View.GONE);

		// Pop the stack :)
		if (appOfflineMode == false){
			Intent intent = new Intent(this, TabBarActivity.class);
			startActivity(intent);
		} 
		setResult(Activity.RESULT_OK);

		finish();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK){
			setResult(Activity.RESULT_OK);
			finish();
		}
		
		super.onActivityResult(requestCode, resultCode, data);
	}
}
