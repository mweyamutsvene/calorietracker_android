package com.livestrong.calorietracker.activities;


import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ProgressBar;

import com.livestrong.calorietracker.MyPlateApplication;
import com.livestrong.calorietracker.R;
import com.livestrong.calorietracker.back.api.CTWebserviceRequest;
import com.livestrong.calorietracker.back.db.ModelManager;
import com.livestrong.calorietracker.back.db.gen.Profile;
import com.livestrong.calorietracker.configuration.MyPlateDefaults;
import com.livestrong.calorietracker.utilities.JSONSafeObject;


public class RegisterActivity extends CalorieTrackerBaseActivity implements CTWebserviceRequest.CTWebserviceRequestDelegate {
	
	LinearLayout view;
	EditText usernameEditText, passwordEditText, birthdayEditText, emailEditText, genderEditText;
	ProgressBar progressBar;
	Button createAccountButton;
	Integer age;
	String gender;
	Dialog ageDialog, genderDialog;
	Boolean agreesToTerms = false;
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        if (savedInstanceState != null) {
        	// The activity was destroyed, and is about to be started again. You need to restore your activity state from savedInstanceState.
            // UI elements states are restored automatically by super.onCreate()
        }

        // The activity is being created; create views, bind data to lists, etc.
        setContentView(R.layout.activity_register);
        
        this.usernameEditText 			= (EditText) findViewById(R.id.usernameEditText);		
		this.passwordEditText 			= (EditText) findViewById(R.id.passwordEditText);
		this.genderEditText				= (EditText) findViewById(R.id.genderEditText);
		this.emailEditText				= (EditText) findViewById(R.id.emailEditText);
		this.birthdayEditText 			= (EditText) findViewById(R.id.birthdayEditText);
		this.progressBar 				= (ProgressBar) findViewById(R.id.progressBar);
		this.createAccountButton = (Button) findViewById(R.id.createAccountButton);
		this.createAccountButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (CTWebserviceRequest.isOnline(MyPlateApplication.getContext()) == false){ // Check if user has internet connection
					new AlertDialog.Builder(RegisterActivity.this)
				      .setMessage("Please ensure you are connected to the internet.")
				      .setTitle("No Internet Connection")
				      .setNeutralButton(android.R.string.ok,
				         new DialogInterface.OnClickListener() {
				         public void onClick(DialogInterface dialog, int whichButton){}
				         })
				      .show();
					return;
				}
				
				
				if (validateFields()){
					if (agreesToTerms == false){
						final Dialog dialog = new Dialog(RegisterActivity.this);
						dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
						dialog.setContentView(R.layout.dialog_register); 
						
						// Initialize Terms and conditions button
						Button termsButton = (Button) dialog.findViewById(R.id.termsAndConditionsButton);
						termsButton.setOnClickListener(new OnClickListener() {
							@Override
							public void onClick(View v) {
								Intent intent = new Intent(RegisterActivity.this, WebViewActivity.class);
								intent.putExtra(WebViewActivity.INTENT_URL, "http://www.livestrong.com/terms/");
								intent.putExtra(WebViewActivity.INTENT_TITLE, "TERMS OF USE");
								startActivity(intent);
							}
						});
						
						// Initialize Privacy button
						Button privacyButton = (Button) dialog.findViewById(R.id.privacyButton);
						privacyButton.setOnClickListener(new OnClickListener() {
							@Override
							public void onClick(View v) {
								Intent intent = new Intent(RegisterActivity.this, WebViewActivity.class);
								intent.putExtra(WebViewActivity.INTENT_URL, "http://www.livestrong.com/privacy-policy/");
								intent.putExtra(WebViewActivity.INTENT_TITLE, "PRIVACY POLICY");
								startActivity(intent);	
							}
						});
						
						Button cancelButton = (Button) dialog.findViewById(R.id.cancelButton);
						cancelButton.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								dialog.dismiss();
							}
						});	
						
						Button doneButton = (Button) dialog.findViewById(R.id.doneButton);
						doneButton.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								registerUser();
								dialog.cancel();
								agreesToTerms = true;
							}
						});
						
						dialog.show();
					} else { // User has already agreed to terms
						registerUser();
					}
				}
			}
		});
		
		//Set up Cancel Button
		Button cancelButton = (Button) findViewById(R.id.cancelButton);
		cancelButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		//Initialize the two pickers
		this.initializeGenderPicker();
		this.initializeAgePicker();

        getActionBar().setDisplayHomeAsUpEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getActionBar().setHomeButtonEnabled(true);
        }
	}

    // Initialize menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
       return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            Intent intent = new Intent(this, WelcomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);

    }

	private void registerUser(){
		//Show Progress
		this.progressBar.setVisibility(View.VISIBLE);
		this.createAccountButton.setVisibility(View.INVISIBLE);
		
		//Send register request to server
		CTWebserviceRequest request = CTWebserviceRequest.postSignup(
				usernameEditText.getText().toString(), 
				genderEditText.getText().toString(),
				emailEditText.getText().toString(),
				passwordEditText.getText().toString(), 
				null);
		
		request.delegate = this;
		
	}

	private void initializeAgePicker(){
		this.birthdayEditText.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus){
					return;
				}
				
				//Create new dialog if it currently doesn't exist
				if (ageDialog == null){
					ageDialog = new Dialog(RegisterActivity.this);
					ageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					ageDialog.setContentView(R.layout.dialog_birthday); 
					
					//Set up number picker
					NumberPicker numberPicker = (NumberPicker) ageDialog.findViewById(R.id.numberPicker1);
					numberPicker.setMaxValue(100);
					numberPicker.setMinValue(0);
					numberPicker.setValue(18);
					numberPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

					//Set up Cancel Button
					Button cancelButton = (Button) ageDialog.findViewById(R.id.cancelButton);
					cancelButton.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
				            birthdayEditText.clearFocus();
				            ageDialog.cancel();
						}
					});	
					
					//Set up Done Button
					Button doneButton = (Button) ageDialog.findViewById(R.id.doneButton);
					doneButton.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							NumberPicker numberPicker = (NumberPicker) ageDialog.findViewById(R.id.numberPicker1);
							age = numberPicker.getValue();						
							
							refreshBirthdayEditText();
							birthdayEditText.clearFocus();
							ageDialog.dismiss();
						}
					});
				}
				
				//Show the dialog if it is not already showing
				if (!ageDialog.isShowing()){
					ageDialog.show();	
				}
			}
		});
	}
	
	
	private void initializeGenderPicker(){
		this.genderEditText.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus){
					return;
				}
				
				//Create new dialog if it currently doesn't exist
				if (genderDialog == null){
					genderDialog = new Dialog(RegisterActivity.this);
					genderDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					genderDialog.setContentView(R.layout.dialog_birthday); 
					
					//Set up Gender picker
					final NumberPicker numberPicker = (NumberPicker) genderDialog.findViewById(R.id.numberPicker1);
					numberPicker.setMaxValue(1);
					numberPicker.setMinValue(0);
					numberPicker.setMinimumWidth(100);
					numberPicker.setValue(0);
					numberPicker.setDisplayedValues(new String[] { "Male", "Female" } );
					numberPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
					
					//Set up Cancel Button
					Button cancelButton = (Button) genderDialog.findViewById(R.id.cancelButton);
					cancelButton.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
				            genderEditText.clearFocus();
				            genderDialog.cancel();
						}
					});	
					
					//Set up Done Button
					Button doneButton = (Button) genderDialog.findViewById(R.id.doneButton);
					doneButton.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							gender = numberPicker.getDisplayedValues()[numberPicker.getValue()];
							refreshGenderEditText();
							genderEditText.clearFocus();
							genderDialog.dismiss();
						}
					});
				}
				
				//Show the dialog if it is not already showing
				if (!genderDialog.isShowing()){
					genderDialog.show();	
				}
			}
		});
	}
	private void refreshBirthdayEditText(){
		this.birthdayEditText.setText(age.toString());
	}
	private void refreshGenderEditText(){
		this.genderEditText.setText(gender);
	}
	
	private boolean validateFields(){
		String errorMessage = "";
		if (this.usernameEditText.getText().toString().equals("")){
			errorMessage += "Your username must be entered.\n";
		}
		
		String password = this.passwordEditText.getText().toString(); 
		if (password.equals("")){
			errorMessage += "A password must be entered.\n";
		}
		if (this.birthdayEditText.getText().toString().equals("")){
			errorMessage += "You must enter your age.\n";
		}
		if (this.emailEditText.getText().toString().equals("")){
			errorMessage += "You must enter your email address.\n";
		}
		
		if (!errorMessage.equals("")){
			new AlertDialog.Builder(RegisterActivity.this)
		      .setMessage(errorMessage)
		      .setNeutralButton(android.R.string.ok,
		         new DialogInterface.OnClickListener() {
		         public void onClick(DialogInterface dialog, int whichButton){}
		         })
		      .show();
			return false;
		}
		return true;
	}

	@Override
	public void dataReceived(CTWebserviceRequest request, CTWebserviceRequest.CTWebserviceRequestType methodCalled, Object data) {

//		Intent intent = new Intent(this, ProfileActivity.class);
//		startActivity(intent);
//		
		JSONObject responseObject = (JSONObject)data;
		
		if(JSONSafeObject.safeGetBoolean("success", responseObject)){
			//Save Credentials
			MyPlateDefaults.setPref(MyPlateDefaults.PREFS_USERNAME, this.usernameEditText.getText().toString());
			MyPlateDefaults.setPref(MyPlateDefaults.PREFS_PASSWORD, this.passwordEditText.getText().toString());
            Profile profile = new Profile();
            profile.editAge(this.age);
            profile.editActivityLevel(0.0);
            profile.editGender(this.gender.equals("Male") ? Profile.Gender.GENDER_MALE : Profile.Gender.GENDER_FEMALE);
            profile.editCaloriesMode(Profile.GoalMode.GOAL_MODE_CALCULATED);
            profile.editHeight(70);
            profile.editWeight(150);
            ModelManager.sharedModelManager().saveUserProfile(profile);
            Intent intent = new Intent(this, TabBarActivity.class);
            startActivity(intent);
			setResult(Activity.RESULT_OK);
			finish();
		}
		else{
			// Auth failed
			this.progressBar.setVisibility(View.GONE);
			this.progressBar.setVisibility(View.INVISIBLE);
			this.createAccountButton.setVisibility(View.VISIBLE);
			
			try {
				new AlertDialog.Builder(RegisterActivity.this)
				  .setMessage(responseObject.getString("message"))
				  .setNeutralButton(android.R.string.ok,
				     new DialogInterface.OnClickListener() {
				     public void onClick(DialogInterface dialog, int whichButton){}
				     })
				  .show();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		} 
	}

	@Override
	public boolean errorOccurred(CTWebserviceRequest request,
			CTWebserviceRequest.CTWebserviceRequestType methodCalled, Exception error,
			String errorMessage) {
		this.progressBar.setVisibility(View.INVISIBLE);
		this.createAccountButton.setVisibility(View.VISIBLE);
		if (errorMessage == null) {
			errorMessage = "There was a problem creating your account.\nMake sure you are online, or try again later.";
		}
		new AlertDialog.Builder(MyPlateApplication.getFrontMostActivity())
	      .setMessage(errorMessage)
	      .setNeutralButton(android.R.string.ok,
	         new DialogInterface.OnClickListener() {
	         public void onClick(DialogInterface dialog, int whichButton){}
	         })
	      .show();
		return true;
	}
}
