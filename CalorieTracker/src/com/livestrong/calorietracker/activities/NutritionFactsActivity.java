package com.livestrong.calorietracker.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.livestrong.calorietracker.R;
import com.livestrong.calorietracker.back.db.ModelManager;
import com.livestrong.calorietracker.back.db.gen.Food;
import com.livestrong.calorietracker.back.db.gen.Profile;


public class NutritionFactsActivity extends CalorieTrackerBaseActivity {
	
	private Food mFood;
	
	private class UserProfileTask extends AsyncTask<Void, Void, Profile>
	{
		@Override
		protected Profile doInBackground(Void... params)
		{
			return ModelManager.sharedModelManager().getUserProfile();
		}
		
		protected void onPostExecute(Profile profile)
		{
			if (profile != null) 
			{
				((TextView) findViewById(R.id.totalFatDailyTextView)).setText(getDailyValue(profile.getCaloriesGoal(), mFood.getFat(), 65)+"%");
	            ((TextView) findViewById(R.id.satFatDailyTextView)).setText(getDailyValue(profile.getCaloriesGoal(), mFood.getSaturatedFat(), 20)+"%");
	            ((TextView) findViewById(R.id.cholesterolDailyTextView)).setText(getDailyValue(profile.getCaloriesGoal(), mFood.getCholesterol(), 300)+"%");
	            ((TextView) findViewById(R.id.sodiumDailyTextView)).setText(getDailyValue(profile.getCaloriesGoal(), mFood.getSodium(), 2300)+"%");
	            ((TextView) findViewById(R.id.carbsDailyTextView)).setText(getDailyValue(profile.getCaloriesGoal(), mFood.getCarbs(), 300)+"%");
	            ((TextView) findViewById(R.id.dietaryFiberDailyTextView)).setText(getDailyValue(profile.getCaloriesGoal(), mFood.getDietaryFiber(), 25)+"%");
	            ((TextView) findViewById(R.id.proteinDailyTextView)).setText(getDailyValue(profile.getCaloriesGoal(), mFood.getProtein(), 50)+"%");

	            ((TextView) findViewById(R.id.fatPercTextView)).setText(Math.round(mFood.getPercentCaloriesFromFat() * 100)+"%");
	            ((TextView) findViewById(R.id.carbsPercTextView)).setText(Math.round(mFood.getPercentCaloriesFromCarbs()* 100)+"%");
	            ((TextView) findViewById(R.id.proteinPercTextView)).setText(Math.round(mFood.getPercentCaloriesFromProtein()* 100)+"%");
	        }
		};

	};

	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        if (savedInstanceState != null) {
        	// The activity was destroyed, and is about to be started again. You need to restore your activity state from savedInstanceState.
            // UI elements states are restored automatically by super.onCreate()
        }

        // The activity is being created; create views, bind data to lists, etc.
        setContentView(R.layout.activity_nutrition_facts);
        
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
        	mFood = (Food) extras.get(Food.class.getName());

            ((TextView) findViewById(R.id.servingSizeTextView)).setText(mFood.getServingSize());
            ((TextView) findViewById(R.id.caloriesTextView)).setText(mFood.getCalories()+"");
            ((TextView) findViewById(R.id.caloriesFromFatTextView)).setText(mFood.getCaloriesFromFat()+"");

            ((TextView) findViewById(R.id.totalFatTextView)).setText(Math.round(mFood.getFat())+"g");
            ((TextView) findViewById(R.id.satFatTextView)).setText(Math.round(mFood.getSaturatedFat())+"g");
            ((TextView) findViewById(R.id.cholesterolTextView)).setText(Math.round(mFood.getCholesterol())+"mg");
            ((TextView) findViewById(R.id.sodiumTextView)).setText(Math.round(mFood.getSodium())+"mg");
            ((TextView) findViewById(R.id.carbsTextView)).setText(Math.round(mFood.getCarbs())+"g");
            ((TextView) findViewById(R.id.dietaryFiberTextView)).setText(Math.round(mFood.getDietaryFiber())+"g");
            ((TextView) findViewById(R.id.sugarsTextView)).setText(Math.round(mFood.getSugars())+"g");
            ((TextView) findViewById(R.id.proteinTextView)).setText(Math.round(mFood.getProtein())+"g");

            // Daily values
            new UserProfileTask().execute(new Void[]{});
            
        }
        getActionBar().setDisplayHomeAsUpEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getActionBar().setHomeButtonEnabled(true);
        }
    }

    // Initialize menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            Intent intent = new Intent(this, TabBarActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);

    }
	
	private int getDailyValue(int caloriesGoal, double grams, double rdv) {
		double dailyValueRatio = caloriesGoal / 2000.0;
		return (int) Math.round(100 * (grams / (rdv * dailyValueRatio)));
	}

    @Override
    protected void onStart() {
        super.onStart();
        // The activity is about to become visible.
        // -> onResume()
    }

    @Override
    protected void onResume() {
        super.onResume();
        // The activity has become visible (it is now "resumed").
        
        
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    	// Called before making the activity vulnerable to destruction; save your activity state in outState.
        // UI elements states are saved automatically by super.onSaveInstanceState()
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Another activity is taking focus (this activity is about to be "paused"); commit unsaved changes to persistent data, etc.
        // -> onStop()
    }

    @Override
    protected void onStop() {
        super.onStop();
        // The activity is no longer visible (it is now "stopped")
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        // The activity was stopped, and is about to be started again. It was not destroyed, so all members are intact.
        // -> onStart()
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // The activity is about to be destroyed.
        if (isFinishing()) {
        	// Someone called finish()
        } else {
        	// System is temporarily destroying this instance of the activity to save space
        }
    }
}
