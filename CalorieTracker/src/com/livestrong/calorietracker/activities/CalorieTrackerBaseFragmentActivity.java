package com.livestrong.calorietracker.activities;


import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import com.livestrong.calorietracker.MyPlateApplication;

public class CalorieTrackerBaseFragmentActivity extends FragmentActivity{
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(this.getClass().getName(), "Creating Activity");
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		// Make sure nobody tries to use the DatabaseHelper without extending one of the AbstractLiveStrong*Activity classes.
		Log.d(this.getClass().getName(), "Destroying Activity");
	}

	@Override
    protected void onResume() {
        super.onResume();
        // The activity has become visible (it is now "resumed").
        
        // We track running activities like this, to be able to detect when the app gets backgrounded.
        MyPlateApplication app = (MyPlateApplication) getApplication();
        app.plusActivity(this);
		Log.d(this.getClass().getName(), "Resuming Activity");

    }

    @Override
    protected void onPause() {
        super.onPause();
        // Another activity is taking focus (this activity is about to be "paused"); commit unsaved changes to persistent data, etc.
        // -> onStop()

        // We track running activities like this, to be able to detect when the app gets backgrounded.
        MyPlateApplication app = (MyPlateApplication) getApplication();
        app.minusActivity();
        
		Log.d(this.getClass().getName(), "Pausing Activity");

    }

}
