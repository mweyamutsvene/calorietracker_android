package com.livestrong.calorietracker.activities;

import android.app.ActionBar;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.*;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import com.livestrong.calorietracker.MyPlateApplication;
import com.livestrong.calorietracker.R;
import com.livestrong.calorietracker.fragments.CommunityFragment;
import com.livestrong.calorietracker.fragments.DiaryFragment;
import com.livestrong.calorietracker.fragments.MoreFragment;
import com.livestrong.calorietracker.fragments.MyPlateFragment;
import com.livestrong.calorietracker.fragments.ProgressFragment;

import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.widget.TextView;

import java.util.Date;

public class TabBarActivity extends CalorieTrackerBaseFragmentActivity  {

    private Fragment mCurrentFragment;
    private MyPlateFragment mMyPlateFragment;
    private DiaryFragment mDiaryFragment;
    private CommunityFragment mCommunityFragment;
    private ProgressFragment mProgressFragment;

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private CharSequence mDrawerTitle;
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTitle = mDrawerTitle = getTitle();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        // set a custom shadow that overlays the main content when the drawer opens
//        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener
        mDrawerList.setAdapter(new BaseAdapter(){
            @Override
            public int getCount() {
                return 4;
            }

            @Override
            public Object getItem(int i) {
                return null;
            }

            @Override
            public long getItemId(int i) {
                return 0;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                //Create cell if it doesn't exist already
                if (convertView == null || convertView.getId() != R.id.listItemDrawer) {
                    convertView = LayoutInflater.from(MyPlateApplication.getContext()).inflate(R.layout.list_item_drawer, null);

                }
                //get the cell items
                ImageView imageView = (ImageView)convertView.findViewById(R.id.imageView);
                TextView textView = (TextView)convertView.findViewById(R.id.textView);

                //Set up the different cells in the tableview
                switch (position){
                    case 0:{
                        imageView.setImageResource(R.drawable.ic_myplate);
                        textView.setText("My Plate");
                        break;
                    }
                    case 1:{
                        imageView.setImageResource(R.drawable.ic_diary);
                        textView.setText("Diary");
                        break;

                    }
                    case 2:{
                        imageView.setImageResource(R.drawable.ic_progress);
                        textView.setText("Progress");
                        break;

                    }
                    case 3:{
                        imageView.setImageResource(R.drawable.ic_community);
                        textView.setText("Community");
                        break;

                    }
                }
                return convertView;
            }
        });


        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // enable ActionBar app icon to behave as action to toggle nav drawer
        getActionBar().setDisplayHomeAsUpEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getActionBar().setHomeButtonEnabled(true);
        }

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()

            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if(mMyPlateFragment == null){
            //Create Fragment if it hasn't already
            mMyPlateFragment = new MyPlateFragment();
        }


        getActionBar().setTitle("My Plate");
        mCurrentFragment = mMyPlateFragment;
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, mMyPlateFragment);
        fragmentTransaction.commit();

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item)) {

            return true;
        }

        // Handle action buttons
        switch(item.getItemId()) {
            case R.id.action_profile:{
                Intent intent = new Intent(this, MoreFragment.class);
                intent.putExtra(Fragment.class.getName(),R.id.action_profile);
                startActivity(intent);
                return true;
            }
            case R.id.action_about:{
                Intent intent = new Intent(this, MoreFragment.class);
                intent.putExtra(Fragment.class.getName(),R.id.action_about);
                startActivity(intent);
                return true;
            }
            case R.id.action_settings:{
                Intent intent = new Intent(this, MoreFragment.class);
                intent.putExtra(Fragment.class.getName(),R.id.action_settings);
                startActivity(intent);
                return true;
            }
            case R.id.action_support:{
                Intent intent = new Intent(this, MoreFragment.class);
                intent.putExtra(Fragment.class.getName(),R.id.action_support);
                startActivityForResult(intent, 0);
                return true;
            }
            case R.id.action_track:{
                if(mCurrentFragment == mDiaryFragment){
                   return mDiaryFragment.onOptionsItemSelected(item);
                }
                Intent intent = new Intent(this, TrackActivity.class);
                intent.putExtra(Date.class.getName(),new Date());
                startActivity(intent);
                return true;
            }
            default:
                getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);

                return   super.onOptionsItemSelected(item);

        }
    }


    /**
     *
     *  displays Myplate fragment
     *
     **/
    public void displayMyPlateTab(){
        if(mMyPlateFragment == null){
            //Create Fragment if it hasn't already
            mMyPlateFragment = new MyPlateFragment();
        }


        getActionBar().setTitle("My Plate");
        mCurrentFragment = mMyPlateFragment;

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, mMyPlateFragment);
//        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }

    /**
     *
     *  displays Diary fragment
     *
     **/
    public void displayDiaryTab(){
        if(mDiaryFragment == null){
            //Create Fragment if it hasn't already
            mDiaryFragment = new DiaryFragment();
        }



        getActionBar().setTitle("Diary");
        if(mCurrentFragment == mDiaryFragment){
            mDiaryFragment.onResume();
            return;
        }
        mCurrentFragment = mDiaryFragment;

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, mDiaryFragment);
//        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();


    }

    /**
     *
     *  displays Community fragment
     *
     **/
    public void displayCommunityTab(){
        if(mCommunityFragment == null){
            //Create Fragment if it hasn't already
            mCommunityFragment = new CommunityFragment();
        }


        getActionBar().setTitle("Community");

        mCurrentFragment = mCommunityFragment;

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, mCommunityFragment);
//        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();


    }
    /**
     *
     *  displays Progress fragment
     *
     **/
    public void displayProgressTab(){
        if(mProgressFragment == null){
            mProgressFragment = new ProgressFragment();
        }


        getActionBar().setTitle("Progress");

        mCurrentFragment = mProgressFragment;
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, mProgressFragment);
//        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();


    }

    /* The click listner for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
           selectItem(position);
        }
    }

    private void selectItem(int position) {
        // update the main content by replacing fragments
        switch (position) {
            case 0:
                displayMyPlateTab();
                break;
            case 1:
                displayDiaryTab();
                break;
            case 2:
                displayProgressTab();
                break;
            case 3:
                displayCommunityTab();
                break;
        }

        // update selected item and title, then close the drawer
        mTitle = getActionBar().getTitle();
        mDrawerList.setItemChecked(position, true);
        mDrawerLayout.closeDrawer(mDrawerList);

    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK){
            Intent intent = new Intent(this, WelcomeActivity.class);
            startActivity(intent);

            finish();
        }

        super.onActivityResult(requestCode, resultCode, data);


    }
}
