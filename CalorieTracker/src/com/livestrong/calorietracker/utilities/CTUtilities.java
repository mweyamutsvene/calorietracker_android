package com.livestrong.calorietracker.utilities;

import com.livestrong.calorietracker.configuration.MyPlateDefaults;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;

public class CTUtilities {
    public final static double KG_PER_POUND = 0.45359237;
    public final static double POUND_PER_STONES = 14;
	public static final String md5(final String s) {
	    try {
	        // Create MD5 Hash
	        MessageDigest digest = java.security.MessageDigest
	                .getInstance("MD5");
	        digest.update(s.getBytes());
	        byte messageDigest[] = digest.digest();

	        // Create Hex String
	        StringBuffer hexString = new StringBuffer();
	        for (int i = 0; i < messageDigest.length; i++) {
	            String h = Integer.toHexString(0xFF & messageDigest[i]);
	            while (h.length() < 2)
	                h = "0" + h;
	            hexString.append(h);
	        }
	        return hexString.toString();

	    } catch (NoSuchAlgorithmException e) {
	        e.printStackTrace();
	    }
	    return "";
	}
	
	public static final String dataToMd5(final byte[] bytes) {
		//Convert data into md5 hash string
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		byte[] thedigest = md.digest(bytes);
		BigInteger bigInt = new BigInteger(1,thedigest);
		String hashString = bigInt.toString(16);
		return hashString;

	}
    public static String getWeightWithUnits(double weight) {
        MyPlateDefaults.WeightUnits unitsPreference = MyPlateDefaults.WeightUnits.valueOf((String) MyPlateDefaults.getPref(MyPlateDefaults.PREFS_WEIGHT_UNITS, MyPlateDefaults.WeightUnits.POUNDS.toString()));

        DecimalFormat decimalFormat = new DecimalFormat("#.##");

        switch (unitsPreference) {
            case KILOGRAMS:
                return decimalFormat.format(weight * KG_PER_POUND) + "kg";
            case POUNDS:
                return decimalFormat.format(weight) + " pounds";
            case STONES:
                double stones = weight / POUND_PER_STONES;
                return decimalFormat.format(stones) + " stones"; // This is not a typo; the plural of stone is stone (no s), when used as a measurement unit. Ref: http://en.wikipedia.org/wiki/Stone_(Imperial_mass)#Current_use
        }
        return "Unknown";
    }

    public static double getWeightForSelectedUnits(double weight){
        MyPlateDefaults.WeightUnits unitsPreference = MyPlateDefaults.WeightUnits.valueOf( (String) MyPlateDefaults.getPref(MyPlateDefaults.PREFS_WEIGHT_UNITS, MyPlateDefaults.WeightUnits.POUNDS.toString()));
        switch (unitsPreference) {
            case KILOGRAMS:
                return (weight * KG_PER_POUND);
            case POUNDS:
                return weight;
            case STONES:
            default:
                return (double) weight / POUND_PER_STONES;
        }
    }

    public static double convertWeightFromSelectedUnits(double weight){
        MyPlateDefaults.WeightUnits unitsPreference = MyPlateDefaults.WeightUnits.valueOf( (String) MyPlateDefaults.getPref(MyPlateDefaults.PREFS_WEIGHT_UNITS, MyPlateDefaults.WeightUnits.POUNDS.toString()));
        switch (unitsPreference) {
            case KILOGRAMS:
                return (weight / KG_PER_POUND);
            case POUNDS:
                return weight;
            case STONES:
            default:
                return (double) weight * POUND_PER_STONES;
        }
    }

}
