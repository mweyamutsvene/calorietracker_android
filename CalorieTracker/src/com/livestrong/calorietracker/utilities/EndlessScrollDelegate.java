package com.livestrong.calorietracker.utilities;

public interface EndlessScrollDelegate {

	public void loadListDataForPage(int page);
	
}
