package com.livestrong.calorietracker.utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONSafeObject extends JSONObject {

	static  public  Integer safeGetInt(String key, JSONObject object){
		if(!object.has(key)){
			return 0;
		}
		String value = null;
		try {
			 value = object.getString(key);
             value = value.replaceAll("[^0-9.]", "" );
			 if(value.equals(""))
				 return 0;
			 
			 
		} catch (JSONException e) {
			e.printStackTrace();
			return 0;
		}
		
		Integer safeValue = Integer.parseInt(value);
		return safeValue;
	}
	
	static public Float safeGetFloat(String key, JSONObject object){
		if(!object.has(key)){
			return 0.0f;
		}
		String value = null;
		try {
			 value = object.getString(key);
            value = value.replaceAll("[^0-9.]", "" );
			 if(value.equals(""))
				 return 0.0f;
			 
			 
		} catch (JSONException e) {
			e.printStackTrace();
			return 0.0f;
		}
		
		Float safeValue = Float.parseFloat(value);
		return safeValue;
	}
	
	static public Double safeGetDouble(String key, JSONObject object){
		if(!object.has(key)){
			return 0.0;
		}
		String value = null;
		try {
			 value = object.getString(key);
			 value = value.replaceAll("[^0-9.]", "" );
			 if(value.equals(""))
				 return 0.0;
			 
			 
		} catch (JSONException e) {
			e.printStackTrace();
			return 0.0;
		}
		
		Double safeValue = Double.parseDouble(value);
		return safeValue;
	}
	
	static public String safeGetString(String key, JSONObject object){
		if(!object.has(key)){
			return "";
		}
		String value = null;
		try {
			 value = object.getString(key);
			  
		} catch (JSONException e) {
			e.printStackTrace();
			return "";
		}
		
		return value;
	}
	
	static public Boolean safeGetBoolean(String key, JSONObject object){
		if(!object.has(key)){
			return null;	
		}
		Boolean value = false;
		try {
			 value = object.getBoolean(key);
			  
		} catch (JSONException e) {
			e.printStackTrace();
            try {
                value = object.getInt(key) > 0;
            } catch (JSONException e1) {
                e1.printStackTrace();
                return false;
            }
            return value;
		}
		
		return value;
	}
	static public JSONObject safeGetJSONObject(String key, JSONObject object){
		if(!object.has(key)){
			return null;	
		}
		JSONObject value = null;
		try {
			 value = object.getJSONObject(key);
			  
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		
		return value;
	}
    static public JSONArray safeGetJSONArray(String key, JSONObject object){
        if(!object.has(key)){
            return null;
        }
        JSONArray value = null;
        try {
            value = object.getJSONArray(key);

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        return value;
    }
	
}
