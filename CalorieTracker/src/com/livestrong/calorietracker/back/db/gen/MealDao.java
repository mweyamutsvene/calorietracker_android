package com.livestrong.calorietracker.back.db.gen;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table MEAL.
*/
public class MealDao extends AbstractDao<Meal, Long> {

    public static final String TABLENAME = "MEAL";

    /**
     * Properties of entity Meal.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property Calories = new Property(1, Integer.class, "calories", false, "CALORIES");
        public final static Property DateCreated = new Property(2, java.util.Date.class, "dateCreated", false, "DATE_CREATED");
        public final static Property DateDeleted = new Property(3, java.util.Date.class, "dateDeleted", false, "DATE_DELETED");
        public final static Property DateModified = new Property(4, java.util.Date.class, "dateModified", false, "DATE_MODIFIED");
        public final static Property Name = new Property(5, String.class, "name", false, "NAME");
        public final static Property RemoteId = new Property(6, String.class, "remoteId", false, "REMOTE_ID");
    };

    private DaoSession daoSession;


    public MealDao(DaoConfig config) {
        super(config);
    }
    
    public MealDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
        this.daoSession = daoSession;
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "'MEAL' (" + //
                "'_id' INTEGER PRIMARY KEY AUTOINCREMENT ," + // 0: id
                "'CALORIES' INTEGER," + // 1: calories
                "'DATE_CREATED' INTEGER," + // 2: dateCreated
                "'DATE_DELETED' INTEGER," + // 3: dateDeleted
                "'DATE_MODIFIED' INTEGER," + // 4: dateModified
                "'NAME' TEXT," + // 5: name
                "'REMOTE_ID' TEXT);"); // 6: remoteId
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "'MEAL'";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, Meal entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        Integer calories = entity.getCalories();
        if (calories != null) {
            stmt.bindLong(2, calories);
        }
 
        java.util.Date dateCreated = entity.getDateCreated();
        if (dateCreated != null) {
            stmt.bindLong(3, dateCreated.getTime());
        }
 
        java.util.Date dateDeleted = entity.getDateDeleted();
        if (dateDeleted != null) {
            stmt.bindLong(4, dateDeleted.getTime());
        }
 
        java.util.Date dateModified = entity.getDateModified();
        if (dateModified != null) {
            stmt.bindLong(5, dateModified.getTime());
        }
 
        String name = entity.getName();
        if (name != null) {
            stmt.bindString(6, name);
        }
 
        String remoteId = entity.getRemoteId();
        if (remoteId != null) {
            stmt.bindString(7, remoteId);
        }
    }

    @Override
    protected void attachEntity(Meal entity) {
        super.attachEntity(entity);
        entity.__setDaoSession(daoSession);
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public Meal readEntity(Cursor cursor, int offset) {
        Meal entity = new Meal( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getInt(offset + 1), // calories
            cursor.isNull(offset + 2) ? null : new java.util.Date(cursor.getLong(offset + 2)), // dateCreated
            cursor.isNull(offset + 3) ? null : new java.util.Date(cursor.getLong(offset + 3)), // dateDeleted
            cursor.isNull(offset + 4) ? null : new java.util.Date(cursor.getLong(offset + 4)), // dateModified
            cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5), // name
            cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6) // remoteId
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, Meal entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setCalories(cursor.isNull(offset + 1) ? null : cursor.getInt(offset + 1));
        entity.setDateCreated(cursor.isNull(offset + 2) ? null : new java.util.Date(cursor.getLong(offset + 2)));
        entity.setDateDeleted(cursor.isNull(offset + 3) ? null : new java.util.Date(cursor.getLong(offset + 3)));
        entity.setDateModified(cursor.isNull(offset + 4) ? null : new java.util.Date(cursor.getLong(offset + 4)));
        entity.setName(cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5));
        entity.setRemoteId(cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6));
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(Meal entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(Meal entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
}
