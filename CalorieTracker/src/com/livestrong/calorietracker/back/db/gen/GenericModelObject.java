package com.livestrong.calorietracker.back.db.gen;

public interface GenericModelObject {
	public String getRemoteId();
}

