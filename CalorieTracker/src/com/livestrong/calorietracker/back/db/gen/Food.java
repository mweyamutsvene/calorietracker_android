package com.livestrong.calorietracker.back.db.gen;

import java.util.List;

import de.greenrobot.dao.DaoException;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;

import com.livestrong.calorietracker.back.db.LiveStrongDisplayableListItem;

import com.livestrong.calorietracker.back.db.sync.ModelSyncCompletedDelegate;
import com.livestrong.calorietracker.utilities.JSONSafeObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
// KEEP INCLUDES END
/**
 * Entity mapped to table FOOD.
 */
public class Food implements GenericModelObject, LiveStrongDisplayableListItem, Serializable {

    private Long id;
    private String brand;
    private Integer calories;
    private Integer caloriesFromFat;
    private Float carbs;
    private Float cholesterol;
    private java.util.Date dateCreated;
    private java.util.Date dateDeleted;
    private java.util.Date dateModified;
    private Float dietaryFiber;
    private Float fat;
    private Boolean fullyPopulated;
    private String imageUrl;
    private String name;
    private Float percentCaloriesFromCarbs;
    private Float percentCaloriesFromFat;
    private Float percentCaloriesFromProtein;
    private Float protein;
    private String remoteId;
    private Float saturatedFat;
    private String servingSize;
    private Float sodium;
    private Float sugars;
    private Boolean verified;

    /** Used to resolve relations */
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    private transient FoodDao myDao;

    private List<DiaryEntry> foodDiaryEntries;
    private List<MealItem> mealItemList;

    // KEEP FIELDS - put your custom fields here
	public static Integer customFoodId = 256940;
    public final static int CALORIES_PER_FAT = 9;
    public final static int CALORIES_PER_CARBS = 4;
    public final static int CALORIES_PER_PROTEIN = 4;
    public Food(Boolean isCustom) {
        if(!isCustom){
          return;
        }
        this.setRemoteId(customFoodId+"");
        this.setName("My Custom Food");
        this.setCalories(500);
        this.setServingSize("1 Serving");
    }
    // KEEP FIELDS END

    public Food() {
    }

    public Food(Long id) {
        this.id = id;
    }

    public Food(Long id, String brand, Integer calories, Integer caloriesFromFat, Float carbs, Float cholesterol, java.util.Date dateCreated, java.util.Date dateDeleted, java.util.Date dateModified, Float dietaryFiber, Float fat, Boolean fullyPopulated, String imageUrl, String name, Float percentCaloriesFromCarbs, Float percentCaloriesFromFat, Float percentCaloriesFromProtein, Float protein, String remoteId, Float saturatedFat, String servingSize, Float sodium, Float sugars, Boolean verified) {
        this.id = id;
        this.brand = brand;
        this.calories = calories;
        this.caloriesFromFat = caloriesFromFat;
        this.carbs = carbs;
        this.cholesterol = cholesterol;
        this.dateCreated = dateCreated;
        this.dateDeleted = dateDeleted;
        this.dateModified = dateModified;
        this.dietaryFiber = dietaryFiber;
        this.fat = fat;
        this.fullyPopulated = fullyPopulated;
        this.imageUrl = imageUrl;
        this.name = name;
        this.percentCaloriesFromCarbs = percentCaloriesFromCarbs;
        this.percentCaloriesFromFat = percentCaloriesFromFat;
        this.percentCaloriesFromProtein = percentCaloriesFromProtein;
        this.protein = protein;
        this.remoteId = remoteId;
        this.saturatedFat = saturatedFat;
        this.servingSize = servingSize;
        this.sodium = sodium;
        this.sugars = sugars;
        this.verified = verified;
    }

    /** called by internal mechanisms, do not call yourself. */
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getFoodDao() : null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Integer getCalories() {
        return calories;
    }

    public void setCalories(Integer calories) {
        this.calories = calories;
    }

    public Integer getCaloriesFromFat() {
        return caloriesFromFat;
    }

    public void setCaloriesFromFat(Integer caloriesFromFat) {
        this.caloriesFromFat = caloriesFromFat;
    }

    public Float getCarbs() {
        return carbs;
    }

    public void setCarbs(Float carbs) {
        this.carbs = carbs;
    }

    public Float getCholesterol() {
        return cholesterol;
    }

    public void setCholesterol(Float cholesterol) {
        this.cholesterol = cholesterol;
    }

    public java.util.Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(java.util.Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public java.util.Date getDateDeleted() {
        return dateDeleted;
    }

    public void setDateDeleted(java.util.Date dateDeleted) {
        this.dateDeleted = dateDeleted;
    }

    public java.util.Date getDateModified() {
        return dateModified;
    }

    public void setDateModified(java.util.Date dateModified) {
        this.dateModified = dateModified;
    }

    public Float getDietaryFiber() {
        return dietaryFiber;
    }

    public void setDietaryFiber(Float dietaryFiber) {
        this.dietaryFiber = dietaryFiber;
    }

    public Float getFat() {
        return fat;
    }

    public void setFat(Float fat) {
        this.fat = fat;
    }

    public Boolean getFullyPopulated() {
        return fullyPopulated;
    }

    public void setFullyPopulated(Boolean fullyPopulated) {
        this.fullyPopulated = fullyPopulated;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPercentCaloriesFromCarbs() {
        return percentCaloriesFromCarbs;
    }

    public void setPercentCaloriesFromCarbs(Float percentCaloriesFromCarbs) {
        this.percentCaloriesFromCarbs = percentCaloriesFromCarbs;
    }

    public Float getPercentCaloriesFromFat() {
        return percentCaloriesFromFat;
    }

    public void setPercentCaloriesFromFat(Float percentCaloriesFromFat) {
        this.percentCaloriesFromFat = percentCaloriesFromFat;
    }

    public Float getPercentCaloriesFromProtein() {
        return percentCaloriesFromProtein;
    }

    public void setPercentCaloriesFromProtein(Float percentCaloriesFromProtein) {
        this.percentCaloriesFromProtein = percentCaloriesFromProtein;
    }

    public Float getProtein() {
        return protein;
    }

    public void setProtein(Float protein) {
        this.protein = protein;
    }

    public String getRemoteId() {
        return remoteId;
    }

    public void setRemoteId(String remoteId) {
        this.remoteId = remoteId;
    }

    public Float getSaturatedFat() {
        return saturatedFat;
    }

    public void setSaturatedFat(Float saturatedFat) {
        this.saturatedFat = saturatedFat;
    }

    public String getServingSize() {
        return servingSize;
    }

    public void setServingSize(String servingSize) {
        this.servingSize = servingSize;
    }

    public Float getSodium() {
        return sodium;
    }

    public void setSodium(Float sodium) {
        this.sodium = sodium;
    }

    public Float getSugars() {
        return sugars;
    }

    public void setSugars(Float sugars) {
        this.sugars = sugars;
    }

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }

    /** To-many relationship, resolved on first access (and after reset). Changes to to-many relations are not persisted, make changes to the target entity. */
    public List<DiaryEntry> getFoodDiaryEntries() {
        if (foodDiaryEntries == null) {
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            DiaryEntryDao targetDao = daoSession.getDiaryEntryDao();
            List<DiaryEntry> foodDiaryEntriesNew = targetDao._queryFood_FoodDiaryEntries(id);
            synchronized (this) {
                if(foodDiaryEntries == null) {
                    foodDiaryEntries = foodDiaryEntriesNew;
                }
            }
        }
        return foodDiaryEntries;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    public synchronized void resetFoodDiaryEntries() {
        foodDiaryEntries = null;
    }

    /** To-many relationship, resolved on first access (and after reset). Changes to to-many relations are not persisted, make changes to the target entity. */
    public List<MealItem> getMealItemList() {
        if (mealItemList == null) {
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            MealItemDao targetDao = daoSession.getMealItemDao();
            List<MealItem> mealItemListNew = targetDao._queryFood_MealItemList(id);
            synchronized (this) {
                if(mealItemList == null) {
                    mealItemList = mealItemListNew;
                }
            }
        }
        return mealItemList;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    public synchronized void resetMealItemList() {
        mealItemList = null;
    }

    /** Convenient call for {@link AbstractDao#delete(Object)}. Entity must attached to an entity context. */
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.delete(this);
    }

    /** Convenient call for {@link AbstractDao#update(Object)}. Entity must attached to an entity context. */
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.update(this);
    }

    /** Convenient call for {@link AbstractDao#refresh(Object)}. Entity must attached to an entity context. */
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.refresh(this);
    }

    // KEEP METHODS - put your custom methods here

    public static Food getFood(String foodId, FoodDao context){
    	return context.queryBuilder().where(FoodDao.Properties.RemoteId.eq(foodId)).list().get(0);
    }

    private static float setPercentCaloriesValue(Object object){
    	if(object.getClass().getName().equals("null"))
    		
    		return 0.0f;
    	
    	String serverValue = object.toString();
    	if(serverValue != null && serverValue.length() > 0){
    		float percentServerValue = Float.parseFloat(serverValue)/100;
    		return percentServerValue;
    	}
    	return 0.0f;
    }
    
   
    public static List<Food> getFoodsFromResponse(JSONArray foodArray, FoodDao context){
        List<Food> resultFoods = new ArrayList<Food>();
		for(int i = 0; i < foodArray.length(); i++){
			JSONObject foodObject;
			try {
				foodObject = foodArray.getJSONObject(i);

                Food food = new Food();

                food.remoteId = JSONSafeObject.safeGetString("food_id", foodObject);
				food.name = JSONSafeObject.safeGetString("item_title", foodObject);
				food.calories = JSONSafeObject.safeGetInt("cals", foodObject);
				food.servingSize = JSONSafeObject.safeGetString("serving_size", foodObject);
				food.brand = JSONSafeObject.safeGetString("item_brand", foodObject);
				food.caloriesFromFat = JSONSafeObject.safeGetInt("cals_from_fat", foodObject);
				food.fat = JSONSafeObject.safeGetFloat("fat", foodObject);
				food.carbs = JSONSafeObject.safeGetFloat("carbs", foodObject);
				food.protein = JSONSafeObject.safeGetFloat("protein", foodObject);
				food.saturatedFat = JSONSafeObject.safeGetFloat("sat_fat", foodObject);
				food.dietaryFiber = JSONSafeObject.safeGetFloat("dietary_fiber", foodObject);
				food.sugars = JSONSafeObject.safeGetFloat("sugars", foodObject);
				food.sodium = JSONSafeObject.safeGetFloat("sodium", foodObject);
				food.verified = JSONSafeObject.safeGetBoolean("verified", foodObject);
                food.cholesterol = JSONSafeObject.safeGetFloat("cholesterol",foodObject);
				JSONObject imageDict = JSONSafeObject.safeGetJSONObject("images", foodObject);
				String imageUrl = JSONSafeObject.safeGetString("100", imageDict);
				if(imageUrl !=null && !imageUrl.contains("noLogo") && !imageUrl.contains("recipe")){
					food.imageUrl = imageUrl;
				}
				
				food.percentCaloriesFromFat = setPercentCaloriesValue((!foodObject.isNull("cals_perc_fat")?foodObject.getString("cals_perc_fat"):"0"));
				if(food.percentCaloriesFromFat == 0){
                    food.setPercentCaloriesFromFat(food.fat * 9.0f / food.calories);
				}
				
				food.percentCaloriesFromCarbs = setPercentCaloriesValue((!foodObject.isNull("cals_perc_carbs")?foodObject.getString("cals_perc_carbs"):"0"));
				if(food.percentCaloriesFromCarbs == 0){
                    food.setPercentCaloriesFromCarbs(food.carbs * 4.0f / food.calories);
				}

				food.percentCaloriesFromProtein = setPercentCaloriesValue((!foodObject.isNull("cals_perc_protein")?foodObject.getString("cals_perc_protein"):"0"));
				if(food.percentCaloriesFromProtein == 0){
                    food.setPercentCaloriesFromProtein(food.protein * 4.0f / food.calories);
				}

                if(food.remoteId == null || food.isCustom() || food.remoteId.equals("")){
                    Log.d("Debug", "Null exercise");
                }
				food.fullyPopulated = true;

                if(!food.isCustom()){
                    //Delete old duplicates
                    List<Food> foods = context.queryBuilder().where(FoodDao.Properties.RemoteId.eq(food.remoteId)).list();
                    if(foods.size() > 0){
                        for(int index = 0; index < foods.size(); index++){
                            Food oldFood = foods.get(index);
                            context.delete(oldFood);
                        }
                    }
                }
                else{
                    //Delete old duplicates
                    List<Food> foods = context.queryBuilder().where(FoodDao.Properties.Name.eq(food.name)).list();
                    if(foods.size() > 0){
                        for(int index = 0; index < foods.size(); index++){
                            Food oldFood = foods.get(index);
                            context.delete(oldFood);
                        }
                    }
                }
				//Insert newly updated food
				context.insert(food);
                resultFoods.add(food);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
        return resultFoods;
    	
    }
    public Boolean isCustom(){
        return this.getRemoteId().equals(customFoodId+"");
    }
    @Override
    public String getTitle() {
        return this.name;
    }

    @Override
    public String getDescription() {
        return this.servingSize + ", " + getCalories() + " calories";
    }

    @Override
    public String getSmallImage() {
        if (this.imageUrl != null) {
            String url = this.imageUrl;
            if (url.contains("nologo.gif")) {
                return "";
            }
            return url;
        } else {
            return "";
        }
    }
    public static List<Food> getCustomFoods(FoodDao dao) {

        return dao.queryBuilder()
                .where(FoodDao.Properties.RemoteId.eq(Food.customFoodId))
                .orderRaw("NAME").list();

    }
    public static List<Food> getFoodsLike(FoodDao dao,String likeString) {

        return dao.queryBuilder()
                .where(FoodDao.Properties.Name.like("%" + likeString + "%"))
                .orderDesc(FoodDao.Properties.Name)
                .list();

    }

    public static byte[] serializeObject(Object o) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {
            ObjectOutput out = new ObjectOutputStream(bos);
            out.writeObject(o);
            out.close();

            // Get the bytes of the serialized object
            byte[] buf = bos.toByteArray();

            return buf;
        } catch(IOException ioe) {
            Log.e("serializeObject", "error", ioe);

            return null;
        }
    }
    public static Object deserializeObject(byte[] b) {
        try {
            ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(b));
            Object object = in.readObject();
            in.close();

            return object;
        } catch(ClassNotFoundException cnfe) {
            Log.e("deserializeObject", "class not found error", cnfe);

            return null;
        } catch(IOException ioe) {
            Log.e("deserializeObject", "io error", ioe);

            return null;
        }
    }

    static public class SyncFoodTask extends AsyncTask<JSONArray, Void, List<Food>> {
    	private ModelSyncCompletedDelegate delegate;
    	private FoodDao foodDao;
    	public SyncFoodTask(ModelSyncCompletedDelegate delegate, FoodDao foodDao) {
    		this.delegate = delegate;
    		this.foodDao = foodDao;
    	}

		@Override
		protected List<Food> doInBackground(JSONArray... params) {
			JSONArray foodObjects = (JSONArray)params[0];
			return getFoodsFromResponse(foodObjects, foodDao);
			
		}

        @Override
        protected void onPostExecute(List<Food> object) {
            delegate.finishedSyncThreaded(ModelSyncCompletedDelegate.ModelSyncType.SYNC_TYPE_FOOD, object);
            return;
        }
  
    }
    // KEEP METHODS END

}
