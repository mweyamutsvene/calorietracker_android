package com.livestrong.calorietracker.back.db.gen;

import android.database.sqlite.SQLiteDatabase;

import java.util.Map;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.AbstractDaoSession;
import de.greenrobot.dao.identityscope.IdentityScopeType;
import de.greenrobot.dao.internal.DaoConfig;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * {@inheritDoc}
 * 
 * @see de.greenrobot.dao.AbstractDaoSession
 */
public class DaoSession extends AbstractDaoSession {

    private final DaoConfig diaryEntryDaoConfig;
    private final DaoConfig exerciseDaoConfig;
    private final DaoConfig foodDaoConfig;
    private final DaoConfig mealDaoConfig;
    private final DaoConfig mealItemDaoConfig;
    private final DaoConfig profileDaoConfig;
    private final DaoConfig communityMessageDaoConfig;

    private final DiaryEntryDao diaryEntryDao;
    private final ExerciseDao exerciseDao;
    private final FoodDao foodDao;
    private final MealDao mealDao;
    private final MealItemDao mealItemDao;
    private final ProfileDao profileDao;
    private final CommunityMessageDao communityMessageDao;

    public DaoSession(SQLiteDatabase db, IdentityScopeType type, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig>
            daoConfigMap) {
        super(db);

        diaryEntryDaoConfig = daoConfigMap.get(DiaryEntryDao.class).clone();
        diaryEntryDaoConfig.initIdentityScope(type);

        exerciseDaoConfig = daoConfigMap.get(ExerciseDao.class).clone();
        exerciseDaoConfig.initIdentityScope(type);

        foodDaoConfig = daoConfigMap.get(FoodDao.class).clone();
        foodDaoConfig.initIdentityScope(type);

        mealDaoConfig = daoConfigMap.get(MealDao.class).clone();
        mealDaoConfig.initIdentityScope(type);

        mealItemDaoConfig = daoConfigMap.get(MealItemDao.class).clone();
        mealItemDaoConfig.initIdentityScope(type);

        profileDaoConfig = daoConfigMap.get(ProfileDao.class).clone();
        profileDaoConfig.initIdentityScope(type);

        communityMessageDaoConfig = daoConfigMap.get(CommunityMessageDao.class).clone();
        communityMessageDaoConfig.initIdentityScope(type);

        diaryEntryDao = new DiaryEntryDao(diaryEntryDaoConfig, this);
        exerciseDao = new ExerciseDao(exerciseDaoConfig, this);
        foodDao = new FoodDao(foodDaoConfig, this);
        mealDao = new MealDao(mealDaoConfig, this);
        mealItemDao = new MealItemDao(mealItemDaoConfig, this);
        profileDao = new ProfileDao(profileDaoConfig, this);
        communityMessageDao = new CommunityMessageDao(communityMessageDaoConfig, this);

        registerDao(DiaryEntry.class, diaryEntryDao);
        registerDao(Exercise.class, exerciseDao);
        registerDao(Food.class, foodDao);
        registerDao(Meal.class, mealDao);
        registerDao(MealItem.class, mealItemDao);
        registerDao(Profile.class, profileDao);
        registerDao(CommunityMessage.class, communityMessageDao);
    }
    
    public void clear() {
        diaryEntryDaoConfig.getIdentityScope().clear();
        exerciseDaoConfig.getIdentityScope().clear();
        foodDaoConfig.getIdentityScope().clear();
        mealDaoConfig.getIdentityScope().clear();
        mealItemDaoConfig.getIdentityScope().clear();
        profileDaoConfig.getIdentityScope().clear();
        communityMessageDaoConfig.getIdentityScope().clear();
    }

    public DiaryEntryDao getDiaryEntryDao() {
        return diaryEntryDao;
    }

    public ExerciseDao getExerciseDao() {
        return exerciseDao;
    }

    public FoodDao getFoodDao() {
        return foodDao;
    }

    public MealDao getMealDao() {
        return mealDao;
    }

    public MealItemDao getMealItemDao() {
        return mealItemDao;
    }

    public ProfileDao getProfileDao() {
        return profileDao;
    }

    public CommunityMessageDao getCommunityMessageDao() {
        return communityMessageDao;
    }

}
