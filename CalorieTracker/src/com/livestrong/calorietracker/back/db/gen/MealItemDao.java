package com.livestrong.calorietracker.back.db.gen;

import java.util.List;
import java.util.ArrayList;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.SqlUtils;
import de.greenrobot.dao.internal.DaoConfig;
import de.greenrobot.dao.query.Query;
import de.greenrobot.dao.query.QueryBuilder;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table MEAL_ITEM.
*/
public class MealItemDao extends AbstractDao<MealItem, Long> {

    public static final String TABLENAME = "MEAL_ITEM";

    /**
     * Properties of entity MealItem.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property Servings = new Property(1, Float.class, "servings", false, "SERVINGS");
        public final static Property FoodId = new Property(2, Long.class, "foodId", false, "FOOD_ID");
        public final static Property MealId = new Property(3, Long.class, "mealId", false, "MEAL_ID");
        public final static Property MealItemId = new Property(4, Long.class, "mealItemId", false, "MEAL_ITEM_ID");
    };

    private DaoSession daoSession;

    private Query<MealItem> meal_MealItemListQuery;
    private Query<MealItem> food_MealItemListQuery;

    public MealItemDao(DaoConfig config) {
        super(config);
    }
    
    public MealItemDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
        this.daoSession = daoSession;
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "'MEAL_ITEM' (" + //
                "'_id' INTEGER PRIMARY KEY AUTOINCREMENT ," + // 0: id
                "'SERVINGS' REAL," + // 1: servings
                "'FOOD_ID' INTEGER," + // 2: foodId
                "'MEAL_ID' INTEGER," + // 3: mealId
                "'MEAL_ITEM_ID' INTEGER);"); // 4: mealItemId
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "'MEAL_ITEM'";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, MealItem entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        Float servings = entity.getServings();
        if (servings != null) {
            stmt.bindDouble(2, servings);
        }
 
        Long foodId = entity.getFoodId();
        if (foodId != null) {
            stmt.bindLong(3, foodId);
        }
 
        Long mealId = entity.getMealId();
        if (mealId != null) {
            stmt.bindLong(4, mealId);
        }
 
        Long mealItemId = entity.getMealItemId();
        if (mealItemId != null) {
            stmt.bindLong(5, mealItemId);
        }
    }

    @Override
    protected void attachEntity(MealItem entity) {
        super.attachEntity(entity);
        entity.__setDaoSession(daoSession);
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public MealItem readEntity(Cursor cursor, int offset) {
        MealItem entity = new MealItem( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getFloat(offset + 1), // servings
            cursor.isNull(offset + 2) ? null : cursor.getLong(offset + 2), // foodId
            cursor.isNull(offset + 3) ? null : cursor.getLong(offset + 3), // mealId
            cursor.isNull(offset + 4) ? null : cursor.getLong(offset + 4) // mealItemId
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, MealItem entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setServings(cursor.isNull(offset + 1) ? null : cursor.getFloat(offset + 1));
        entity.setFoodId(cursor.isNull(offset + 2) ? null : cursor.getLong(offset + 2));
        entity.setMealId(cursor.isNull(offset + 3) ? null : cursor.getLong(offset + 3));
        entity.setMealItemId(cursor.isNull(offset + 4) ? null : cursor.getLong(offset + 4));
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(MealItem entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(MealItem entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
    /** Internal query to resolve the "mealItemList" to-many relationship of Meal. */
    public List<MealItem> _queryMeal_MealItemList(Long mealItemId) {
        synchronized (this) {
            if (meal_MealItemListQuery == null) {
                QueryBuilder<MealItem> queryBuilder = queryBuilder();
                queryBuilder.where(Properties.MealItemId.eq(null));
                meal_MealItemListQuery = queryBuilder.build();
            }
        }
        Query<MealItem> query = meal_MealItemListQuery.forCurrentThread();
        query.setParameter(0, mealItemId);
        return query.list();
    }

    /** Internal query to resolve the "mealItemList" to-many relationship of Food. */
    public List<MealItem> _queryFood_MealItemList(Long mealItemId) {
        synchronized (this) {
            if (food_MealItemListQuery == null) {
                QueryBuilder<MealItem> queryBuilder = queryBuilder();
                queryBuilder.where(Properties.MealItemId.eq(null));
                food_MealItemListQuery = queryBuilder.build();
            }
        }
        Query<MealItem> query = food_MealItemListQuery.forCurrentThread();
        query.setParameter(0, mealItemId);
        return query.list();
    }

    private String selectDeep;

    protected String getSelectDeep() {
        if (selectDeep == null) {
            StringBuilder builder = new StringBuilder("SELECT ");
            SqlUtils.appendColumns(builder, "T", getAllColumns());
            builder.append(',');
            SqlUtils.appendColumns(builder, "T0", daoSession.getFoodDao().getAllColumns());
            builder.append(',');
            SqlUtils.appendColumns(builder, "T1", daoSession.getMealDao().getAllColumns());
            builder.append(" FROM MEAL_ITEM T");
            builder.append(" LEFT JOIN FOOD T0 ON T.'FOOD_ID'=T0.'_id'");
            builder.append(" LEFT JOIN MEAL T1 ON T.'MEAL_ID'=T1.'_id'");
            builder.append(' ');
            selectDeep = builder.toString();
        }
        return selectDeep;
    }
    
    protected MealItem loadCurrentDeep(Cursor cursor, boolean lock) {
        MealItem entity = loadCurrent(cursor, 0, lock);
        int offset = getAllColumns().length;

        Food food = loadCurrentOther(daoSession.getFoodDao(), cursor, offset);
        entity.setFood(food);
        offset += daoSession.getFoodDao().getAllColumns().length;

        Meal meal = loadCurrentOther(daoSession.getMealDao(), cursor, offset);
        entity.setMeal(meal);

        return entity;    
    }

    public MealItem loadDeep(Long key) {
        assertSinglePk();
        if (key == null) {
            return null;
        }

        StringBuilder builder = new StringBuilder(getSelectDeep());
        builder.append("WHERE ");
        SqlUtils.appendColumnsEqValue(builder, "T", getPkColumns());
        String sql = builder.toString();
        
        String[] keyArray = new String[] { key.toString() };
        Cursor cursor = db.rawQuery(sql, keyArray);
        
        try {
            boolean available = cursor.moveToFirst();
            if (!available) {
                return null;
            } else if (!cursor.isLast()) {
                throw new IllegalStateException("Expected unique result, but count was " + cursor.getCount());
            }
            return loadCurrentDeep(cursor, true);
        } finally {
            cursor.close();
        }
    }
    
    /** Reads all available rows from the given cursor and returns a list of new ImageTO objects. */
    public List<MealItem> loadAllDeepFromCursor(Cursor cursor) {
        int count = cursor.getCount();
        List<MealItem> list = new ArrayList<MealItem>(count);
        
        if (cursor.moveToFirst()) {
            if (identityScope != null) {
                identityScope.lock();
                identityScope.reserveRoom(count);
            }
            try {
                do {
                    list.add(loadCurrentDeep(cursor, false));
                } while (cursor.moveToNext());
            } finally {
                if (identityScope != null) {
                    identityScope.unlock();
                }
            }
        }
        return list;
    }
    
    protected List<MealItem> loadDeepAllAndCloseCursor(Cursor cursor) {
        try {
            return loadAllDeepFromCursor(cursor);
        } finally {
            cursor.close();
        }
    }
    

    /** A raw-style query where you can pass any WHERE clause and arguments. */
    public List<MealItem> queryDeep(String where, String... selectionArg) {
        Cursor cursor = db.rawQuery(getSelectDeep() + where, selectionArg);
        return loadDeepAllAndCloseCursor(cursor);
    }
 
}
