package com.livestrong.calorietracker.back.db.gen;

import java.util.List;

import de.greenrobot.dao.DaoException;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
import com.livestrong.calorietracker.back.db.LiveStrongDisplayableListItem;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import android.util.Log;

import org.json.JSONObject;


import com.livestrong.calorietracker.utilities.JSONSafeObject;
// KEEP INCLUDES END
/**
 * Entity mapped to table MEAL.
 */
public class Meal implements GenericModelObject, LiveStrongDisplayableListItem, Serializable {

    private Long id;
    private Integer calories;
    private java.util.Date dateCreated;
    private java.util.Date dateDeleted;
    private java.util.Date dateModified;
    private String name;
    private String remoteId;

    /** Used to resolve relations */
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    private transient MealDao myDao;

    private List<MealItem> mealItemList;

    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    public Meal() {
    }

    public Meal(Long id) {
        this.id = id;
    }

    public Meal(Long id, Integer calories, java.util.Date dateCreated, java.util.Date dateDeleted, java.util.Date dateModified, String name, String remoteId) {
        this.id = id;
        this.calories = calories;
        this.dateCreated = dateCreated;
        this.dateDeleted = dateDeleted;
        this.dateModified = dateModified;
        this.name = name;
        this.remoteId = remoteId;
    }

    /** called by internal mechanisms, do not call yourself. */
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getMealDao() : null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCalories() {
        return calories;
    }

    public void setCalories(Integer calories) {
        this.calories = calories;
    }

    public java.util.Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(java.util.Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public java.util.Date getDateDeleted() {
        return dateDeleted;
    }

    public void setDateDeleted(java.util.Date dateDeleted) {
        this.dateDeleted = dateDeleted;
    }

    public java.util.Date getDateModified() {
        return dateModified;
    }

    public void setDateModified(java.util.Date dateModified) {
        this.dateModified = dateModified;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemoteId() {
        return remoteId;
    }

    public void setRemoteId(String remoteId) {
        this.remoteId = remoteId;
    }

    /** To-many relationship, resolved on first access (and after reset). Changes to to-many relations are not persisted, make changes to the target entity. */
    public List<MealItem> getMealItemList() {
        if (mealItemList == null) {
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            MealItemDao targetDao = daoSession.getMealItemDao();
            List<MealItem> mealItemListNew = targetDao._queryMeal_MealItemList(id);
            synchronized (this) {
                if(mealItemList == null) {
                    mealItemList = mealItemListNew;
                }
            }
        }
        return mealItemList;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    public synchronized void resetMealItemList() {
        mealItemList = null;
    }

    /** Convenient call for {@link AbstractDao#delete(Object)}. Entity must attached to an entity context. */
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.delete(this);
    }

    /** Convenient call for {@link AbstractDao#update(Object)}. Entity must attached to an entity context. */
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.update(this);
    }

    /** Convenient call for {@link AbstractDao#refresh(Object)}. Entity must attached to an entity context. */
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.refresh(this);
    }

    // KEEP METHODS - put your custom methods here
    public static Meal getMeal(String mealId, MealDao context){
        return context.queryBuilder().where(MealDao.Properties.RemoteId.eq(mealId)).list().get(0);
    }

    public void updateMealWithData(JSONObject mealObject){
    	this.remoteId =  JSONSafeObject.safeGetString("meal_id", mealObject);
    	this.name = JSONSafeObject.safeGetString("meal_name",mealObject);
    	this.calories = JSONSafeObject.safeGetInt("cals",mealObject);
    	this.dateCreated = new Date();
    	this.dateModified= new Date();

    }

    public static List<Meal> getMeals(MealDao dao) {
        return dao.queryBuilder().limit(50).list();
    }

    @Override
    public String getTitle() {
        return this.getName();
    }

    @Override
    public String getDescription() {
        return Math.round(getCalories()) + " calories";
    }

    @Override
    public String getSmallImage() {
        return null;
    }

    public static byte[] serializeObject(Object o) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {
            ObjectOutput out = new ObjectOutputStream(bos);
            out.writeObject(o);
            out.close();

            // Get the bytes of the serialized object
            byte[] buf = bos.toByteArray();

            return buf;
        } catch(IOException ioe) {
            Log.e("serializeObject", "error", ioe);

            return null;
        }
    }
    public static Object deserializeObject(byte[] b) {
        try {
            ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(b));
            Object object = in.readObject();
            in.close();

            return object;
        } catch(ClassNotFoundException cnfe) {
            Log.e("deserializeObject", "class not found error", cnfe);

            return null;
        } catch(IOException ioe) {
            Log.e("deserializeObject", "io error", ioe);

            return null;
        }
    }
    // KEEP METHODS END

}
