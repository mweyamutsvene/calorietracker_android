package com.livestrong.calorietracker.back.db;

public interface LiveStrongDisplayableListItem {

	public String getTitle();
	public String getDescription();
	public String getSmallImage();
}
