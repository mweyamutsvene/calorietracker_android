package com.livestrong.calorietracker.back.db;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.*;

import com.livestrong.calorietracker.MyPlateApplication;
import com.livestrong.calorietracker.back.api.CTWebserviceRequest;
import com.livestrong.calorietracker.back.api.CTWebserviceRequest.CTWebserviceRequestDelegate;

import com.livestrong.calorietracker.back.db.gen.DiaryEntry;
import com.livestrong.calorietracker.back.db.gen.DiaryEntry.*;

import com.livestrong.calorietracker.back.db.gen.ExerciseDao;
import com.livestrong.calorietracker.back.db.gen.Food;
import com.livestrong.calorietracker.configuration.MyPlateDefaults;
import com.livestrong.calorietracker.utilities.JSONSafeObject;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;


import com.livestrong.calorietracker.back.db.gen.Food.SyncFoodTask;
import com.livestrong.calorietracker.back.db.sync.ExerciseDiarySyncTask;
import com.livestrong.calorietracker.back.db.sync.FoodDiarySyncTask;
import com.livestrong.calorietracker.back.db.sync.MealSyncTask;
import com.livestrong.calorietracker.back.db.sync.ModelSyncCompletedDelegate;
import com.livestrong.calorietracker.back.db.sync.SyncTask;
import com.livestrong.calorietracker.back.db.sync.WaterDiarySyncTask;
import com.livestrong.calorietracker.back.db.sync.WeightDiarySyncTask;
import com.livestrong.calorietracker.back.db.sync.SyncTask.SyncDelegate;
import com.livestrong.calorietracker.utilities.SimpleDate;
import com.livestrong.calorietracker.back.db.gen.DaoMaster;
import com.livestrong.calorietracker.back.db.gen.DaoSession;
import com.livestrong.calorietracker.back.db.gen.DiaryEntryDao;
import com.livestrong.calorietracker.back.db.gen.Exercise;
import com.livestrong.calorietracker.back.db.gen.FoodDao;
import com.livestrong.calorietracker.back.db.gen.Meal;
import com.livestrong.calorietracker.back.db.gen.MealDao;
import com.livestrong.calorietracker.back.db.gen.Profile;
import com.livestrong.calorietracker.back.db.gen.ProfileDao;

/**
 * Model Manager will handle all database interactions between 
 * activities - model, and model - server
 * 
 * 
 * @author Tommy
 *
 */
public class ModelManager implements CTWebserviceRequestDelegate, ModelSyncCompletedDelegate, SyncDelegate {
	public interface ModelManagerDelegate {

		public void syncCompleted(ModelManager modelManager, Boolean success);
		public void syncPercentCompleted(ModelManager modelManager, float percentComplete);

	}
    public interface ModelManagerFoodSearchDelegate {

        public void searchComplete(ModelManager modelManager, Boolean success, List<Food>results);

    }
    public static Method METHOD_GET_MEALS;
    public static Method METHOD_SEARCH_FOODS;
    public static Method METHOD_GET_RECENT_FOODS;
    public static Method METHOD_GET_FAVORITE_FOODS;
    public static Method METHOD_GET_CUSTOM_FOODS;

    public static Method METHOD_SEARCH_EXERCISES;
    public static Method METHOD_GET_RECENT_EXERCISES;
    public static Method METHOD_GET_FAVORITE_EXERCISES;
    public static Method METHOD_GET_CUSTOM_EXERCISES;

    public static Method METHOD_GET_DAILY_DIARY_ENTRIES_FOR_DATES;
    public static Method METHOD_GET_DAILY_DIARY_ENTRIES_FOR_DAY;
    public static Method METHOD_GET_TODAY_NUTRIENTS;

    static {
        try {
            METHOD_GET_CUSTOM_FOODS = ModelManager.class.getMethod("getCustomFoods");
            METHOD_GET_CUSTOM_EXERCISES = ModelManager.class.getMethod("getCustomExercises");
            METHOD_GET_MEALS = ModelManager.class.getMethod("getMeals");
            METHOD_SEARCH_FOODS = ModelManager.class.getMethod("getFoodsLike", String.class);
            METHOD_GET_RECENT_FOODS = ModelManager.class.getMethod("getRecentFoods", DiaryEntryCategoryEnum.class);
            METHOD_GET_FAVORITE_FOODS = ModelManager.class.getMethod("getFavoriteFoods", DiaryEntryCategoryEnum.class);
            METHOD_SEARCH_EXERCISES = ModelManager.class.getMethod("getExercisesLike", String.class);
            METHOD_GET_RECENT_EXERCISES = ModelManager.class.getMethod("getRecentExercises", DiaryEntryCategoryEnum.class);
            METHOD_GET_FAVORITE_EXERCISES = ModelManager.class.getMethod("getFavoriteExercises", DiaryEntryCategoryEnum.class);
            METHOD_GET_DAILY_DIARY_ENTRIES_FOR_DAY = ModelManager.class.getMethod("getDiaryEntriesForDay", Date.class);
            METHOD_GET_DAILY_DIARY_ENTRIES_FOR_DATES = ModelManager.class.getMethod("getDiaryEntriesForDates", Date.class, Date.class);
            METHOD_GET_TODAY_NUTRIENTS = ModelManager.class.getMethod("getTodayNutrients");
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }


	public static ModelManagerDelegate delegate = null;
    public static ModelManagerFoodSearchDelegate foodSearchDelegate = null;
	private static ModelManager instance = null;
	private static boolean initialized = false;
	private static Context context = null;
	private static DaoMaster daoMaster = null;
	public static DaoSession daoSession = null;
	private static DaoMaster.OpenHelper helper = null;
	private JSONArray meals = null;
	private JSONArray exercises = null;
	private JSONArray weighIns = null;
	private JSONArray foods = null;
	private JSONArray waters = null;
    private Boolean profileFinishedSync = false;

    private Boolean isSyncing = false;
	protected ModelManager() {
		// Exists only to defeat instantiation.
	}

	/**
	 * Creates an instance of the Model Manager class
	 * 
	 * @return
	 */
	public static ModelManager sharedModelManager() {
		if(instance == null) {
			instance = new ModelManager();
		}
		return instance;
	}

    public static void resetModelManager(){
        ModelManager.initialized = false;
    }

    public static void resetDatabase(){
        DaoMaster.dropAllTables(daoMaster.getDatabase(),true);
        initialized = false;
        initializeModelManager(context);
        DaoMaster.createAllTables(daoMaster.getDatabase(),true);
    }
	/**
	 * Initializes the model manager class with a context to create
	 * the dao master, session, and db
	 * 
	 * @param aContext
	 */
	public static void initializeModelManager(Context aContext){
		if(ModelManager.initialized == true){
			return;
		}
		context = aContext;
		initialized = true;
		helper = new DaoMaster.OpenHelper(context, "calorietracker-db", null) {
			@Override
			public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
				// TODO Auto-generated method stub
			}
		};

		SQLiteDatabase db = helper.getWritableDatabase(); 

		// Construct the DaoMaster which brokers DAOs for the Domain Objects
		daoMaster = new DaoMaster(db); 

		// Create the session which is a container for the DAO layer and has a cache which will return handles to the same object across multiple queries
		daoSession = daoMaster.newSession(); 

	}


	private void syncDiary(){
        //Get the last sync Date if it exists
        SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'+00:00'");
        dateTimeFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));

        long lastOnloadSync = MyPlateDefaults.getPref(MyPlateDefaults.PREFS_LAST_SYNC, (long) 0);
        String lastSyncString = "";
        if(lastOnloadSync != 0){
            Date date = new Date(lastOnloadSync);
            lastSyncString = dateTimeFormatter.format(date);
        }

        //Get diary and send to server
        DiaryEntryDao dao = daoSession.getDiaryEntryDao();
        List<DiaryEntry> dirtyDiaries = DiaryEntry.dirtyDiaryEntries(dao);
        if(dirtyDiaries.size() == 0){
            lastSyncString = "";
        }
        JSONObject object = DiaryEntry.serializeDiaryToJSON(dao);


        CTWebserviceRequest.postDiary(object, lastSyncString).delegate = this;

    }
	
	private void syncProfile(){
        profileFinishedSync = false;
        Profile userProfile = this.getUserProfile();
        if(userProfile == null || userProfile.getIsSynchronized() == true){
            CTWebserviceRequest.getProfile().delegate = this;
        }
        else{
            CTWebserviceRequest.postProfile(userProfile.serializeProfileToJSON()).delegate = this;
        }
    }
	private void updateDelegateSyncInfo(){
        Integer foodDiaryDone = (foods != null)?0:1;
        Integer weightDiaryDone = (weighIns != null)?0:1;
        Integer waterDiaryDone = (waters != null)?0:1;
        Integer exerciseDiaryDone = (exercises != null)?0:1;

        Integer numberOfItemsCompleted = foodDiaryDone+weightDiaryDone+waterDiaryDone+exerciseDiaryDone;
        if(delegate != null){
            delegate.syncPercentCompleted(this,(float)numberOfItemsCompleted/4);
        }
        if(numberOfItemsCompleted == 4 && profileFinishedSync){
            if(delegate != null){
             delegate.syncCompleted(this,true);
            }

            //Inform all activities that are registered for this event to know
            //Sync has completed
            isSyncing = false;
            Intent intent = new Intent("syncComplete");
            LocalBroadcastManager.getInstance(MyPlateApplication.getContext()).sendBroadcast(intent);
        }
    }
	public void synchronizeDataWithServer(){

        if(isSyncing){
         return;
        }
        isSyncing = true;
		syncDiary();

	}

    public void saveUserProfile(Profile entity){
        ProfileDao profileDao = daoSession.getProfileDao();
        profileDao.deleteAll();
        profileDao.insert(entity);

    }
    public Profile getUserProfile(){
        ProfileDao profileDao = daoSession.getProfileDao();
        try{
            List profiles = profileDao.loadAll();
            if(profiles.size() > 0){
                Profile profile = (Profile)profiles.get(0);
                return profile;
            }
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }

        return null;

    }
    public void updateWeightWithLatestEntry(){
        List<DiaryEntry> frequentDiaryEntries = DiaryEntry.recentDiaryEntries(daoSession.getDiaryEntryDao(), DiaryEntry.DiaryEntryCategoryEnum.CTDiaryEntryTypeWeighIn);
        if(frequentDiaryEntries.size() < 1){
            return;
        }

        //Get Weight from last weighin
        DiaryEntry lastWeighin = frequentDiaryEntries.get(0);

        //Save weight to user so we dont have to keep pulling the latest diary
        float weight = lastWeighin.getWeight();
        Profile user =  this.getUserProfile();
        if(user == null){
            return;
        }
        user.setWeight(weight);

        //update user profile
        ProfileDao profileDao = daoSession.getProfileDao();
        profileDao.update(user);


    }
    public static DiaryEntry getWaterEntryForDate(Date day){
        List<DiaryEntry> dailyDiary = DiaryEntry.loadEntriesForDay(daoSession.getDiaryEntryDao(),day);
        for (DiaryEntry entry : dailyDiary){
            if(DiaryEntry.DiaryEntryCategoryEnum.values()[entry.getCategory()].equals(DiaryEntry.DiaryEntryCategoryEnum.CTDiaryEntryTypeWater)){
                return entry;
            }
        }
        return null;
    }
    public static DiaryEntry getWeightEntryForDate(Date day){
        List<DiaryEntry> dailyDiary = DiaryEntry.loadEntriesForDay(daoSession.getDiaryEntryDao(),day);
        for (DiaryEntry entry : dailyDiary){
            if(DiaryEntry.DiaryEntryCategoryEnum.values()[entry.getCategory()].equals(DiaryEntry.DiaryEntryCategoryEnum.CTDiaryEntryTypeWeighIn)){
                return entry;
            }
        }
        return null;
    }
    /**
     * Load the Diary Entries from the local DB.
     *
     * @param day The day the entries were entered on.
     * @param delegate (Optional) To make this call asynchronous, send an object that implements DataHelperDelegate.
     * @return List<DiaryEntry>
     */
    public static  List<DiaryEntry> getDiaryEntriesForDay(Date day, DataHelperDelegate delegate) {
        if (delegate == null) {
            return getDiaryEntriesForDay(day);
        }

        AsyncDataHelper asyncTask = new AsyncDataHelper(delegate, ModelManager.METHOD_GET_DAILY_DIARY_ENTRIES_FOR_DAY);
        asyncTask.execute(day);
        return null;

    }
    public static List<DiaryEntry> getDiaryEntriesForDay(Date day){
        return  DiaryEntry.loadEntriesForDay(daoSession.getDiaryEntryDao(),day);
    }

    /**
     * Load the Diary Entries from the local DB.
     *
     * @param startDate The first day the entries were entered on.
     * @param endDate The last day the entries were entered on.
     * @param delegate (Optional) To make this call asynchronous, send an object that implements DataHelperDelegate.
     * @return List<DiaryEntry>
     */
    public static  Map<SimpleDate,List<DiaryEntry>> getDiaryEntriesForDates(Date startDate, Date endDate, DataHelperDelegate delegate) {
        if (delegate == null) {
            return getDiaryEntriesForDates(startDate, endDate);
        }

        AsyncDataHelper asyncTask = new AsyncDataHelper(delegate, ModelManager.METHOD_GET_DAILY_DIARY_ENTRIES_FOR_DATES);
        asyncTask.execute(startDate,endDate);
        return null;

    }
    public static Map<SimpleDate,List<DiaryEntry>> getDiaryEntriesForDates(Date startDate, Date endDate){
        return  DiaryEntry.loadAllEntriesForDates(daoSession.getDiaryEntryDao(),startDate,endDate);
    }

    /**
     * Load today's nutrients (fat, carbs, protein).
     *
     * @param delegate (Optional) To make this call asynchronous, send an object that implements DataHelperDelegate.
     * @return Map<String, Double>
     * @example Map<String, Double> todayNutrients = DataHelper.getTodayNutrients(null);
     */
    @SuppressWarnings("unchecked")
    public static Map<String, Double> getTodayNutrients(DataHelperDelegate delegate) {
        if (delegate == null) {
            return getTodayNutrients();
        }

        AsyncDataHelper asyncTask = new AsyncDataHelper(delegate, ModelManager.METHOD_GET_TODAY_NUTRIENTS);
        asyncTask.execute();
        return null;
    }


    public static Map<String, Double> getTodayNutrients() {
        List<DiaryEntry> diaryEntries = ModelManager.getDiaryEntriesForDay(new Date());
        return DiaryEntry.getFatCarbsProtein(diaryEntries);
    }

    public static Map<DiaryEntry.DiaryEntryCategoryEnum, Float> getDiarySummaryPerType(Date day) {
        List<DiaryEntry> dailyDiary = DiaryEntry.loadEntriesForDay(daoSession.getDiaryEntryDao(),day);
        return DiaryEntry.getDiarySummaryPerType(dailyDiary);

    }

    /**
     * Load the recently used Food from the local DB.
     *
     * @param category The TimeOfDay for which you want the recently used food.
     * @param delegate (Optional) To make this call asynchronous, send an object that implements DataHelperDelegate.
     * @return List<Food>
     * @example List<Food> foods = DataHelper.getRecentFoods(TimeOfDay.BREAKFAST, null);
     */
    public static List<Food> getRecentFoods(DiaryEntryCategoryEnum category, DataHelperDelegate delegate) {
        if (delegate == null) {
            return getRecentFoods(category);
        }

        AsyncDataHelper asyncTask = new AsyncDataHelper(delegate, ModelManager.METHOD_GET_RECENT_FOODS);
        asyncTask.execute(category);
        return null;

    }
    public static List<Food> getRecentFoods(DiaryEntryCategoryEnum category){
        List<DiaryEntry> frequentDiaryEntries = DiaryEntry.recentDiaryEntries(daoSession.getDiaryEntryDao(),category);
        TreeSet<String> foodIds = new TreeSet<String>();
        List<Food> foods = new ArrayList<Food>();
        for (DiaryEntry diaryEntry : frequentDiaryEntries) {
            Log.d("Entry Date","Entry Date:"+diaryEntry.getDateCreated().toString());
            Food food = diaryEntry.getFood();
            if (food == null/* || food.isGeneric()*/ || foodIds.contains(food.getRemoteId())) {
                // Generic calorie coming from the API
                continue;
            }
            foodIds.add(food.getRemoteId());
            foods.add(food);
        }
        return foods;
    }

    /**
     * Load favorite Foods information from either from the local DB or the remote server.
     *
     * @param category The TimeOfDay for which you want the favorite food.
     * @param delegate (Optional) To make this call asynchronous, send an object that implements DataHelperDelegate.
     * @return List<Food>
     * @example List<Food> food = DataHelper.getFavoriteFoods(TimeOfDay.BREAKFAST, null);
     */
    public static List<Food> getFavoriteFoods(DiaryEntryCategoryEnum category, DataHelperDelegate delegate) {
        if (delegate == null) {
            return getFavoriteFoods(category);
        }

        AsyncDataHelper asyncTask = new AsyncDataHelper(delegate, ModelManager.METHOD_GET_FAVORITE_FOODS);
        asyncTask.execute(category);
        return null;

    }

    public static List<Food> getFavoriteFoods(DiaryEntryCategoryEnum category) {
        List<DiaryEntry> frequentDiaryEntries = DiaryEntry.favoriteDiaryEntries(daoSession.getDiaryEntryDao(), category);
        TreeSet<String> foodIds = new TreeSet<String>();
        ArrayList<Food> foods = new ArrayList<Food>();
        for (DiaryEntry diaryEntry : frequentDiaryEntries) {
            Food food = diaryEntry.getFood();
            if (food == null/* || food.isGeneric()*/ || foodIds.contains(food.getRemoteId())) {
                // Generic calorie coming from the API
                continue;
            }
            foodIds.add(food.getRemoteId());
            foods.add(food);
        }
        return foods;

    }

    /**
     * Search the local database to find Food objects.
     * @param query: the string to search for
     * @param delegate (Optional) To make this call asynchronous, send an object that implements DataHelperDelegate.
     * @return FoodSearchResponse
     * @example FoodSearchResponse foodSearchResult = DataHelper.searchFoods("sandwich", isAutoComplete, null);
     */
    public static List<Food> getFoodsLike(String query, DataHelperDelegate delegate) {
        if (delegate == null) {
            return getFoodsLike(query);
        }

        AsyncDataHelper asyncTask = new AsyncDataHelper(delegate, ModelManager.METHOD_SEARCH_FOODS);
        asyncTask.execute(query);
        return null;

    }

    public static List<Food> getFoodsLike(String searchString){
        FoodDao dao = daoSession.getFoodDao();
        return Food.getFoodsLike(dao, searchString);
    }

    /**
     * Search the Remote database to find Food objects.
     * @param query: the string to search for
     * @param foodSearchDelegate: send an object that implements DataHelperDelegate.
     * @return null
     */
    public void getFoodsLikeFromServer(ModelSyncCompletedDelegate foodSearchDelegate, String query){
        CTWebserviceRequest request =  CTWebserviceRequest.getSearchFoods(query);
        request.storedObject = query;
        request.delegate = this;
        request.modelDelegate = foodSearchDelegate;

    }


    /**
     * Load Meal information from the local DB.
     *
     * @param delegate (Optional) To make this call asynchronous, send an object that implements DataHelperDelegate.
     * @return List<Meals>
     */
    public static List<Meal> getMeals(DataHelperDelegate delegate) {
        if (delegate == null) {
            return getMeals();
        }
        AsyncDataHelper asyncTask = new AsyncDataHelper(delegate, ModelManager.METHOD_GET_MEALS);
        asyncTask.execute();
        return null;
    }

    public static List<Meal> getMeals() {
        MealDao dao = daoSession.getMealDao();
        return Meal.getMeals(dao);
    }

    /**
     * Load Custom Foods information from either from the local DB or the remote server.
     *
     * @param delegate (Optional) To make this call asynchronous, send an object that implements DataHelperDelegate.
     * @return List<Food>
     */
    public static List<Food> getCustomFoods(DataHelperDelegate delegate) {
        if (delegate == null) {
            return getCustomFoods();
        }
        AsyncDataHelper asyncTask = new AsyncDataHelper(delegate, ModelManager.METHOD_GET_CUSTOM_FOODS);
        asyncTask.execute();
        return null;
    }

    public static List<Food> getCustomFoods() {
        FoodDao dao = daoSession.getFoodDao();
        return Food.getCustomFoods(dao);
    }

    /**
     * Load the recently used Exercises from the local DB.
     *
     * @param category The TimeOfDay for which you want the recently used food.
     * @param delegate (Optional) To make this call asynchronous, send an object that implements DataHelperDelegate.
     * @return List<Food>
     * @example List<Food> foods = DataHelper.getRecentFoods(TimeOfDay.BREAKFAST, null);
     */
    public static List<Exercise> getRecentExercises(DiaryEntryCategoryEnum category, DataHelperDelegate delegate) {
        if (delegate == null) {
            return getRecentExercises(category);
        }

        AsyncDataHelper asyncTask = new AsyncDataHelper(delegate, ModelManager.METHOD_GET_RECENT_EXERCISES);
        asyncTask.execute(category);
        return null;

    }

    public static List<Exercise> getRecentExercises(DiaryEntry.DiaryEntryCategoryEnum category) {
        List<DiaryEntry> frequentDiaryEntries = DiaryEntry.recentDiaryEntries(daoSession.getDiaryEntryDao(),category);
        TreeSet<String> exerciseIds = new TreeSet<String>();
        ArrayList<Exercise> exercises = new ArrayList<Exercise>();
        for (DiaryEntry diaryEntry : frequentDiaryEntries) {
            Exercise exercise = diaryEntry.getExercise();
            if (exercise == null/* || food.isGeneric()*/ || exerciseIds.contains(exercise.getRemoteId())) {
                // Generic calorie coming from the API
                continue;
            }
            exerciseIds.add(exercise.getRemoteId());
            exercises.add(exercise);
        }
        return exercises;

    }
    /**
     * Load the Favorite used Exercises from the local DB.
     *
     * @param category The TimeOfDay for which you want the recently used food.
     * @param delegate (Optional) To make this call asynchronous, send an object that implements DataHelperDelegate.
     * @return List<Food>
     * @example List<Food> foods = DataHelper.getRecentFoods(TimeOfDay.BREAKFAST, null);
     */
    public static List<Exercise> getFavoriteExercises(DiaryEntryCategoryEnum category, DataHelperDelegate delegate) {
        if (delegate == null) {
            return getFavoriteExercises(category);
        }

        AsyncDataHelper asyncTask = new AsyncDataHelper(delegate, ModelManager.METHOD_GET_FAVORITE_EXERCISES);
        asyncTask.execute(category);
        return null;

    }

    public static List<Exercise> getFavoriteExercises(DiaryEntry.DiaryEntryCategoryEnum category) {
        List<DiaryEntry> frequentDiaryEntries = DiaryEntry.favoriteDiaryEntries(daoSession.getDiaryEntryDao(), category);
        TreeSet<String> exerciseIds = new TreeSet<String>();
        ArrayList<Exercise> exercises = new ArrayList<Exercise>();
        for (DiaryEntry diaryEntry : frequentDiaryEntries) {
            Exercise exercise = diaryEntry.getExercise();
            if (exercise == null/* || food.isGeneric()*/ || exerciseIds.contains(exercise.getRemoteId())) {
                // Generic calorie coming from the API
                continue;
            }
            exerciseIds.add(exercise.getRemoteId());
            exercises.add(exercise);
        }
        return exercises;

    }

    /**
     * Search the local database to find Food objects.
     * @param query: the string to search for
     * @param delegate (Optional) To make this call asynchronous, send an object that implements DataHelperDelegate.
     * @return FoodSearchResponse
     * @example FoodSearchResponse foodSearchResult = DataHelper.searchFoods("sandwich", isAutoComplete, null);
     */
    public static List<Exercise> getExercisesLike(String query, DataHelperDelegate delegate) {
        if (delegate == null) {
            return getExercisesLike(query);
        }

        AsyncDataHelper asyncTask = new AsyncDataHelper(delegate, ModelManager.METHOD_SEARCH_EXERCISES);
        asyncTask.execute(query);
        return null;

    }
    public static List<Exercise> getExercisesLike(String searchString){
        ExerciseDao dao = daoSession.getExerciseDao();
        return Exercise.getExercisesLike(dao, searchString);
    }

    /**
     * Search the Remote database to find Exercise objects.
     * @param exerciseSearchDelegate: send an object that implements DataHelperDelegate.
     * @param query: the string to search for
     * @return null
     */
    public void getExercisesLikeFromServer(String query,ModelSyncCompletedDelegate exerciseSearchDelegate){
        CTWebserviceRequest request =  CTWebserviceRequest.getSearchExercises(query);
        request.storedObject = query;
        request.delegate = this;
        request.modelDelegate = exerciseSearchDelegate;
    }

    /**
     * Load Custom Exercise information from either from the local DB or the remote server.
     *
     * @param delegate (Optional) To make this call asynchronous, send an object that implements DataHelperDelegate.
     * @return List<Food>
     */
    public static List<Exercise> getCustomExercises(DataHelperDelegate delegate) {
        if (delegate == null) {
            return getCustomExercises();
        }
        AsyncDataHelper asyncTask = new AsyncDataHelper(delegate, ModelManager.METHOD_GET_CUSTOM_EXERCISES);
        asyncTask.execute();
        return null;
    }

    public static List<Exercise> getCustomExercises() {
        ExerciseDao dao = daoSession.getExerciseDao();
        return Exercise.getCustomExercise(dao);
    }



    public void insertDiaryEntry(DiaryEntry entity){
        DiaryEntryDao diaryEntryDao = daoSession.getDiaryEntryDao();
        diaryEntryDao.insert(entity);

    }
    public void deleteDiaryEntry(DiaryEntry entity){
        entity.setIsSynchronized(false);
        entity.setDateDeleted(new Date());

        DiaryEntryDao diaryEntryDao = daoSession.getDiaryEntryDao();
        diaryEntryDao.update(entity);

    }
    public void updateDiaryEntry(DiaryEntry entity){
        DiaryEntryDao diaryEntryDao = daoSession.getDiaryEntryDao();
        diaryEntryDao.update(entity);

    }


    /**
     * Helper method for generating dates into a sorted set.
     *
     * @param fromDate Start Date.
     * @param toDate End Date.
     * @return SortedSet<SimpleDate>
     */
    public static SortedSet<SimpleDate> getAllDates(Date fromDate, Date toDate) {
        SortedSet<SimpleDate> allDates = new TreeSet<SimpleDate>();

        Date day = new SimpleDate(fromDate);
        Calendar nextDay = Calendar.getInstance();

        while (day.before(toDate)) {
            allDates.add(new SimpleDate(day));

            nextDay.setTime(day);
            nextDay.add(Calendar.DATE, 1);
            day = nextDay.getTime();
        }

        return allDates;
    }

    public static List<String> loadSearchForFood(String searchString) {
        FoodDao dao = daoSession.getFoodDao();
        List<String> foodnames = new ArrayList<String>();
       List<Food> likeFoods =  Food.getFoodsLike(dao, searchString);
        for (Food likeFood : likeFoods){
            foodnames.add(likeFood.getName());
        }
        return foodnames;

    }

    public static List<String> loadSearchForExercise(String searchString) {
        ExerciseDao dao = daoSession.getExerciseDao();
        List<String> exerciseNames = new ArrayList<String>();
        List<Exercise> likeExercises =  Exercise.getExercisesLike(dao, searchString);
        for (Exercise likeExercise : likeExercises){
            exerciseNames.add(likeExercise.getName());
        }
        return exerciseNames;

    }


    public static List<String> consumeLocalFoodNames(String query) {
        List<String> foodNames = new ArrayList<String>();
        List<String> possibleNames = new ArrayList<String>();

        BufferedReader in = null;
        try {
            in = new BufferedReader(
                    new InputStreamReader(context.getAssets().open("foodNames.txt"), "UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            for (;;) {
                String line = in.readLine();
                if (line == null)
                    break;

                if(line.toLowerCase().startsWith(query.toLowerCase())){
                    foodNames.add(line);
                }
                else  if(line.toLowerCase().contains(query.toLowerCase())){
                    possibleNames.add(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        foodNames.addAll(possibleNames);
        return foodNames;
    }

    public static void refreshFoodData(String guid, ModelManagerDelegate delegate){
        CTWebserviceRequest.getFood(guid).delegate = ModelManager.sharedModelManager();
        ModelManager.delegate = delegate;
    }
    public static Map<SimpleDate, Integer> getDailyCaloriesSum(Date fromDate, Date toDate) {
        Map<SimpleDate, Integer> dailySummary = new LinkedHashMap<SimpleDate, Integer>();

        fromDate = new SimpleDate(fromDate);
        toDate = new SimpleDate(toDate);

        SortedSet<SimpleDate> allDates = getAllDates(fromDate, toDate);
        Map<SimpleDate,List<DiaryEntry>> monthlyEntries = DiaryEntry.loadAllEntriesForDates(daoSession.getDiaryEntryDao(),fromDate,toDate);


        for (SimpleDate day : allDates) {
            List<DiaryEntry> entriesForDay = monthlyEntries.get(day);
            if(entriesForDay == null || entriesForDay.size() == 0){
                dailySummary.put(day, 0);
                continue;
            }

            Map<DiaryEntry.DiaryEntryCategoryEnum, Float>diaryEntry = DiaryEntry.getDiarySummaryPerType(entriesForDay);
            Float breakfastCalories = diaryEntry.get(DiaryEntry.DiaryEntryCategoryEnum.CTDiaryEntryTypeBreakfast);
            Float lunchCalories = diaryEntry.get(DiaryEntry.DiaryEntryCategoryEnum.CTDiaryEntryTypeLunch);
            Float dinnerCalories = diaryEntry.get(DiaryEntry.DiaryEntryCategoryEnum.CTDiaryEntryTypeDinner);
            Float snacksCalories = diaryEntry.get(DiaryEntry.DiaryEntryCategoryEnum.CTDiaryEntryTypeSnacks);
            Float exerciseCalories = diaryEntry.get(DiaryEntry.DiaryEntryCategoryEnum.CTDiaryEntryTypeExercise);

            dailySummary.put(day, (int)(breakfastCalories + lunchCalories + dinnerCalories + snacksCalories + exerciseCalories));

        }

        return dailySummary;
    }

    public static Map<SimpleDate, Integer> getDailyCaloriesGoals(Date fromDate, Date toDate) {

        fromDate = new SimpleDate(fromDate);
        toDate = new SimpleDate(toDate);

        Map<SimpleDate, Integer> dailySummary = new LinkedHashMap<SimpleDate, Integer>();

        Profile userProfile = ModelManager.sharedModelManager().getUserProfile();

        SortedSet<SimpleDate> allDates = getAllDates(fromDate, toDate);
        for (SimpleDate day : allDates) {
            dailySummary.put(day, 0);

            List<DiaryEntry> dailyDiary = DiaryEntry.loadEntriesForDay(daoSession.getDiaryEntryDao(),day, DiaryEntry.DiaryEntryType.DIARY_WEIGHT_ENTRY);
            if(dailyDiary.size()>0){
                DiaryEntry weightEntry = dailyDiary.get(0);
                dailySummary.put(day, userProfile.getCaloriesGoal(weightEntry.getWeight()));
            }
        }
        return dailySummary;
    }
	//================================================================================
    // Webserver Delegate Overrides
    //================================================================================
	@Override
	public void dataReceived(CTWebserviceRequest request,
			CTWebserviceRequest.CTWebserviceRequestType methodCalled, Object data) {

        switch (methodCalled) {
            case REQUEST_POST_DIARY:{
                if(data.getClass() == JSONObject.class){
                    try{
                        meals = ((JSONObject) data).getJSONArray("Meals");
                        exercises = ((JSONObject) data).getJSONArray("Fitness");
                        weighIns = ((JSONObject) data).getJSONArray("Weight");
                        foods = ((JSONObject) data).getJSONArray("Food");
                        waters = ((JSONObject) data).getJSONArray("Water");

                        //Sync Meal Diary Entries
                        SyncTask syncTask = new MealSyncTask(this, daoSession);
                        syncTask.execute(meals);

                        //Sync Weight Diary Entries
                        syncTask = new WeightDiarySyncTask(this, daoSession);
                        syncTask.execute(weighIns);

                        //Sync Water Diary Entries
                        syncTask = new WaterDiarySyncTask(this, daoSession);
                        syncTask.execute(waters);

                        //Sync Exercise Diary Entries
                        syncTask = new ExerciseDiarySyncTask(this, daoSession);
                        syncTask.execute(exercises);

                        syncProfile();
                    }
                    catch (Exception e) {
                        // TODO: handle exception
                        e.printStackTrace();
                    }
                }
                break;
            }
            case REQUEST_GET_FOOD:{
                if(data.getClass() == JSONArray.class){
                    JSONArray newFood = (JSONArray) data;

                    SyncFoodTask task = new SyncFoodTask(this, daoSession.getFoodDao());
                    task.execute(newFood);
                }
                break;
            }
            case REQUEST_GET_EXERCISE:{
                if(data.getClass() == JSONArray.class){
                    JSONArray newExercises = (JSONArray) data;

                    Exercise.SyncExerciseTask task = new Exercise.SyncExerciseTask(this, daoSession.getExerciseDao());
                    task.execute(newExercises);
                }
                break;
            }
            case REQUEST_GET_PROFILE:
            case REQUEST_POST_PROFILE:{
                if(data.getClass() == JSONObject.class){
                    JSONObject jsonObject = (JSONObject)data;

                    String username = MyPlateDefaults.getPref(MyPlateDefaults.PREFS_USERNAME, (String) null);
                    double calories = JSONSafeObject.safeGetDouble("calories", jsonObject);
                    double height = JSONSafeObject.safeGetDouble("height",jsonObject);
                    double goal = JSONSafeObject.safeGetDouble("goal",jsonObject);
                    double activityLevel = JSONSafeObject.safeGetDouble("activity_level",jsonObject);
                    int gender = JSONSafeObject.safeGetString("gender",jsonObject).equals("male")?0:1;
                    int mode = JSONSafeObject.safeGetString("mode",jsonObject).equals("calculated")?0:1;
                    String dob = JSONSafeObject.safeGetString("dob",jsonObject);
                    Profile user = new Profile(username,height,150.0,activityLevel,calories,goal,18,gender,mode,true,(long)0);
                    user.setAge(dob);
                    user.setIsSynchronized(true);
                    this.saveUserProfile(user);
                    this.updateWeightWithLatestEntry();
                }
                profileFinishedSync = true;
                this.updateDelegateSyncInfo();
                break;
            }
            case REQUEST_GET_SEARCH_FOOD:{
                if(data.getClass() == JSONObject.class){
                    JSONObject newFood = (JSONObject) data;
                    JSONArray foodArray = JSONSafeObject.safeGetJSONArray("foods",newFood);

                    //TODO: Fetch all foods with QueryString
                    ModelSyncCompletedDelegate delegate = (ModelSyncCompletedDelegate)request.modelDelegate;
                    SyncFoodTask task = new SyncFoodTask(delegate, daoSession.getFoodDao());
                    task.execute(foodArray);


                }
                break;

            }
            case REQUEST_GET_SEARCH_EXERCISE:{
                if(data.getClass() == JSONObject.class){
                    JSONObject newExercise = (JSONObject) data;
                    JSONArray newExercises = JSONSafeObject.safeGetJSONArray("exercises",newExercise);

                    //TODO: Fetch all foods with QueryString
                    ModelSyncCompletedDelegate delegate = (ModelSyncCompletedDelegate)request.modelDelegate;
                    Exercise.SyncExerciseTask task = new Exercise.SyncExerciseTask(delegate, daoSession.getExerciseDao());
                    task.execute(newExercises);
                }
                break;
            }
            default:
                break;
        }
		// TODO Auto-generated method stub
		Log.d("Debug", data.toString());
	}

		@Override
	public boolean errorOccurred(CTWebserviceRequest request,
			CTWebserviceRequest.CTWebserviceRequestType methodCalled, Exception error,
			String errorMessage) {
		// TODO Auto-generated method stub
            if(errorMessage != null){
                if(delegate != null){
                    delegate.syncCompleted(this,false);
                }

                isSyncing = false;
                Intent intent = new Intent("syncFailed");
                LocalBroadcastManager.getInstance(MyPlateApplication.getContext()).sendBroadcast(intent);
                Log.d("Debug", errorMessage.toString());
            }

		return false;
	}
	
	
	//================================================================================
    // ModelSyncCompletion Delegate Overrides
    //================================================================================
	@Override
	public void finishedSyncThreaded(ModelSyncType syncType, Object data) {
		// TODO Auto-generated method stub
		switch (syncType) {
		case SYNC_TYPE_FOOD:
			if (meals != null){
				MealSyncTask syncTask = new MealSyncTask(this, daoSession);
				syncTask.execute(meals);
			}
			else
			if(foods != null){
				FoodDiarySyncTask syncTask = new FoodDiarySyncTask(this, daoSession);
				syncTask.execute(foods);
			}
			
			
			break;
		case SYNC_TYPE_EXERCISE:
			if(exercises != null){
				ExerciseDiarySyncTask syncTask = new ExerciseDiarySyncTask(this, daoSession);
				syncTask.execute(exercises);
			}
			
		default:
			break;
		}
		
	}

	@Override
	public boolean errorOccurredThreaded(ModelSyncType syncType,
			Exception error, String errorMessage) {
		// TODO Auto-generated method stub
		return false;
	}

	//================================================================================
    // DiarySyncTask Delegate Overrides
    //================================================================================

	@SuppressWarnings("unchecked")
	@Override
	public void finishedSyncThreaded(SyncType syncType, Object data, boolean success) {
		// TODO Auto-generated method stub
		switch (syncType) {
		case DIARY_SYNC_FOOD:
			if(success == false){
				//3) fetch foods from server
                if(data instanceof Set){
                    CTWebserviceRequest.getFoods((Set<String>)data).delegate = this;
                }
            }
			else{
				foods = null;
                this.updateDelegateSyncInfo();

            }
			break;
		case DIARY_SYNC_WEIGHT:
			weighIns = null;
            this.updateWeightWithLatestEntry();
            this.updateDelegateSyncInfo();

            break;
		case DIARY_SYNC_WATER:
			waters = null;
            this.updateDelegateSyncInfo();

            break;
			
		case SYNC_MEALS:
			if(success == false){
				//3) fetch foods from server
                if(data instanceof Set){
                    CTWebserviceRequest.getFoods((Set<String>)data).delegate = this;
                }
            }
			else{
				meals = null;
				
				//Sync foods and Food Diary Entries
				 SyncTask syncTask = new FoodDiarySyncTask(this, daoSession);
				 syncTask.execute(foods);
			}
			break;
		case DIARY_SYNC_EXERCISE:
			if(success == false){
				//3) fetch foods from server
                if(data instanceof Set){
                    CTWebserviceRequest.getExercises((Set<String>)data).delegate = this;
                }
            }
			else{
				exercises = null;
                this.updateDelegateSyncInfo();

            }
			break;
		default:
			break;
		}
		
	}

	@Override
	public boolean errorSyncOccurredThreaded(SyncType syncType,
			Exception error, String errorMessage) {
		// TODO Auto-generated method stub
		return false;
	}

	
}
