package com.livestrong.calorietracker.back.db.sync;

import java.util.Date;
import java.util.Hashtable;
import java.util.Set;
import java.util.concurrent.Callable;

import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

import com.livestrong.calorietracker.back.db.gen.DaoSession;
import com.livestrong.calorietracker.back.db.gen.DiaryEntry;
import com.livestrong.calorietracker.back.db.gen.DiaryEntryDao;
import com.livestrong.calorietracker.back.db.gen.Food;
import com.livestrong.calorietracker.back.db.gen.FoodDao;
import com.livestrong.calorietracker.back.db.sync.SyncTask.SyncDelegate.SyncType;

public class FoodDiarySyncTask extends SyncTask{
	private static Integer customFoodId = 256940;

	public FoodDiarySyncTask(SyncDelegate delegate, DaoSession session) {
		super(delegate, session, SyncType.DIARY_SYNC_FOOD);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected Set<String> doInBackground(JSONArray... params) {
		final JSONArray diaryObject = params[0];
		
		Set<String> foodsNeededToBeSynced = parseMissingServerObjects(diaryObject,"item_id",daoSession.getFoodDao());
		if(foodsNeededToBeSynced == null || foodsNeededToBeSynced.isEmpty() ){
            try {
                daoSession.callInTx(new Callable<Object>() {
                    @Override
                    public Object call() throws Exception {
                        syncFoods(diaryObject);

                        return null;
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
		else{
			return foodsNeededToBeSynced;
		}		
		return null;
	}

    @Override
    protected void onPostExecute(Set<String> foodsNeededToBeSynced) {
        if(foodsNeededToBeSynced != null && foodsNeededToBeSynced.size()>0){
            delegate.finishedSyncThreaded(SyncType.DIARY_SYNC_FOOD, foodsNeededToBeSynced,false);
        }
        else{
            super.onPostExecute(null);
        }
    }
	private boolean syncFoods(JSONArray foodRecords){
		if (foodRecords.length() == 0){
			// _finishedFoods = YES;
			return true;
		}

		// prefetch any existing Diary entries in one query, into a map of guid -> object
		DiaryEntryDao diaryDao = daoSession.getDiaryEntryDao();
		Hashtable<String, DiaryEntry> recordsById = getRecordsById(diaryDao,foodRecords, "guid");

		// prefetch any existing Food entries in one query, into a map of guid -> object
		FoodDao foodDao = daoSession.getFoodDao();
		Hashtable<String, Food> foodsById = getRecordsById(foodDao,foodRecords, "item_id");


		for(int i = 0; i < foodRecords.length(); i++){
			String dateStamp = null;
			Integer foodId = null;
			JSONObject foodObject = null;
			DiaryEntry diaryEntry = null;
			Food food = null;
			//Meal meal = null;

			try{
				foodObject = foodRecords.getJSONObject(i);
				String remoteId = foodObject.getString("guid");
				diaryEntry = recordsById.get(remoteId);

				if(foodObject.has("datestamp")){
					dateStamp = foodObject.getString("datestamp");
				}
				// make sure we have the referenced food, sometimes they disappear from the server
				if(foodObject.has("item_id")){
					foodId = foodObject.getInt("item_id");
				}


				// special handling for deleted items, don't touch any fields except for the deleted 
				// flag because they don't send all the records
				if(foodObject.has("deleted") && foodObject.getString("deleted").equals("1")){
					if(diaryEntry != null){
						Log.d("Debug", "Received deleted food diary entry" + remoteId);
						diaryEntry.setDateDeleted(new Date());
						diaryEntry.setIsSynchronized(true);

                        //update into database
                        diaryDao.update(diaryEntry);
					}
					continue;
				}

				//Check if this is a custom food and do some weird shit that accomidates the server... :(
				if(foodId != customFoodId){
					food = foodsById.get(foodId.toString());
					if (food == null) {
						Log.d("Debug", "Error, missing food"+  foodId + "for diary entry" + remoteId +"for" + dateStamp);
						return false;
					}
				}


				//The Diary doesn't exist locally - add it.
				if (diaryEntry == null) {
					Log.d("Debug", "Received new food diary " + remoteId + " for " + dateStamp);
					diaryEntry = new DiaryEntry();
					diaryEntry.setRemoteId(remoteId);
					if(foodId == customFoodId){
						food = new Food(true);
						foodDao.insert(food);
						diaryEntry.setFood(food);
					}
					else if(food != null){
						diaryEntry.setFood(food);
					}
					diaryDao.insert(diaryEntry);

				}
				else{
					//We are updating it locally
					Log.d("Debug", "Received updated food diary " + remoteId + " for " + dateStamp);
					if(foodId == customFoodId){						
						food = diaryEntry.getFood();
					}
				}

				diaryEntry.updateFoodDiaryWithData(foodObject);
				diaryEntry.updateConsumption();

				//update into database
				diaryDao.update(diaryEntry);
				foodDao.update(food);
			}
			catch (Exception e) {
				e.printStackTrace();
				continue;
			}


		}

		Log.d("Debug", "Done with Food Diaries");
		return true;

	}
}
