package com.livestrong.calorietracker.back.db.sync;

import java.util.Date;
import java.util.Hashtable;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

import com.livestrong.calorietracker.back.db.gen.DaoSession;
import com.livestrong.calorietracker.back.db.gen.DiaryEntry;
import com.livestrong.calorietracker.back.db.gen.DiaryEntryDao;
import com.livestrong.calorietracker.back.db.gen.Exercise;
import com.livestrong.calorietracker.back.db.gen.ExerciseDao;
import com.livestrong.calorietracker.back.db.sync.SyncTask.SyncDelegate.SyncType;
import com.livestrong.calorietracker.utilities.JSONSafeObject;

public class ExerciseDiarySyncTask extends SyncTask{
	private static Integer customExerciseId = 87;

	public ExerciseDiarySyncTask(SyncDelegate delegate, DaoSession session){
		super(delegate, session, SyncType.DIARY_SYNC_EXERCISE);
		// TODO Auto-generated constructor stub
	}

    /** The system calls this to perform work in a worker thread and
     * delivers it the parameters given to AsyncTask.execute() */
	@Override
	protected Set<String> doInBackground(JSONArray... params) {
		JSONArray diaryObject = params[0];
		
		Set<String> exercisesNeededToBeSynced = parseMissingServerObjects(diaryObject,"fitness_id",daoSession.getExerciseDao());
		if(exercisesNeededToBeSynced == null || exercisesNeededToBeSynced.isEmpty() ){
			syncExercises(diaryObject);
		}
		else{
			return exercisesNeededToBeSynced;
		}		
		return null;
	}


    @Override
     protected void onPostExecute(Set<String> exercisesNeededToBeSynced) {
        if(exercisesNeededToBeSynced != null && exercisesNeededToBeSynced.size()>0){
            delegate.finishedSyncThreaded(SyncType.DIARY_SYNC_EXERCISE, exercisesNeededToBeSynced,false);
        }
        else{
          super.onPostExecute(null);
        }
    }
	private boolean syncExercises(JSONArray exerciseRecords){
		if (exerciseRecords.length() == 0){
			// _finishedFoods = YES;
			return true;
		}

		// prefetch any existing Diary entries in one query, into a map of guid -> object
		DiaryEntryDao diaryDao = daoSession.getDiaryEntryDao();
		Hashtable<String, DiaryEntry> recordsById = getRecordsById(diaryDao,exerciseRecords,"guid");

		// prefetch any existing Exercise entries in one query, into a map of guid -> object
		ExerciseDao exerciseDao = daoSession.getExerciseDao();
		Hashtable<String, Exercise> exerciseById = getRecordsById(exerciseDao,exerciseRecords, "fitness_id");


		for(int i = 0; i < exerciseRecords.length(); i++){
			String dateStamp = null;
			Integer exerciseId = null;
			JSONObject exerciseObject = null;
			DiaryEntry diaryEntry = null;
			Exercise exercise = null;

			try{
				exerciseObject = exerciseRecords.getJSONObject(i);
				String remoteId = exerciseObject.getString("guid");
				diaryEntry = recordsById.get(remoteId);
				dateStamp = JSONSafeObject.safeGetString("datestamp", exerciseObject);
				exerciseId = JSONSafeObject.safeGetInt("fitness_id", exerciseObject);

				// special handling for deleted items, don't touch any fields except for the deleted
				// flag because they don't send all the records
				if(JSONSafeObject.safeGetString("deleted",exerciseObject).equals("1")){
					if(diaryEntry != null){
						Log.d("Debug", "Received deleted exercise diary entry" + remoteId);
						diaryEntry.setDateDeleted(new Date());
						diaryEntry.setIsSynchronized(true);

                        //update into database
                        diaryDao.update(diaryEntry);
					}
					continue;
				}

				//Check if this is a custom food and do some weird shit that accomidates the server... :(
				if(exerciseId != customExerciseId){
					exercise = exerciseById.get(exerciseId.toString());
					if (exercise == null) {
						Log.d("Debug", "Error, missing Exercise"+  exerciseById + "for diary entry" + remoteId +"for" + dateStamp);
						return false;
					}
				}


				//The Diary doesn't exist locally - add it.
				if (diaryEntry == null) {
					Log.d("Debug", "Received new Exercise diary " + remoteId + " for " + dateStamp);
					diaryEntry = new DiaryEntry();
					diaryEntry.setRemoteId(remoteId);
					if(exerciseId == customExerciseId){
						exercise = new Exercise(true);
						exerciseDao.insert(exercise);
						diaryEntry.setExercise(exercise);
					}
					else if(exercise != null){
						diaryEntry.setExercise(exercise);
					}
					diaryDao.insert(diaryEntry);

				}
				else{
					//We are updating it locally
					Log.d("Debug", "Received updated Exercise diary " + remoteId + " for " + dateStamp);
					if(exerciseId == customExerciseId){						
						exercise = diaryEntry.getExercise();
					}
				}

				diaryEntry.updateExerciseDiaryWithData(exerciseObject);
				diaryEntry.updateConsumption();

				//update into database
				diaryDao.update(diaryEntry);
				exerciseDao.update(exercise);
			}
			catch (Exception e) {
				e.printStackTrace();
				continue;
			}


		}

		Log.d("Debug", "Done with Food Diaries");
		return true;

	}
}
