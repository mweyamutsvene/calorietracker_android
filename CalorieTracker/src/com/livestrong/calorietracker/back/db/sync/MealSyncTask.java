package com.livestrong.calorietracker.back.db.sync;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.livestrong.calorietracker.back.db.gen.DaoSession;
import com.livestrong.calorietracker.back.db.gen.Food;
import com.livestrong.calorietracker.back.db.gen.Meal;
import com.livestrong.calorietracker.back.db.gen.MealDao;
import com.livestrong.calorietracker.back.db.gen.MealItem;
import com.livestrong.calorietracker.back.db.gen.MealItemDao;
import com.livestrong.calorietracker.back.db.sync.SyncTask.SyncDelegate.SyncType;

public class MealSyncTask extends SyncTask {

	public MealSyncTask(SyncDelegate delegate, DaoSession session) {
		super(delegate, session, SyncType.SYNC_MEALS);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected Set<String> doInBackground(JSONArray... params) {
		JSONArray diaryObject = params[0];
		Set<String> foodsNeededToBeSynced = parseMissingServerObjects(separateFoodItemsFromMeals(diaryObject),"item_id",daoSession.getFoodDao());
		if(foodsNeededToBeSynced == null || foodsNeededToBeSynced.isEmpty() ){
			syncMeals(diaryObject);
		}
		else{
			return foodsNeededToBeSynced;
		}
		return super.doInBackground(params);

	}
    @Override
    protected void onPostExecute(Set<String> foodsNeededToBeSynced) {
        if(foodsNeededToBeSynced != null && foodsNeededToBeSynced.size()>0){
            delegate.finishedSyncThreaded(SyncType.SYNC_MEALS, foodsNeededToBeSynced,false);
        }
        else{
            super.onPostExecute(null);
        }
    }
	private JSONArray separateFoodItemsFromMeals(JSONArray meals){
		List<JSONObject> listOfFoods = new ArrayList<JSONObject>();
		for(int i = 0; i < meals.length(); i++){
			JSONObject meal = null;
			JSONArray foods = null;
			try {
				meal = meals.getJSONObject(i);
				foods = meal.getJSONArray("items");
				for (int j = 0; j < foods.length(); j++) {
					listOfFoods.add(foods.getJSONObject(j));
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return new JSONArray(listOfFoods);
	}
	
	private void syncMeals(JSONArray mealObjects){
		 // prefetch any existing Diary entries in one query, into a map of guid -> object
		MealDao mealDao = daoSession.getMealDao();
		MealItemDao mealItemDao = daoSession.getMealItemDao();

		Hashtable<String, Meal> recordsById = getRecordsById(mealDao,mealObjects,"meal_id");
		
		for(int i = 0; i < mealObjects.length(); i++){
			JSONObject mealObject = null;

			try {
				mealObject = mealObjects.getJSONObject(i);
				String remoteId = mealObject.getString("meal_id");
				Meal meal = recordsById.get(remoteId);
				if(meal == null){
					meal = new Meal();
					meal.setRemoteId(remoteId);
					mealDao.insert(meal);
				}
				else{
					List<MealItem> mealItems = meal.getMealItemList();
					for (MealItem mealItem : mealItems) {
						daoSession.delete(mealItem);
					}
				}
				//Update core meal data
				meal.updateMealWithData(mealObject);
				
				//create meal items for each meal
				JSONArray items = mealObject.getJSONArray("items");
				for (int j = 0; j < items.length(); j++) {
					JSONObject item = items.getJSONObject(j);
					Integer foodId = item.getInt("item_id");
					Float servings = Float.parseFloat(item.getString("servings"));
					
					Food food = Food.getFood(foodId.toString(),  daoSession.getFoodDao());
					if (food == null) {
	                    Log.d("Debug","Error, missing food" + foodId);
	                    continue;
	                }
					
					MealItem mealItem = new MealItem();
                    mealItemDao.insert(mealItem);

                    mealItem.setFood(food);
					mealItem.setServings(servings);
					mealItem.setMeal(meal);
                    mealItem.setMealItemId(meal.getId());
                    mealItemDao.update(mealItem);

                    //meal.getMealItemList().add(mealItem);

				}
				mealDao.update(meal);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			

		}
		Log.d("Debug", "Done with Food Diaries");

	}
	
	
	
}
