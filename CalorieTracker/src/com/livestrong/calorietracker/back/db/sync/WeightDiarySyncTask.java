package com.livestrong.calorietracker.back.db.sync;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.livestrong.calorietracker.back.db.gen.DaoSession;
import com.livestrong.calorietracker.back.db.gen.DiaryEntry;
import com.livestrong.calorietracker.back.db.gen.DiaryEntryDao;

public class WeightDiarySyncTask extends SyncTask {

	public WeightDiarySyncTask(SyncDelegate delegate, DaoSession session) {
		super(delegate, session, SyncDelegate.SyncType.DIARY_SYNC_WEIGHT);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected Set<String> doInBackground(JSONArray... params) {
		JSONArray diaryObject = params[0];
		syncWeighIns(diaryObject);
		return super.doInBackground(params);
	}
	
	private void syncWeighIns(JSONArray weighIns){
		if (weighIns.length() == 0){
			// _finishedWeigh = YES;
			return;
		}

		 // prefetch any existing Diary entries in one query, into a map of guid -> object
		DiaryEntryDao diaryDao = daoSession.getDiaryEntryDao();
		Hashtable<String, DiaryEntry> recordsById = getRecordsById(diaryDao,weighIns, "guid");
	
		
		
		for(int i = 0; i < weighIns.length(); i++){
			
			JSONObject weighinObject = null;
			DiaryEntry diaryEntry = null;
			String dateStamp = null;
			
			try{
				weighinObject = weighIns.getJSONObject(i);
				String remoteId = weighinObject.getString("guid");
				
				SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
				dateStamp = weighinObject.getString("datestamp");
				Date date = dateFormatter.parse(dateStamp);
				
				diaryEntry = recordsById.get(remoteId);

				// special handling for deleted items, don't touch any fields except for the deleted 
				// flag because they don't send all the records
				if(weighinObject.has("deleted") && weighinObject.getString("deleted").equals("1")){
					if(diaryEntry != null){
						Log.d("Debug", "Received deleted weighin diary entry:" + remoteId);
						diaryEntry.setDateDeleted(new Date());
						diaryEntry.setIsSynchronized(false);

                        diaryDao.update(diaryEntry);

                    }
					continue;
				}
				
				 // adding or updating the record for this date?
				Boolean shouldUpdate = false;
	            if (diaryEntry == null) {
					Log.d("Debug", "Received New weighin diary entry:" + remoteId);
					diaryEntry = new DiaryEntry();
					diaryEntry.setRemoteId(remoteId);
					diaryEntry.setEntryDate(date);
	            } else {
	            	//we are updating here
					Log.d("Debug", "Received Update weighin diary entry:" + remoteId);
					shouldUpdate = true;
	            }
	            
	            diaryEntry.updateWeightDiaryWithData(weighinObject);	           

	            //Either update or insert into database
	            if(shouldUpdate == true){
	            	diaryDao.update(diaryEntry);
	            }
	            else{
	            	diaryDao.insert(diaryEntry);
	            }
			}
			catch (JSONException e) {
				Log.d("Debug", "JSON parse Exception ");
				e.printStackTrace();

				continue;
				// TODO: handle exception
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		Log.d("Debug", "Done with Weighins");

	}

}
