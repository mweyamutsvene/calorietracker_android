package com.livestrong.calorietracker.back.db.sync;


import java.util.Hashtable;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.database.sqlite.SQLiteException;
import android.os.AsyncTask;

import com.livestrong.calorietracker.back.db.gen.DaoSession;
import com.livestrong.calorietracker.back.db.gen.GenericModelObject;
import com.livestrong.calorietracker.back.db.sync.SyncTask.SyncDelegate.SyncType;


import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.query.QueryBuilder;
import de.greenrobot.dao.query.WhereCondition.StringCondition;

public class SyncTask extends AsyncTask<JSONArray, Void, Set<String>>{
	public interface SyncDelegate {
		public enum SyncType
		{
			DIARY_SYNC_FOOD,
			DIARY_SYNC_EXERCISE,
			DIARY_SYNC_WEIGHT,
			DIARY_SYNC_WATER,
			SYNC_MEALS,

		} ;
		public void finishedSyncThreaded(final SyncType syncType, final Object data, final boolean success);
		public boolean errorSyncOccurredThreaded(final SyncType syncType, final Exception error, final String errorMessage);

	}
	protected SyncDelegate delegate;
	protected DaoSession daoSession;
	private SyncType type;

	public SyncTask(SyncDelegate delegate, DaoSession session,SyncType diaryType) {
		this.delegate = delegate;
		this.daoSession = session;
		this.type = diaryType;
	}

    /** The system calls this to perform work in a worker thread and
     * delivers it the parameters given to AsyncTask.execute() */
	protected Set<String> doInBackground(JSONArray... params) {
		return null;
	}

    /** The system calls this to perform work in the UI thread and delivers
     * the result from doInBackground() */
    protected void onPostExecute(Set<String> result) {
        delegate.finishedSyncThreaded(type, result,true);

    }


	@SuppressWarnings("unchecked")
	private <D> Set<D> getSetOfPropertiesForKey(String key, JSONArray array){
		Set<D> newIdSet = new TreeSet<D>();
        if(array == null){
            return newIdSet;
        }
		for(int i = 0; i < array.length(); i++){
			D itemId = null;
			try{
				JSONObject object = array.getJSONObject(i);
				itemId = (D) object.get(key);
			}
			catch (Exception e) {
				continue;
			}

			newIdSet.add(itemId);

		}
		return newIdSet;
	}
	
	protected  <D extends GenericModelObject> Hashtable<String, D> getRecordsById(AbstractDao<D, Long> dao, JSONArray records, String propertyName){
		//Get ids from records with property name
		Set<String> newIdSet = getSetOfPropertiesForKey(propertyName, records);
        if(newIdSet.size() == 0){
            return null;
        }

		//Build Query
		String idsToFetchString= null;
		if(newIdSet.toArray()[0].toString().contains("-")){
			Set<String> alteredSet = new TreeSet<String>();
			for (String id : newIdSet) {
				id = ("\"" + id + "\"");
				alteredSet.add(id);
			}
			idsToFetchString= StringUtils.join(alteredSet, ",");
		}
		else{
			idsToFetchString= StringUtils.join(newIdSet, ",");

		}
		String queryString = "REMOTE_ID IN " + "("+idsToFetchString+")";
		QueryBuilder<D> qb = dao.queryBuilder();
		qb.where(new StringCondition(queryString)).build();
		List<D> list = null;
		try {
			list = qb.list();
		} catch (SQLiteException e) {
			return null;
		}		
		//Create hashtable of records with recordId
		Hashtable<String,D> recordsById = new Hashtable<String, D>();
		ListIterator<D> listIterator = list.listIterator();
		while(listIterator.hasNext()){
			D entry = listIterator.next();
			recordsById.put(entry.getRemoteId().toString(), entry);
		}

		return recordsById;
	}
	
	protected Set<String> parseMissingServerObjects(JSONArray serverRecords, String key, AbstractDao<?, ?> dao){
		//1) get all disjoint ids from key item_id
		Set<String> newItemIdSet = getSetOfPropertiesForKey(key, serverRecords);

		//2) find set of ids not currently in database
		String idsToFetchString = StringUtils.join(newItemIdSet, ",");
		String queryString = "REMOTE_ID IN " + "("+idsToFetchString+")";

		QueryBuilder<?> qb = dao.queryBuilder();
		qb.where(new StringCondition(queryString)).build();
		List<?> genericModelObjects = null;
		try {
			genericModelObjects = qb.list();
		} catch (SQLiteException e) {
			e.printStackTrace();
			return null;
		}

		Set<String> currentItemIdSet =new TreeSet<String>();

		for (Object item: genericModelObjects) {
			currentItemIdSet.add(((GenericModelObject)item).getRemoteId().toString());
		}

		newItemIdSet.removeAll(currentItemIdSet);
		return newItemIdSet;

	}
	
}
