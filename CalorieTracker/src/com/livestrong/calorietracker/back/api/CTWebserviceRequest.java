package com.livestrong.calorietracker.back.api;


import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Locale;
import java.util.Set;

import com.livestrong.calorietracker.Constants;

import org.apache.commons.lang.StringUtils;
import org.apache.http.Header;
import org.apache.http.HeaderIterator;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.os.Build;
import android.util.Log;

import com.livestrong.calorietracker.configuration.MyPlateDefaults;
import com.livestrong.calorietracker.utilities.CTUtilities;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

/**
 * Request that manages webservice call with the object that called the request
 * 
 * @author thomashanks
 *
 */

public class CTWebserviceRequest {
	//Enum for identifying webservice calls
	public enum CTWebserviceRequestType
	{
	    REQUEST_GET_EXERCISE,
        REQUEST_GET_SEARCH_EXERCISE,
        REQUEST_GET_FOOD,
        REQUEST_GET_SEARCH_FOOD,
        REQUEST_GET_UPC,
	    REQUEST_GET_PROFILE,
	    REQUEST_GET_MESSAGES,
	    REQUEST_GET_COMMENTS,
	    REQUEST_GET_FACEBOOKLOGIN,
	    REQUEST_GET_STORE_PRODUCTS,
	    REQUEST_GET_STORE_CHECK_ACCESS_TO_PRODUCTS,
	    REQUEST_POST_SIGNUP,
	    REQUEST_POST_LOGIN,
	    REQUEST_POST_PROFILE,
	    REQUEST_POST_DIARY,
	    REQUEST_POST_MESSAGE,
	    REQUEST_POST_COMMENT,
	    REQUEST_POST_STORE_PAYMENT_RECEIPT
	};

	private enum CTWebserviceRequestHTTPMethod
	{
	  HTTPMETHOD_POST,
	  HTTPMETHOD_PUT,
	  HTTPMETHOD_GET,
	  HTTPMETHOD_DELETE,
	} ;


	public interface CTWebserviceRequestDelegate {

		/**
		 * Callback method used when the requested data is ready.
		 *  
		 * @param methodCalled See DataHelper.METHOD_* constants.
		 * @param data The requested data.
		 */
		public void dataReceived(CTWebserviceRequest request,final CTWebserviceRequestType methodCalled, final Object data);

		/**
		 * Call method called when an error occurs while trying to fetch data from the local database or remote API.
		 * 
		 * @param methodCalled 
		 * @param error The exception that was thrown, if any. null otherwise.
		 * @param errorMessage The human-readable error message, if any. Can also be null.
		 * @return True if your implementation handled the error, false otherwise. If false is returned, the DataHelper will try to display a generic error message dialog. Return true if you don't want that.
		 */
		public boolean errorOccurred(CTWebserviceRequest request, CTWebserviceRequestType methodCalled, final Exception error, final String errorMessage);

	}
	
	//private static String BASE_URL = "http://mobile.livestrongdev.com";
    private static String BASE_URL = "https://www.livestrong.com";

    private static AsyncHttpClient client = new AsyncHttpClient();
	private CTWebserviceRequestHTTPMethod methodType;
	private CTWebserviceRequestType requestType;
	private HttpRequest myRequest;
	private HttpResponse myResponse;
	private RequestParams requestParams;
	public CTWebserviceRequestDelegate delegate;
    public Object modelDelegate = null;
	public Object storedObject = null;
	
	public CTWebserviceRequest(){
		//BASE_URL = "http://mobile.livestrongdev.com";
        if(Constants.DEBUG_MODE == true){
            //BASE_URL = "http://mobile.livestrongdev.com";
    		createLoggingMechanisms();
        }
        client.setUserAgent("CalorieTracker/" + Constants.APPLICATION_VERSION + " (Android " + Build.VERSION.RELEASE + ")");
    }
	/**
	 * @return the delegate
	 */
	public CTWebserviceRequestDelegate getDelegate() {
		return delegate;
	}

	/**
	 * @param delegate the delegate to set
	 */
	public void setDelegate(CTWebserviceRequestDelegate delegate) {
		this.delegate = delegate;
	}

	public CTWebserviceRequestHTTPMethod getMethodType() {
		return methodType;
	}
	public void setMethodType(CTWebserviceRequestHTTPMethod methodType) {
		this.methodType = methodType;
	}
    /**
     * Tells you if the user has logged in or not. The UI and API calls will change depending on this.
     * @return boolean isLoggedIn
     */
    public static boolean isLoggedIn() {
        return MyPlateDefaults.getPref(MyPlateDefaults.PREFS_USERNAME, (String) null) != null;
    }
	public static boolean isOnline(Context context) {
		ConnectivityManager connec = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeInfo = connec.getActiveNetworkInfo();
		if (activeInfo != null && (activeInfo.isConnected() || activeInfo.getState().equals(State.CONNECTED))) {
			return true;
		}

		NetworkInfo mobileInfo = connec.getNetworkInfo(0);
		NetworkInfo wifiInfo = connec.getNetworkInfo(1);
		NetworkInfo wimaxInfo = connec.getNetworkInfo(6);
		boolean bm = mobileInfo != null ? (mobileInfo.isConnected() || mobileInfo.getState().equals(State.CONNECTED)) : false;
		boolean bw = wimaxInfo != null ? (wimaxInfo.isConnected() || wimaxInfo.getState().equals(State.CONNECTED)) : false;
		boolean bx = wifiInfo != null ? (wifiInfo.isConnected() || wifiInfo.getState().equals(State.CONNECTED)) : false;
		
		if (bm || bw || bx){
			return true;
		} else {
			Log.d("ApiHelper", "WARNING: is NOT online");
			return false;
		}
	}
	/**
	 * @param type CTWebserviceRequestType for current request
	 * @return string relative path of webservice call
	 */
	private static String getRelativeUrl(CTWebserviceRequestType type){
		String path = null;
	    switch (type) {
            case REQUEST_GET_SEARCH_EXERCISE:
	        case REQUEST_GET_EXERCISE:{
	            path = "/service/fitness/exercises/";
	            break;
	        }
            case REQUEST_GET_SEARCH_FOOD:
	        case REQUEST_GET_FOOD:{
	            path = "/service/food/foods/";
	            break;
	        }
	        case REQUEST_GET_UPC:{
	            path = "/service/food/upc/";
	            break;
	        }
	        case REQUEST_GET_PROFILE:{
	            path = "/service/user/profile/";
	            break;
	        }
	        case REQUEST_GET_MESSAGES:{
	            path = "/api/dares/dare/657/";
	            break;
	        }
	        case REQUEST_GET_COMMENTS:{
	            path = "/api/dares/post/";
	            break;
	        }
	        case REQUEST_GET_FACEBOOKLOGIN:{
	            path = "/service/user/facebook/";
	            break;
	        }
	        case REQUEST_GET_STORE_PRODUCTS:
	        {
	            path = "api/products/fetchProducts/";
	            break;
	        }
	        case REQUEST_GET_STORE_CHECK_ACCESS_TO_PRODUCTS:
	        {
	            path = "/api/products/checkAccess/";
	            break;
	        }
	        case REQUEST_POST_SIGNUP:{
	            path = "/service/Registration/Register/";
	            break;
	        }
	        case REQUEST_POST_LOGIN:{
	            //path = @"/api/dares/authenticateduser/";
	            path = "/service/user/authenticate/";
	            break;
	        }
	        case REQUEST_POST_PROFILE:{
	            path = "/service/user/profile/";
	            break;
	        }
	        case REQUEST_POST_DIARY:{
	            path = "/service/sync/diary/";
	            break;
	        }
	        case REQUEST_POST_MESSAGE:{
	            path = "/api/dares/posts/657/";
	            break;
	        }
	        case REQUEST_POST_COMMENT:{
	            path = "/api/dares/comments/";
	            break;
	        }
	        case REQUEST_POST_STORE_PAYMENT_RECEIPT:{
	        	path = "api/products/validateInAppPurchase/?isSandbox=1";
	        	break;
	        }
	        default:
	            break;
	    }
	    return path;

	}
	
	/**
	 * Logs the current request before it is made - implicitly called
	 */
	private void logRequest(){
		HttpRequest request = myRequest;
		
		//Get request Method and URL
    	String requestString = "REQUEST: " + request.getRequestLine().getMethod() +  request.getRequestLine().getUri();
    	String headerString = "\n{\n";
    	
    	//Get headers
    	HeaderIterator headers =  request.headerIterator();
    	while(headers.hasNext()){
    		Object element = headers.next();
    		headerString += "    " + element.toString() + "\n";
    	}
    	headerString += "}\n";
    	
    	if(requestParams != null){
    		headerString += requestParams.toString();
    	}
    	//Combine headers and method and URL
    	requestString += headerString;
		Log.d("Debug", requestString);

        
	}
	
	
	/**
	 * Logs the response message if explicitly called
	 * 
	 * @param responseMessage
	 */
	private void logResponse(String responseMessage){
        if(Constants.DEBUG_MODE == false || myRequest == null || myResponse == null){
            return;
        }
		HttpRequest request = myRequest;
		HttpResponse response = myResponse;
		
		//Get Response Method and URL
    	String requestString = "RESPONSE: "+ request.getRequestLine().getMethod() +  request.getRequestLine().getUri();
    	String headerString = "\n{\n";
    	
    	//Get headers
    	HeaderIterator headers =  response.headerIterator();
    	while(headers.hasNext()){
    		Object element = headers.next();
    		headerString += "    " + element.toString() + "\n";
    	}
    	headerString += "}\n" + responseMessage;
    	
    	//Combine headers and method and URL
    	requestString += headerString;
		Log.d("Debug", requestString);
	}
	
	private static void addAuthorizationHeaderForRequest(){
		//Create Auth Header
		String username = MyPlateDefaults.getPref(MyPlateDefaults.PREFS_USERNAME, (String)null);
		String password = MyPlateDefaults.getPref(MyPlateDefaults.PREFS_PASSWORD, (String)null);
        if(username == null || password == null){
            return;
        }

		UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(username,password);
		Header header = BasicScheme.authenticate(credentials, "UTF-8", false);
		client.addHeader(header.getName(), header.getValue());

	}
	/**
	 * Creates the absolute URL based on the passed in relative URL
	 * 
	 * @param relativeUrl
	 * @return
	 */
	private static String getAbsoluteUrl(String relativeUrl) {
		return BASE_URL + relativeUrl;
	}
	
	/**
	 * Creates a logging mechanism for the request and response of
	 * the request
	 */
	private void createLoggingMechanisms(){
		//Add Interceptors to capture data to be used for logging
		DefaultHttpClient myClient = (DefaultHttpClient) client.getHttpClient();
		myClient.addResponseInterceptor(new HttpResponseInterceptor() {
			@Override
			public void process(HttpResponse response, HttpContext context){
				myResponse = response;
			}
		},0);
		
		myClient.addRequestInterceptor(new HttpRequestInterceptor() {
            @Override
            public void process(HttpRequest request, HttpContext context) {
            	//Save copy of request for future logging
            	myRequest = request;
            	logRequest();
            }
        });
	}

    public void onSuccess(JSONArray responseObject){}

	/**
	 * Creates the response handler for the webservice call
	 * 
	 * @param request The current request
	 * @param requestType The type of the current request
	 * @return JsonHttpResponseHandler Handler for the current request
	 */
	private static JsonHttpResponseHandler createResponseHandler(final CTWebserviceRequest request, final CTWebserviceRequestType requestType){
		return  new JsonHttpResponseHandler(){
			
			@Override
			public void onSuccess(JSONArray responseObject) {
               request.onSuccess(responseObject);
				// We successfully have received a response - inform the delegate
				// if it exists;
				request.logResponse(responseObject.toString());

				if(request.delegate != null){
					request.delegate.dataReceived(request, requestType, responseObject);
				}
			}
			@Override
			public void onSuccess(JSONObject responseObject) {
				// We successfully have received a response - inform the delegate
				// if it exists;
				request.logResponse(responseObject.toString());

				if(request.delegate != null){
					request.delegate.dataReceived(request, requestType, responseObject);
				}

			}
			@Override
			public void onFailure(Throwable e, JSONArray errorResponse) {
				// We have received a failed response - inform the delegate
				// if it exists;
                String errorString = (errorResponse != null)?errorResponse.toString():null;
                request.logResponse(errorString);

				if(request.delegate != null){
					request.delegate.errorOccurred(request, requestType, (Exception) e, errorString);
				}

			}
			@Override
			public void onFailure(Throwable e, JSONObject errorResponse) {
				// We have received a failed response - inform the delegate
				// if it exists;
                String errorString = (errorResponse != null)?errorResponse.toString():null;

                request.logResponse(errorString);
				if(request.delegate != null){
					request.delegate.errorOccurred(request, requestType, (Exception) e, errorString);
				}

			}

            @Override
            protected void handleFailureMessage(java.lang.Throwable throwable, java.lang.String s)
            {
                super.handleFailureMessage(throwable,s);
                if(request.delegate != null){
                    request.delegate.errorOccurred(request, requestType, (Exception) throwable, s);
                }
            }


		};
	
	}

    private static Object parseResponse(String responseBody) throws JSONException {
        Object result = null;
        //trim the string to prevent start with blank, and test if the string is valid JSON, because the parser don't do this :(. If Json is not valid this will return null
        responseBody = responseBody.trim();
        if(responseBody.startsWith("{") || responseBody.startsWith("[")) {
            result = new JSONTokener(responseBody).nextValue();
        }
        if (result == null) {
            result = responseBody;
        }
        return result;
    }

    private static AsyncHttpResponseHandler createPlainResponseHandler(final CTWebserviceRequest request, final CTWebserviceRequestType requestType){
        return  new AsyncHttpResponseHandler(){

            @Override
            public void onSuccess(java.lang.String s) {
                Object responseObject = null;
                try {
                    responseObject = parseResponse(s);
                } catch (JSONException e) {
                    e.printStackTrace();
                    if(request.delegate != null){
                        request.delegate.errorOccurred(request, requestType, (Exception) e, "Parse Error");
                    }
                }


                // We successfully have received a response - inform the delegate
                // if it exists;
                request.logResponse(responseObject.toString());

                if(request.delegate != null){
                    request.delegate.dataReceived(request, requestType, responseObject);
                }
            }

            @Override
            public void onFailure(java.lang.Throwable throwable, java.lang.String s) {
                // We have received a failed response - inform the delegate
                // if it exists;
                String errorString = (s != null)?s.toString():null;
                request.logResponse(errorString);

                if(request.delegate != null){
                    request.delegate.errorOccurred(request, requestType, (Exception) throwable, errorString);
                }

            }

        };

    }

	private static void get(String url, RequestParams params, JsonHttpResponseHandler responseHandler) {
		client.get(getAbsoluteUrl(url), params, responseHandler);
	}

	private static void postJsonData(JSONObject jsonObject, String url, AsyncHttpResponseHandler responseHandler) {
		StringEntity entity = null;
		try {
			entity = new StringEntity(jsonObject.toString());

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		client.post(null, getAbsoluteUrl(url), entity, "application/json", responseHandler);

	}
	private static void postFormData(String url, RequestParams params, JsonHttpResponseHandler responseHandler) {
		client.post(getAbsoluteUrl(url), params, responseHandler);
	}
	
	
	//======================
	//	Get Items
	//======================

	/**
	 * Requests an exercise object with given remoteId
	 * @category GET METHOD
	 * 
	 * @param remoteId id of exercise
	 * @throws JSONException
	 */
	public static CTWebserviceRequest getExercise(String remoteId) {
		CTWebserviceRequest request = new CTWebserviceRequest();
		request.requestType = CTWebserviceRequestType.REQUEST_GET_EXERCISE;
		request.setMethodType(CTWebserviceRequestHTTPMethod.HTTPMETHOD_GET);
		
		//Add auth header
		addAuthorizationHeaderForRequest();
		
		//Create params for request
		RequestParams params = new RequestParams();
	    if(remoteId != null) params.put("fitnessId", remoteId);

		//Create Handler For response
		JsonHttpResponseHandler responseHandler = createResponseHandler(request, request.requestType);
		get(getRelativeUrl(request.requestType), params, responseHandler);
		
		return request;
	}
	
	/**
	 * Requests an array of exercise objects with given remoteIds
	 * @category GET METHOD
	 * 
	 * @param idsToFetch ids of exercises
	 * @throws JSONException
	 */
	public static CTWebserviceRequest getExercises(Set<String> idsToFetch){
	
		CTWebserviceRequest request = new CTWebserviceRequest();
		request.requestType = CTWebserviceRequestType.REQUEST_GET_EXERCISE;
		request.setMethodType(CTWebserviceRequestHTTPMethod.HTTPMETHOD_GET);
	
		//Add auth header
		addAuthorizationHeaderForRequest();
		String idsToFetchString = StringUtils.join(idsToFetch, ",");
		
		//Create params for request
		RequestParams params = new RequestParams();
		if(idsToFetchString != null) params.put("fitnessId", idsToFetchString);
		params.put("limit", "9999");

		//Create Handler For response
		JsonHttpResponseHandler responseHandler = createResponseHandler(request, request.requestType);
		get(getRelativeUrl(request.requestType), params, responseHandler);
		
		return request;
	}
	
	/**
	 * Requests an array of exercise objects with given search string
	 * @category GET METHOD
	 * 
	 * @param searchString search exercises
	 * @throws JSONException
	 */
	public static CTWebserviceRequest getSearchExercises(String searchString){
	
		CTWebserviceRequest request = new CTWebserviceRequest();
		request.requestType = CTWebserviceRequestType.REQUEST_GET_SEARCH_EXERCISE;
		request.setMethodType(CTWebserviceRequestHTTPMethod.HTTPMETHOD_GET);
	
		//Add auth header
		addAuthorizationHeaderForRequest();
		
		//Create params for request
		RequestParams params = new RequestParams();
		if(searchString != null) params.put("query", searchString);
		params.put("fill", "exercise,fitness_id,images,cal_factor");

		//Create Handler For response
		JsonHttpResponseHandler responseHandler = createResponseHandler(request, request.requestType);
		get(getRelativeUrl(request.requestType), params, responseHandler);
		
		return request;
	}
	
	/**
	 * Requests an Food object with given remoteId
	 * @category GET METHOD
	 * 
	 * @param remoteId id of exercise
	 * @throws JSONException
	 */
	public static CTWebserviceRequest getFood(String remoteId) {
		CTWebserviceRequest request = new CTWebserviceRequest();
		request.requestType = CTWebserviceRequestType.REQUEST_GET_FOOD;
		request.setMethodType(CTWebserviceRequestHTTPMethod.HTTPMETHOD_GET);
		
		//Add auth header
		addAuthorizationHeaderForRequest();
		
		//Create params for request
		RequestParams params = new RequestParams();
	    if(remoteId != null) params.put("foodId", remoteId);

		//Create Handler For response
		JsonHttpResponseHandler responseHandler = createResponseHandler(request, request.requestType);
		get(getRelativeUrl(request.requestType), params, responseHandler);
		
		return request;
	}
	/**
	 * Requests an Food object with given UPC Code
	 * @category GET METHOD
	 * 
	 *
	 * @throws JSONException
	 */
	public static CTWebserviceRequest getFoodFromUpc(String upcString, boolean isSynchronous) {
		CTWebserviceRequest request = new CTWebserviceRequest();
		request.requestType = CTWebserviceRequestType.REQUEST_GET_UPC;
		request.setMethodType(CTWebserviceRequestHTTPMethod.HTTPMETHOD_GET);
		
		//Add auth header
		addAuthorizationHeaderForRequest();
		
		//Create params for request
		RequestParams params = new RequestParams();
	    if(upcString != null) params.put("upc", upcString);

		//Create Handler For response
		JsonHttpResponseHandler responseHandler = createResponseHandler(request, request.requestType);
		get(getRelativeUrl(request.requestType), params, responseHandler);
		
		return request;
	}
	
	/**
	 * Requests an array of Food objects with given remoteIds
	 * @category GET METHOD
	 * 
	 * @param idsToFetch ids of exercises
	 * @throws JSONException
	 */
	public static CTWebserviceRequest getFoods(Set<String> idsToFetch){
	
		CTWebserviceRequest request = new CTWebserviceRequest();
		request.requestType = CTWebserviceRequestType.REQUEST_GET_FOOD;
		request.setMethodType(CTWebserviceRequestHTTPMethod.HTTPMETHOD_GET);
	
		//Add auth header
		addAuthorizationHeaderForRequest();
		String idsToFetchString = StringUtils.join(idsToFetch, ",");
		
		//Create params for request
		RequestParams params = new RequestParams();
		if(idsToFetchString != null) params.put("foodId", idsToFetchString);
		params.put("limit", "9999");

		//Create Handler For response
		JsonHttpResponseHandler responseHandler = createResponseHandler(request, request.requestType);
		get(getRelativeUrl(request.requestType), params, responseHandler);
		
		return request;
	}
	/**
	 * Requests an array of Food objects with given search string
	 * @category GET METHOD
	 * 
	 * @param searchString search exercises
	 * @throws JSONException
	 */
	public static CTWebserviceRequest getSearchFoods(String searchString){
	
		CTWebserviceRequest request = new CTWebserviceRequest();
		request.requestType = CTWebserviceRequestType.REQUEST_GET_SEARCH_FOOD;
		request.setMethodType(CTWebserviceRequestHTTPMethod.HTTPMETHOD_GET);
	
		//Add auth header
		addAuthorizationHeaderForRequest();
		
		//Create params for request
		RequestParams params = new RequestParams();
		if(searchString != null) params.put("query", searchString);
		params.put("fill", ",item_title,serving_size,food_id,images.100,images,verification,brand,cals,cholesterol,fat,carbs,protein,cals_from_fat,sat_fat,dietary_fiber,sugars,sodium,verified");

		//Create Handler For response
		JsonHttpResponseHandler responseHandler = createResponseHandler(request, request.requestType);
		get(getRelativeUrl(request.requestType), params, responseHandler);
		
		return request;
	}
	
	/**
	 * Requests the profile of the currently authenticated user
	 * @category GET METHOD
	 * 
	 * @throws JSONException
	 */
	public static CTWebserviceRequest getProfile(){
	
		CTWebserviceRequest request = new CTWebserviceRequest();
		request.requestType = CTWebserviceRequestType.REQUEST_GET_PROFILE;
		request.setMethodType(CTWebserviceRequestHTTPMethod.HTTPMETHOD_GET);
	
		//Add auth header
		addAuthorizationHeaderForRequest();
		
		//Create params for request
		RequestParams params = new RequestParams();
		params.put("fill", "dob,gender,activity_level,height,goal,mode,calories");

		//Create Handler For response
		JsonHttpResponseHandler responseHandler = createResponseHandler(request, request.requestType);
		get(getRelativeUrl(request.requestType), params, responseHandler);
		
		return request;
	}
	
	/**
	 * Requests the messages of the community
	 * @category GET METHOD
	 * 
	 * @throws JSONException
	 */
	public static CTWebserviceRequest getMessagesForPageNumber(int pageNumber, boolean shouldFilter){
	
		CTWebserviceRequest request = new CTWebserviceRequest();
		request.requestType = CTWebserviceRequestType.REQUEST_GET_MESSAGES;
		request.setMethodType(CTWebserviceRequestHTTPMethod.HTTPMETHOD_GET);
	
		//Add auth header
		addAuthorizationHeaderForRequest();
		
		//Create params for request
		RequestParams params = new RequestParams();
		params.put("page", Integer.toString(pageNumber));
		if(shouldFilter){
			params.put("filter", "my");
		}
		//Create Handler For response
		JsonHttpResponseHandler responseHandler = createResponseHandler(request, request.requestType);
		get(getRelativeUrl(request.requestType), params, responseHandler);

        request.storedObject = shouldFilter;
		return request;
	}
	/**
	 * Requests the Comments for the specified message thread id
	 * @category GET METHOD
	 * 
	 * @param messageId the message thread identifer
	 * @throws JSONException
	 */
	public static CTWebserviceRequest getCommentsForMessageId(String messageId, int pageNumber){
	
		CTWebserviceRequest request = new CTWebserviceRequest();
		request.requestType = CTWebserviceRequestType.REQUEST_GET_COMMENTS;
		request.setMethodType(CTWebserviceRequestHTTPMethod.HTTPMETHOD_GET);
	
		//Add auth header
		addAuthorizationHeaderForRequest();

        //Create params for request
        RequestParams params = new RequestParams();
        params.put("page", Integer.toString(pageNumber));

		//Create Handler For response
		JsonHttpResponseHandler responseHandler = createResponseHandler(request, request.requestType);
	    
		//Update relative path manually for this particular request
		String relativeUrl = getRelativeUrl(request.requestType) + messageId + "/";
		get(relativeUrl, params, responseHandler);
		
		return request;
	}
	
	/**
	 * Requests the Login Infomation by using a facebook iid
	 * @category GET METHOD
	 * 
	 * @param facebookId 
	 * @throws JSONException
	 */
	public static CTWebserviceRequest getFacebookLogin(String facebookId){
	
		CTWebserviceRequest request = new CTWebserviceRequest();
		request.requestType = CTWebserviceRequestType.REQUEST_GET_FACEBOOKLOGIN;
		request.setMethodType(CTWebserviceRequestHTTPMethod.HTTPMETHOD_GET);
	
		//Generate hash for security -not really secure, but whatev...
		String hashFormat = "6afb1fcf441f36-" + facebookId;
		
		//Convert string into UTF-8 data
		byte[] bytesOfMessage = null;
		try {
			bytesOfMessage = hashFormat.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String hashString = CTUtilities.dataToMd5(bytesOfMessage);
		
		//Add auth header
		addAuthorizationHeaderForRequest();
		
		//Create params for request
		RequestParams params = new RequestParams();
		params.put("hash",hashString);
		params.put("fbid",facebookId);

		//Create Handler For response
		JsonHttpResponseHandler responseHandler = createResponseHandler(request, request.requestType);
		get(getRelativeUrl(request.requestType), params, responseHandler);
		
		return request;
	}
	
	/**
	 * Requests the store products
	 * @category GET METHOD
	 * 
	 * @throws JSONException
	 */
	public static CTWebserviceRequest getStoreProducts(){
	
		CTWebserviceRequest request = new CTWebserviceRequest();
		request.requestType = CTWebserviceRequestType.REQUEST_GET_STORE_PRODUCTS;
		request.setMethodType(CTWebserviceRequestHTTPMethod.HTTPMETHOD_GET);

		//Add auth header
		addAuthorizationHeaderForRequest();
		
		//Create params for request
		RequestParams params = new RequestParams();
		//For some reason the appId is 2 for My Quit Coach...
		params.put("appId","1");

		//Create Handler For response
		JsonHttpResponseHandler responseHandler = createResponseHandler(request, request.requestType);
		get(getRelativeUrl(request.requestType), params, responseHandler);
		
		return request;
	}
	
	/**
	 * Requests product's with given sku availability access
	 * @category GET METHOD
	 * 
	 * @param sku 
	 * @throws JSONException
	 */
	public static CTWebserviceRequest getStoreProductsAccess(String sku){
	
		CTWebserviceRequest request = new CTWebserviceRequest();
		request.requestType = CTWebserviceRequestType.REQUEST_GET_STORE_CHECK_ACCESS_TO_PRODUCTS;
		request.setMethodType(CTWebserviceRequestHTTPMethod.HTTPMETHOD_GET);

		//Add auth header
		addAuthorizationHeaderForRequest();
		
		//Create params for request
		RequestParams params = new RequestParams();
		//For some reason the appId is 2 for My Quit Coach...
		params.put("SKU",sku);

		//Create Handler For response
		JsonHttpResponseHandler responseHandler = createResponseHandler(request, request.requestType);
		get(getRelativeUrl(request.requestType), params, responseHandler);
		
		return request;
	}
	
	//======================
	//	Post Items
	//======================

	
	/**
	 * Sign up for new account with given parameters
	 * @category POST METHOD
	 * 
	 * @param username
	 * @param gender
	 * @param email
	 * @param password
	 * @param fbId
	 *  
	 * @throws JSONException
	 */
	public static CTWebserviceRequest postSignup(String username, String gender, String email, String password, String fbId){
	
		CTWebserviceRequest request = new CTWebserviceRequest();
		request.requestType = CTWebserviceRequestType.REQUEST_POST_SIGNUP;
		request.setMethodType(CTWebserviceRequestHTTPMethod.HTTPMETHOD_POST);

		//Convert string into UTF-8 data
	    String hashFormat = "079290fce20496-"+ username + "-" + email + "-" + (fbId != null ? fbId : "");
	    byte[] bytesOfMessage = null;
	    try {
	    	bytesOfMessage = hashFormat.getBytes("UTF-8");
	    } catch (UnsupportedEncodingException e) {
	    	// TODO Auto-generated catch block
	    	e.printStackTrace();
	    }
		String hashString = CTUtilities.dataToMd5(bytesOfMessage);

		//Add auth header
		addAuthorizationHeaderForRequest();
		
		JSONObject user = new JSONObject();
		try{
		user.put("username", username);
		user.put("email", email);
		user.put("gender", gender.toLowerCase(Locale.getDefault()));
		user.put("password", password);
		if (fbId != null) user.put("fbid", true);
		user.put("toc_agree", "1");
		user.put("newsletter", "1");
		user.put("source", "CalorieTracker" + "v.1 Android");
		user.put("hash", hashString);
		}
		catch (JSONException e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
		//Create params for request
		RequestParams params = new RequestParams();
		
		//For some reason the appId is 2 for My Quit Coach...
		params.put("registration_data",user.toString());

		request.requestParams = params;
		//Create Handler For response
		JsonHttpResponseHandler responseHandler = createResponseHandler(request, request.requestType);
		postFormData(getRelativeUrl(request.requestType), params, responseHandler);
		
		return request;
	}
	/**
	 * Sign in with given parameters
	 * @category POST METHOD
	 * 
	 * @param username
	 * @param password
	 *  
	 * @throws JSONException
	 */
	public static CTWebserviceRequest login(String username, String password){

		CTWebserviceRequest request = new CTWebserviceRequest();
		request.requestType = CTWebserviceRequestType.REQUEST_POST_LOGIN;
		request.setMethodType(CTWebserviceRequestHTTPMethod.HTTPMETHOD_POST);

		//Create Auth Header for validation - not set in prefs until verification
		UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(username,password);
		Header header = BasicScheme.authenticate(credentials, "UTF-8", false);
		client.addHeader(header.getName(), header.getValue());
				//Create Handler For response
		JsonHttpResponseHandler responseHandler = createResponseHandler(request, request.requestType);
		postFormData(getRelativeUrl(request.requestType), null, responseHandler);
		
		return request;
	}
	
	/**
	 * Post diary info to the server
	 * @category POST METHOD
	 * 
	 * @param diary
	 *  
	 * @throws JSONException
	 */
	public static CTWebserviceRequest postDiary(JSONObject diary, String sinceDate){
	
		CTWebserviceRequest request = new CTWebserviceRequest();
		request.requestType = CTWebserviceRequestType.REQUEST_POST_DIARY;
		request.setMethodType(CTWebserviceRequestHTTPMethod.HTTPMETHOD_POST);

		//Create Auth Header for validation - not set in prefs until verification
		addAuthorizationHeaderForRequest();
					
		//Create Handler For response
		AsyncHttpResponseHandler responseHandler = createPlainResponseHandler(request, request.requestType);

        String relativeUrl = null;
        try {
            relativeUrl = getRelativeUrl(request.requestType) + "?since=" + URLEncoder.encode(sinceDate, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            relativeUrl = getRelativeUrl(request.requestType) + "?since=" +sinceDate;
        }
        postJsonData(diary, relativeUrl, responseHandler);
		return request;
	}
	
	/**
	 * Post User profile info to the server
	 * @category POST METHOD
	 * 
	 * @param userProfile
	 *  
	 * @throws JSONException
	 */
	public static CTWebserviceRequest postProfile(JSONObject userProfile){
	
		CTWebserviceRequest request = new CTWebserviceRequest();
		request.requestType = CTWebserviceRequestType.REQUEST_POST_PROFILE;
		request.setMethodType(CTWebserviceRequestHTTPMethod.HTTPMETHOD_POST);

		//Create Auth Header for validation - not set in prefs until verification
		addAuthorizationHeaderForRequest();
					
		//Create Handler For response
        AsyncHttpResponseHandler responseHandler = createPlainResponseHandler(request, request.requestType);
		postJsonData(userProfile, getRelativeUrl(request.requestType), responseHandler);
		return request;
	}
	
	/**
	 * Post Message to the community
	 * @category POST METHOD
	 * 
	 * @param message
	 *  
	 * @throws JSONException
	 */
	public static CTWebserviceRequest postMessage(String message){
	
		CTWebserviceRequest request = new CTWebserviceRequest();
		request.requestType = CTWebserviceRequestType.REQUEST_POST_MESSAGE;
		request.setMethodType(CTWebserviceRequestHTTPMethod.HTTPMETHOD_POST);

		//Create Auth Header for validation - not set in prefs until verification
		addAuthorizationHeaderForRequest();
			
		//Create params for request
		RequestParams params = new RequestParams();
		if(message != null) params.put("post",message);
		params.put("anonymous","0");
		params.put("post",message);
		
		//Create Handler For response
		JsonHttpResponseHandler responseHandler = createResponseHandler(request, request.requestType);
		postFormData(getRelativeUrl(request.requestType), params, responseHandler);
		return request;
	}
	
	/**
	 * Post Comment to the message thread
	 * @category POST METHOD
	 * 
	 * @param comment
	 * @param messageThreadId
	 * @throws JSONException
	 */
	public static CTWebserviceRequest postComment(String comment, String messageThreadId){
	
		CTWebserviceRequest request = new CTWebserviceRequest();
		request.requestType = CTWebserviceRequestType.REQUEST_POST_COMMENT;
		request.setMethodType(CTWebserviceRequestHTTPMethod.HTTPMETHOD_POST);

		//Create Auth Header for validation - not set in prefs until verification
		addAuthorizationHeaderForRequest();
			
		//Create params for request
		RequestParams params = new RequestParams();
		if(comment != null) params.put("post",comment);

        //Create and update path
        String relativeUrl = getRelativeUrl(request.requestType) + messageThreadId + "/";

        //Create Handler For response
		JsonHttpResponseHandler responseHandler = createResponseHandler(request, request.requestType);
		postFormData(relativeUrl, params, responseHandler);
		return request;
	}

	/**
	 * Post receipt data server from in app purchase
	 * @category POST METHOD
	 * 
	 * @param receipt
	 * @throws JSONException
	 */
	public static CTWebserviceRequest postReceiptData(String receipt){
	
		CTWebserviceRequest request = new CTWebserviceRequest();
		request.requestType = CTWebserviceRequestType.REQUEST_POST_STORE_PAYMENT_RECEIPT;
		request.setMethodType(CTWebserviceRequestHTTPMethod.HTTPMETHOD_POST);

		//Create Auth Header for validation - not set in prefs until verification
		addAuthorizationHeaderForRequest();
			
		//Create params for request
		RequestParams params = new RequestParams();
		params.put("post",receipt);
				
		//Create Handler For response
		JsonHttpResponseHandler responseHandler = createResponseHandler(request, request.requestType);
		postFormData(getRelativeUrl(request.requestType), params, responseHandler);
		return request;
	}

}
