package com.livestrong.calorietracker.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.livestrong.calorietracker.R;
import com.livestrong.calorietracker.MyPlateApplication;
import com.livestrong.calorietracker.back.api.CTWebserviceRequest;
import com.livestrong.calorietracker.back.db.DataHelperDelegate;
import com.livestrong.calorietracker.back.db.LiveStrongDisplayableListItem;
import com.livestrong.calorietracker.back.db.ModelManager;
import com.livestrong.calorietracker.back.db.gen.DiaryEntry.DiaryEntryCategoryEnum;
import com.livestrong.calorietracker.back.db.gen.Food;
import com.livestrong.calorietracker.back.db.sync.ModelSyncCompletedDelegate;
import com.livestrong.calorietracker.utilities.ImageLoader;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;


public class FoodSelectorAdapter extends BaseAdapter implements ModelSyncCompletedDelegate,DataHelperDelegate {


    private Activity activity;
	private List<LiveStrongDisplayableListItem> listItems = new ArrayList<LiveStrongDisplayableListItem>();
	private ImageLoader imageLoader;
	private DiaryEntryCategoryEnum timeOfDay;
	private Boolean isLoading = false;
	private Boolean showSearchOnlinePromp = false;
	public Boolean showNoResultsMessage = false;
	private String mSearchString;
	public FoodSelectorAdapter(Activity activity, DiaryEntryCategoryEnum timeOfDay) {
		this.activity = activity;
		this.imageLoader = new ImageLoader(this.activity);
		this.timeOfDay = timeOfDay;
		
		loadRecentlyEaten();
	}
	
	/**** Load list data functions ****/
	
	public void loadRecentlyEaten(){
        this.isLoading = true;
        this.showSearchOnlinePromp = false;
		this.showNoResultsMessage = false;
        this.setListItems(null);
        ModelManager.getRecentFoods(this.timeOfDay,this);
	}
	
	public void loadFrequentlyEaten(){
        this.isLoading = true;
        this.showSearchOnlinePromp = false;
		this.showNoResultsMessage = false;
        this.setListItems(null);
        ModelManager.getFavoriteFoods(this.timeOfDay, this);
	}
	
	public void loadMyMeals(){
        this.isLoading = true;
        this.showSearchOnlinePromp = false;
		this.showNoResultsMessage = false;
        this.setListItems(null);
        ModelManager.getMeals(this);
	}
	
	public void loadCustomFoods(){
        this.isLoading = true;
        this.showSearchOnlinePromp = false;
		this.showNoResultsMessage = false;
        this.setListItems(null);
        ModelManager.getCustomFoods(this);
	}
	
	public void loadFoodFromLocalSearch(String searchStr){
        mSearchString = searchStr;
        this.isLoading = true;
        this.showSearchOnlinePromp = true;
		this.showNoResultsMessage = false;
        this.setListItems(null);
        ModelManager.getFoodsLike(searchStr,this);
    }
	
	public void loadFoodFromServerSearch(String searchStr){
        mSearchString = searchStr;
		this.showSearchOnlinePromp = false;
		this.isLoading = true;
		this.setListItems(null);
        ModelManager.sharedModelManager().getFoodsLikeFromServer(this,searchStr);

    }

	@Override
	public int getCount() {
		if (this.isLoading){
			return 1;
		}
		
		int count = 0;
		if (this.listItems != null){
			count = this.listItems.size();
		}
		
		if (this.showSearchOnlinePromp ){
			count++;
		}
				
		return count;
	}

	@Override
	public Object getItem(int position) {
		if (this.isShowingSearchOnlinePrompt() && position >= this.listItems.size()){
			return null;
		}
		return (this.listItems == null) ? 0 : this.listItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Context context = MyPlateApplication.getContext();
		
		// show custom views (loading, search prompt)
		if (this.isLoading){
			convertView = LayoutInflater.from(context).inflate(R.layout.list_item_loading, null);
			return convertView;
		} else if (this.showSearchOnlinePromp && position == this.listItems.size()){
			convertView = LayoutInflater.from(context).inflate(R.layout.list_item_message, null);
			TextView message = (TextView) convertView.findViewById(R.id.messageTextView);
			if (CTWebserviceRequest.isOnline(MyPlateApplication.getContext())){
				message.setText("Tap to search online...");
			} else {
				message.setText("Offline - only previously tracked items available.");
			}			
			return convertView;
		} else if (this.showNoResultsMessage && position == this.listItems.size()){
			convertView = LayoutInflater.from(context).inflate(R.layout.list_item_message, null);
			((TextView)convertView.findViewById(R.id.messageTextView)).setText("No Results Found.");
			return convertView;
		}
		
		if (convertView == null || convertView.getId() != R.id.listItemFood) {
			convertView = LayoutInflater.from(context).inflate(R.layout.list_item_food, null);			
		}
		
		// Retrieve food at position
		LiveStrongDisplayableListItem item = (LiveStrongDisplayableListItem)getItem(position);
		if (item != null){
			TextView foodNameTextField = (TextView)convertView.findViewById(R.id.foodNameTextView);
			foodNameTextField.setText(item.getTitle());
			
			TextView foodDescriptionTextField = (TextView)convertView.findViewById(R.id.foodDescriptionTextView);
			foodDescriptionTextField.setText(item.getDescription());
			
			ImageView imageView = (ImageView)convertView.findViewById(R.id.foodImageView);
			if (timeOfDay == DiaryEntryCategoryEnum.CTDiaryEntryTypeBreakfast){
				imageView.setImageResource(R.drawable.icon_breakfast);
			} else if (timeOfDay == DiaryEntryCategoryEnum.CTDiaryEntryTypeLunch){
				imageView.setImageResource(R.drawable.icon_lunch);
			} else if (timeOfDay == DiaryEntryCategoryEnum.CTDiaryEntryTypeDinner){
				imageView.setImageResource(R.drawable.icon_dinner);
			} else {
				imageView.setImageResource(R.drawable.icon_snacks);
			}
			
			imageView.setTag(item.getSmallImage());
			this.imageLoader.DisplayImage(item.getSmallImage(), imageView);			
		}
		
		return convertView;
	}
	
	@SuppressWarnings("unchecked")
	public void setListItems(List<?> listItems){
		this.listItems = (List<LiveStrongDisplayableListItem>) listItems;
		notifyDataSetChanged();
	}
	
	public void setTimeOfDay(DiaryEntryCategoryEnum timeOfDay){
		this.timeOfDay = timeOfDay;
	}
	
	public Boolean isShowingSearchOnlinePrompt(){
		return this.showSearchOnlinePromp;
	}
	
	public Boolean isLoading(){
		return this.isLoading;
	}

    @Override
    public void finishedSyncThreaded(ModelSyncType syncType, Object data) {
        this.isLoading = false;
        this.showNoResultsMessage = false;
        this.showSearchOnlinePromp = false;

        List<Food> likeFoods = (List<Food>)data;
        this.setListItems(likeFoods);
        if (likeFoods.size() == 0){
            this.showNoResultsMessage = true;
        }
        this.notifyDataSetChanged();

    }

    @Override
    public boolean errorOccurredThreaded(ModelSyncType syncType, Exception error, String errorMessage) {
        this.isLoading = false;
        this.notifyDataSetChanged();

        return false;
    }

    @Override
    public void dataReceivedThreaded(Method methodCalled, Object data) {
        this.isLoading = false;
        this.showSearchOnlinePromp = false;
        this.showNoResultsMessage = false;
        List<LiveStrongDisplayableListItem> foods = (List<LiveStrongDisplayableListItem>)data;
        if (foods.size() == 0){
            if(this.mSearchString != null && this.mSearchString.length() > 0){
                this.showSearchOnlinePromp = true;
            }
            else{
                this.showNoResultsMessage = true;
            }

        }
        this.setListItems(foods);
    }

    @Override
    public boolean errorOccurredThreaded(Method methodCalled, Exception error, String errorMessage) {
        this.isLoading = false;
        this.setListItems(new ArrayList<LiveStrongDisplayableListItem>());
        this.notifyDataSetChanged();

        return false;
    }
}
