package com.livestrong.calorietracker.adapters;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.livestrong.calorietracker.MyPlateApplication;
import com.livestrong.calorietracker.R;
import com.livestrong.calorietracker.back.api.CTWebserviceRequest;
import com.livestrong.calorietracker.back.db.gen.CommunityMessage;
import com.livestrong.calorietracker.utilities.EndlessScrollDelegate;
import com.livestrong.calorietracker.utilities.EndlessScrollListener;
import com.livestrong.calorietracker.utilities.ImageLoader;
import com.livestrong.calorietracker.utilities.JSONSafeObject;
import com.livestrong.calorietracker.utilities.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class CommunityCommentsAdapter extends BaseAdapter implements EndlessScrollDelegate,CTWebserviceRequest.CTWebserviceRequestDelegate {

	private ListView listView;
	private List<CommunityMessage> messages = new ArrayList<CommunityMessage>();
	public ImageLoader imageLoader;
	private Boolean showLoadingItem = true;
	private CommunityMessage message;
	private Boolean loadingNewPost = false;
    private Activity mActivity;
	int messageId;

	public CommunityCommentsAdapter(Activity activity, ListView listView, CommunityMessage message) {
		this.mActivity = activity;
		this.listView = listView;
		this.message = message;
		this.messageId = message.getPostId();
		this.imageLoader = new ImageLoader(activity);
	}

	/**** DataHelper Calls/Functions ****/

	public void loadComments(int page) {
		if (this.messages.size() >= this.message.getNumberOfComments()){
			this.showLoadingItem = false;
			this.notifyDataSetChanged();
		} else {
			Log.d("D", "loadPage " + page);
           CTWebserviceRequest request =  CTWebserviceRequest.getCommentsForMessageId(this.messageId+"", page);
            request.delegate = this;
		}
	}

	public void reloadComments() {
		Log.d("D", "reload");
		//this.loadingNewPost = true;
		this.messages.clear();
		setShowLoadingItem(true);
		this.notifyDataSetChanged();

        CTWebserviceRequest request =  CTWebserviceRequest.getCommentsForMessageId(this.messageId+"", 1);
        request.delegate = this;
	}



	/**
	 *  EndlessScrollListener callback
	 */
	@Override
	public void loadListDataForPage(int page) {
		this.loadComments(page);
	}

	@Override
	public int getCount() {
		int count = 0;
		if (this.messages != null) {
			count = this.messages.size();
		}
		if (this.showLoadingItem) {
			count = count + 1;
		}
		
		count++; // To include original message
		
		return count;
	}

	@Override
	public Object getItem(int position) {
		if (position == 0){
			return this.message;
		} else {
			return (this.messages.size() == 0) ? 0 : this.messages.get(position - 1);	
		}
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Context context = MyPlateApplication.getContext();

		// Return loading cell is last item
		if (this.messages == null || position == this.messages.size() + 1) {
			View view = LayoutInflater.from(context).inflate(R.layout.list_item_loading, null);
			return view;
		}

		if (convertView == null || convertView.getId() == R.id.listItemLoading) {
			convertView = LayoutInflater.from(context).inflate(R.layout.list_item_community, null);
			convertView.setBackgroundResource(R.drawable.list_item_background);
		}

		TextView userNameTextField = (TextView) convertView.findViewById(R.id.userNameTextView);
		TextView messageTextField = (TextView) convertView.findViewById(R.id.messageTextView);
		TextView dateTextView = (TextView) convertView.findViewById(R.id.dateTextView);
		TextView commentsTextView = (TextView) convertView.findViewById(R.id.commentsLabelTextView);
		TextView numCommentsTextView = (TextView) convertView.findViewById(R.id.numCommentsTextView);
		ImageView imageView = (ImageView) convertView.findViewById(R.id.imageView);
		
		if (this.message == null){
			return convertView;
		}
			
		if (position == 0){
			userNameTextField.setText(this.message.getAuthor());
			messageTextField.setText(this.message.getPost());
			dateTextView.setText(Utils.getPrettyDate(this.message.getDateCreated()));
			numCommentsTextView.setText(this.message.getNumberOfComments()+"");
			commentsTextView.setVisibility(View.VISIBLE);
			numCommentsTextView.setVisibility(View.VISIBLE);
			this.imageLoader.DisplayImage(this.message.getThumbnailURL(), imageView);
		} else {
			// Retrieve food at position
            CommunityMessage message = (CommunityMessage) getItem(position);
			userNameTextField.setText(message.getAuthor());
			messageTextField.setText(message.getPost());
            dateTextView.setText(Utils.getPrettyDate(message.getDateCreated()));
			commentsTextView.setVisibility(View.INVISIBLE);
			numCommentsTextView.setVisibility(View.INVISIBLE);
            this.imageLoader.DisplayImage(message.getThumbnailURL(), imageView);
		}	

		return convertView;
	}

	private void addMessages(List<CommunityMessage> messages) {
		this.messages.addAll(messages);
		this.notifyDataSetChanged();
	}
	
	private void addMessage(CommunityMessage message){
		this.messages.add(message);
		this.notifyDataSetChanged();
	}

	public List<CommunityMessage> getMessages() {
		if (this.messages == null) {
			return new ArrayList<CommunityMessage>();
		}

		return this.messages;
	}

	public void setShowLoadingItem(Boolean show) {
		this.showLoadingItem = show;
	}

    @Override
    public void dataReceived(CTWebserviceRequest request, CTWebserviceRequest.CTWebserviceRequestType methodCalled, Object data) {
        if (data != null) {
            // Initialize EndlessScroll Listener if list is empty
            boolean shouldAddOnScrollListener = false;
            if (this.messages.size() == 0) {
                shouldAddOnScrollListener = true;
            }

            List<CommunityMessage> newComments = new ArrayList<CommunityMessage>();
            Log.d("D", "num comments: " + newComments.size());
            if (((JSONObject)data) != null) {
                try {
                    List<CommunityMessage> unsortedMessages = new ArrayList<CommunityMessage>();

                    JSONArray newMessages = ((JSONObject)data).getJSONArray("comments");
                    for (int i = 0; i < newMessages.length(); i++){
                        JSONObject JSONMessage = newMessages.getJSONObject(i);
                        JSONObject user = JSONMessage.getJSONObject("user");
                        JSONObject avatars = user.getJSONObject("avatars");

                        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

                        CommunityMessage communityMessage = new CommunityMessage();
                        communityMessage.setNumberOfComments(JSONSafeObject.safeGetInt("comments", JSONMessage));
                        communityMessage.setPostId(JSONSafeObject.safeGetInt("dare_post_id", JSONMessage));
                        communityMessage.setPost(JSONSafeObject.safeGetString("post", JSONMessage));
                        communityMessage.setDateCreated(dateFormatter.parse(JSONSafeObject.safeGetString("date_created", JSONMessage)));
                        communityMessage.setAuthor(JSONSafeObject.safeGetString("username", user));
                        communityMessage.setThumbnailURL(JSONSafeObject.safeGetString("medium", avatars));
                        unsortedMessages.add(communityMessage);
                    }

//                    Collections.sort(unsortedMessages, new Comparator<CommunityMessage>() {
//                        @Override
//                        public int compare(CommunityMessage communityMessage, CommunityMessage communityMessage2) {
//                            int compared = communityMessage.getDateCreated().compareTo(communityMessage2.getDateCreated());
//                            return -compared;
//                        }
//                    });

                     newComments.addAll(unsortedMessages);


                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
                catch (ParseException e) {
                    e.printStackTrace();
                }

            }
            if (newComments.size() > 0) {
                this.addMessages(newComments);
                if (shouldAddOnScrollListener) {
                    this.listView.setOnScrollListener(new EndlessScrollListener(2, this));
                }
            } else {
                this.setShowLoadingItem(false);
                this.listView.setOnScrollListener(null);
                this.notifyDataSetChanged();
            }

//			if (this.loadingNewPost){
//				this.listView.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
//				this.loadingNewPost = false;
//				this.notifyDataSetChanged();
//			}

        } else {
            this.setShowLoadingItem(false);
            this.listView.setOnScrollListener(null);
        }
    }

    @Override
    public boolean errorOccurred(CTWebserviceRequest request, CTWebserviceRequest.CTWebserviceRequestType methodCalled, Exception error, String errorMessage) {
        return false;
    }
}
