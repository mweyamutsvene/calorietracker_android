package com.livestrong.calorietracker.adapters;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.livestrong.calorietracker.MyPlateApplication;
import com.livestrong.calorietracker.R;
import com.livestrong.calorietracker.back.api.CTWebserviceRequest;
import com.livestrong.calorietracker.back.api.CTWebserviceRequest.CTWebserviceRequestDelegate;
import com.livestrong.calorietracker.back.db.gen.CommunityMessage;
import com.livestrong.calorietracker.utilities.EndlessScrollDelegate;
import com.livestrong.calorietracker.utilities.EndlessScrollListener;
import com.livestrong.calorietracker.utilities.ImageLoader;
import com.livestrong.calorietracker.utilities.JSONSafeObject;
import com.livestrong.calorietracker.utilities.PullToRefreshListView;
import com.livestrong.calorietracker.utilities.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class CommunityListAdapter extends BaseAdapter implements EndlessScrollDelegate,CTWebserviceRequestDelegate {



    private static enum ListType {COMMUNITY_MESSAGES, USER_MESSAGES};
	
	private ListType selectedList;
	private ListView listView;
	private Map<ListType, List<CommunityMessage>> messageMap = new Hashtable<ListType, List<CommunityMessage>>();
	public ImageLoader imageLoader;
	private Boolean showLoadingItem = true;
	private Boolean showMessageItem = false;
	private Boolean reloading = false;
	private Map<ListType, Integer> currentPageMap = new Hashtable<ListType, Integer>();
	private Map<ListType, Integer> maxPageMap = new Hashtable<ListType, Integer>();
	private Activity mActivity;

	public CommunityListAdapter(Activity activity, ListView listView) {
		this.mActivity = activity;

		this.listView = listView;
		this.imageLoader = new ImageLoader(activity);
		
		this.selectedList = ListType.COMMUNITY_MESSAGES;
		
		this.messageMap.put(ListType.COMMUNITY_MESSAGES, new ArrayList<CommunityMessage>());
		this.currentPageMap.put(ListType.COMMUNITY_MESSAGES, 0);
		this.maxPageMap.put(ListType.COMMUNITY_MESSAGES, 6);
		
		this.messageMap.put(ListType.USER_MESSAGES, new ArrayList<CommunityMessage>());
		this.currentPageMap.put(ListType.USER_MESSAGES, 0);
		this.maxPageMap.put(ListType.USER_MESSAGES, 6);
	}
    @Override
    public void dataReceived(CTWebserviceRequest request, CTWebserviceRequest.CTWebserviceRequestType methodCalled, Object data) {
        if (this.selectedList == ListType.COMMUNITY_MESSAGES  && (Boolean)request.storedObject == true){
            return;
        }
        if (this.selectedList == ListType.USER_MESSAGES &&  (Boolean)request.storedObject == false){
            return;
        }

        if (reloading){ // User is reloading via pull to refresh
            if (data != null){ // empty the list if we received data
                this.resetList();
            } else if (CTWebserviceRequest.isOnline(MyPlateApplication.getContext()) == false){ // if we are offline show offline message
                this.resetList();
                this.showMessageItem = true;
            }
        }

        List<CommunityMessage> messages = this.messageMap.get(this.selectedList); // Fetch stored messages
		int currentPage = this.currentPageMap.get(this.selectedList); // Fetch current page

        if (messages.size() == 0 && currentPage == 0) { // Initialize EndlessScroll Listener if list is empty
            this.listView.setOnScrollListener(new EndlessScrollListener(2, this));
        }
        currentPage++;

        if (((JSONObject)data) != null) {
            try {
                List<CommunityMessage> unsortedMessages = new ArrayList<CommunityMessage>();

                JSONArray newMessages = ((JSONObject)data).getJSONArray("posts");
                for (int i = 0; i < newMessages.length(); i++){
                    JSONObject JSONMessage = newMessages.getJSONObject(i);
                    JSONObject user = JSONMessage.getJSONObject("user");
                    JSONObject avatars = user.getJSONObject("avatars");

                    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

                    CommunityMessage communityMessage = new CommunityMessage();
                    communityMessage.setNumberOfComments(JSONSafeObject.safeGetInt("comments", JSONMessage));
                    communityMessage.setPostId(JSONSafeObject.safeGetInt("dare_post_id", JSONMessage));
                    communityMessage.setPost(JSONSafeObject.safeGetString("post", JSONMessage));
                    communityMessage.setDateCreated(dateFormatter.parse(JSONSafeObject.safeGetString("date_created", JSONMessage)));
                    communityMessage.setAuthor(JSONSafeObject.safeGetString("username", user));
                    communityMessage.setThumbnailURL(JSONSafeObject.safeGetString("medium", avatars));
                    unsortedMessages.add(communityMessage);
                }

//                Collections.sort(unsortedMessages, new Comparator<CommunityMessage>() {
//                    @Override
//                    public int compare(CommunityMessage communityMessage, CommunityMessage communityMessage2) {
//                        int compared = communityMessage.getDateCreated().compareTo(communityMessage2.getDateCreated());
//                        return -compared;
//                    }
//                });

                if(newMessages.length() > 0){
                    messages.addAll(unsortedMessages);
                }
                else{
                    this.setShowLoadingItem(false);
                    this.listView.setOnScrollListener(null);
                }

            }
            catch (JSONException e) {
                e.printStackTrace();
            }
            catch (ParseException e) {
                e.printStackTrace();
            }
        }



        // If user reached the max number of pages
        if (currentPage == this.maxPageMap.get(this.selectedList) || data == null){
            this.setShowLoadingItem(false);
            this.listView.setOnScrollListener(null);
        }

        // If there are no messages in selectedList and device is offline
        if (messages.size() == 0 /*&& ApiHelper.isOnline() == false*/){
            this.showMessageItem = true;
        } else {
            this.showMessageItem = false;
        }

        // Update member variables
        this.messageMap.put(this.selectedList, messages);
        this.currentPageMap.put(this.selectedList, currentPage);

        if (reloading){ // notify that reload was completed (for pull to refresh)
            ((PullToRefreshListView)this.listView).onRefreshComplete();
            reloading = false;
        }

        this.notifyDataSetChanged();

    }

    @Override
    public boolean errorOccurred(CTWebserviceRequest request, CTWebserviceRequest.CTWebserviceRequestType methodCalled, Exception error, String errorMessage) {
        return false;
    }
	/**** DataHelper Calls/Functions ****/
	
	public void reloadCommunityMessages() {
		this.selectedList = ListType.COMMUNITY_MESSAGES;
		this.reloading = true;
		Log.d("D", "COMMUNITY - reloadPage 1");
        CTWebserviceRequest request = CTWebserviceRequest.getMessagesForPageNumber(1,false);
        request.delegate = this;
	}

	public void reloadUserMessages(){
		this.selectedList = ListType.USER_MESSAGES;
		
		this.reloading = true;
		Log.d("D", "USER - reloadPage 1");
        CTWebserviceRequest request = CTWebserviceRequest.getMessagesForPageNumber(1,true);
        request.delegate= this;

    }
	
	public void loadCommunityMessages(int page){
		if (this.selectedList != ListType.COMMUNITY_MESSAGES){
			this.selectedList = ListType.COMMUNITY_MESSAGES;
			((PullToRefreshListView) this.listView).onRefreshComplete();
			this.notifyDataSetChanged();
		}
		
		// Check if online
		if (CTWebserviceRequest.isOnline(MyPlateApplication.getContext()) == false && this.messageMap.get(this.selectedList).size() == 0){
			this.showMessageItem = true;
			this.showLoadingItem = false;
			notifyDataSetChanged();
			return;
		}
		
		this.showMessageItem = false;
		
		// if data has already been downloaded, show it
		if (page < this.currentPageMap.get(this.selectedList)){
			return;
		} else {
			this.showLoadingItem = true;
            CTWebserviceRequest request = CTWebserviceRequest.getMessagesForPageNumber(page,false);
            request.delegate= this;
		}	
		notifyDataSetChanged();
	}

	public void loadUserMessages(int page){
		if (this.selectedList != ListType.USER_MESSAGES){
			this.selectedList = ListType.USER_MESSAGES;
			((PullToRefreshListView)this.listView).onRefreshComplete();
			this.notifyDataSetChanged();
		}

		// Check if logged in
		if (CTWebserviceRequest.isLoggedIn() == false){
			this.resetList();
			this.showMessageItem = true;
			this.showLoadingItem = false;
			notifyDataSetChanged();
			return;
		}
		
		// Check if online and there are no messages already loaded show offline message
		if (CTWebserviceRequest.isOnline(MyPlateApplication.getContext()) == false && this.messageMap.get(this.selectedList).size() == 0){
			this.showMessageItem = true;
			this.showLoadingItem = false;
			notifyDataSetChanged();
			return;
		}
		
		this.showMessageItem = false;
		
		// if data has already been downloaded, show it
		if (this.messageMap.get(this.selectedList).size() > 0 && page < this.currentPageMap.get(this.selectedList)){
			notifyDataSetChanged();
		}
        else {
			this.showLoadingItem = true;
			notifyDataSetChanged();
            CTWebserviceRequest request = CTWebserviceRequest.getMessagesForPageNumber(page,true);
            request.delegate= this;

		}
	}
	


	/**
	 *  EndlessScrollListener callback
	 */	
	@Override
	public void loadListDataForPage(int page) {
		Log.d("CommunityListAdapter", "loadListDataForPage: "+ page);
		if (page <= this.maxPageMap.get(this.selectedList)){
			if (this.selectedList == ListType.COMMUNITY_MESSAGES){
				Log.d("D", "Community - loadListDataForPage: " + page);
				this.loadCommunityMessages(page);
			} else if (this.selectedList == ListType.USER_MESSAGES){
				Log.d("D", "USER - loadListDataForPage: " + page);
				this.loadUserMessages(page);
			}
		} 
	}
	
	@Override
	public int getCount() {
		if ((showMessageItem && CTWebserviceRequest.isLoggedIn() == false) ||
				(showMessageItem && CTWebserviceRequest.isOnline(MyPlateApplication.getContext()) == false) ||
				(showMessageItem && this.messageMap.get(this.selectedList).size() == 0)){
			return 1;
		}
		
		int count = this.messageMap.get(this.selectedList).size();
				
		if (showLoadingItem){
			count++;
		}
				
		return count;
	}

	@Override
	public CommunityMessage getItem(int position) {
		if (this.showMessageItem){
			return null;
		}
		
		if (this.showLoadingItem && position == this.messageMap.get(this.selectedList).size()){
			return null;
		}
		
		return (this.messageMap.get(this.selectedList) == null) ? null : this.messageMap.get(this.selectedList).get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Context context = MyPlateApplication.getContext();
		
		// Return loading cell is last item
		if (this.messageMap.get(this.selectedList) == null || position == this.messageMap.get(this.selectedList).size()){
			if (this.showLoadingItem){
				View view = LayoutInflater.from(context).inflate(R.layout.list_item_loading, null);
				return view;
			}
		}
				
		// show message cell 
		if (showMessageItem){
			convertView = LayoutInflater.from(context).inflate(R.layout.list_item_message, null);
			TextView messageTextView = (TextView) convertView.findViewById(R.id.messageTextView);
			if (CTWebserviceRequest.isLoggedIn() == false){
				messageTextView.setText("You must be signed in to view your messages.");
			} else if (CTWebserviceRequest.isOnline(MyPlateApplication.getContext()) == false){
				messageTextView.setText("Offline - cannot retrieve messages.");
			} else if (this.messageMap.get(this.selectedList).size() == 0){
				messageTextView.setText("You have no messages.");
			}
			return convertView;
		}
		
		// Inflate community list item
		if (convertView == null || convertView.getId() == R.id.listItemLoading || convertView.getId() == R.id.listItemMessage) {
			convertView = LayoutInflater.from(context).inflate(R.layout.list_item_community, null);			
		}
		
		// Retrieve food at position	
		CommunityMessage message = getItem(position);
		if (message != null){
			TextView userNameTextField = (TextView)convertView.findViewById(R.id.userNameTextView);
			userNameTextField.setText(message.getAuthor());
			
			TextView messageTextField = (TextView)convertView.findViewById(R.id.messageTextView);
			messageTextField.setText(message.getPost());
			
			TextView numberOfCommentsTextView = (TextView)convertView.findViewById(R.id.numCommentsTextView);
			numberOfCommentsTextView.setText(message.getNumberOfComments() + "");
			
			TextView dateTextView = (TextView)convertView.findViewById(R.id.dateTextView);
			dateTextView.setText(Utils.getPrettyDate(message.getDateCreated()));
			
			ImageView imageView = (ImageView)convertView.findViewById(R.id.imageView);
			imageView.setTag(message.getThumbnailURL());
			
			this.imageLoader.DisplayImage(message.getThumbnailURL(), imageView);
		}
		
		return convertView;
	}
	
	private void resetList(){
		this.clearMessages();
		this.listView.setOnScrollListener(null);
		this.currentPageMap.put(this.selectedList, 0);
		//this.setShowLoadingItem(true);
	}
		
	private void clearMessages(){
		this.messageMap.get(this.selectedList).clear();
		this.notifyDataSetChanged();
	}
	
	public void setShowLoadingItem(Boolean show){
		this.showLoadingItem = show;
	}
	
	public void setListView(ListView listView){
		this.listView = listView;
	}
}
