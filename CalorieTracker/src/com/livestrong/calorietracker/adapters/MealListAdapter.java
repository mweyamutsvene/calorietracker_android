package com.livestrong.calorietracker.adapters;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.livestrong.calorietracker.MyPlateApplication;
import com.livestrong.calorietracker.R;
import com.livestrong.calorietracker.back.db.ModelManager;
import com.livestrong.calorietracker.back.db.gen.DiaryEntry.DiaryEntryCategoryEnum;

import com.livestrong.calorietracker.back.db.gen.Meal;
import com.livestrong.calorietracker.back.db.gen.MealItem;
import com.livestrong.calorietracker.utilities.ImageLoader;


public class MealListAdapter extends BaseAdapter {
	
	private Activity activity;
	private Meal meal;
	private List<MealItem> mealItems;
	public ImageLoader imageLoader;
    private DiaryEntryCategoryEnum timeOfDay;

    public MealListAdapter(Activity a, Meal meal,DiaryEntryCategoryEnum timeOfDay) {
		this.activity = a;
		Meal newMeal = Meal.getMeal(meal.getRemoteId(),ModelManager.daoSession.getMealDao());
		this.mealItems = new ArrayList<MealItem>( (Collection<MealItem>) newMeal.getMealItemList());
		this.imageLoader = new ImageLoader(this.activity);
        this.timeOfDay = timeOfDay;
	}
		
	@Override
	public int getCount() {
		return (mealItems == null) ? 0 : mealItems.size();
	}

	@Override
	public Object getItem(int position) {
		return (mealItems == null) ? 0 : mealItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Context context = MyPlateApplication.getContext();
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.list_item_food, null);
		}
		
		// Retrieve food at position
		MealItem item = (MealItem) getItem(position);
		
		TextView foodNameTextField = (TextView)convertView.findViewById(R.id.foodNameTextView);
		foodNameTextField.setText(item.getTitle());
		
		TextView foodDescriptionTextField = (TextView)convertView.findViewById(R.id.foodDescriptionTextView);
		foodDescriptionTextField.setText(item.getDescription());
		
		ImageView imageView = (ImageView)convertView.findViewById(R.id.foodImageView);
		Log.d("D", "imageName: " + item.getSmallImage());
		
		if (timeOfDay == DiaryEntryCategoryEnum.CTDiaryEntryTypeBreakfast){
			imageView.setImageResource(R.drawable.icon_breakfast);
		} else if (timeOfDay == DiaryEntryCategoryEnum.CTDiaryEntryTypeLunch){
			imageView.setImageResource(R.drawable.icon_lunch);
		} else if (timeOfDay == DiaryEntryCategoryEnum.CTDiaryEntryTypeDinner){
			imageView.setImageResource(R.drawable.icon_dinner);
		} else {
			imageView.setImageResource(R.drawable.icon_snacks);
		}
		
		this.imageLoader.DisplayImage(item.getSmallImage(), imageView);
		
		return convertView;
	}
}
