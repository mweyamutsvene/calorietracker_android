package com.livestrong.calorietracker.adapters;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.livestrong.calorietracker.MyPlateApplication;
import com.livestrong.calorietracker.R;
import com.livestrong.calorietracker.back.api.CTWebserviceRequest;
import com.livestrong.calorietracker.back.db.DataHelperDelegate;
import com.livestrong.calorietracker.back.db.ModelManager;
import com.livestrong.calorietracker.back.db.gen.DiaryEntry;
import com.livestrong.calorietracker.back.db.gen.Exercise;
import com.livestrong.calorietracker.back.db.sync.ModelSyncCompletedDelegate;
import com.livestrong.calorietracker.utilities.ImageLoader;


public class ExerciseSelectorAdapter extends BaseAdapter implements ModelSyncCompletedDelegate, DataHelperDelegate {
    private Activity activity;
	private List<Exercise> exercises = new ArrayList<Exercise>();
	private ImageLoader imageLoader;
	private Boolean isLoading = false;
	private Boolean showSearchOnlinePromp = false;
	public Boolean showNoResultsMessage = false;
    private DiaryEntry.DiaryEntryCategoryEnum timeOfDay;
    private String mSearchString;

    public ExerciseSelectorAdapter(Activity activity, List<Exercise> exercises) {
		this.activity = activity;
		this.exercises = exercises;
		this.imageLoader = new ImageLoader(this.activity);
        this.timeOfDay = DiaryEntry.DiaryEntryCategoryEnum.CTDiaryEntryTypeExercise;
	}
	
	/**** Load list data functions ****/
	
	public void loadRecentlyPerformed(){
		this.isLoading = true;
		this.showSearchOnlinePromp = false;
		this.showNoResultsMessage = false;
        this.setExercises(null);

        ModelManager.getRecentExercises(this.timeOfDay,this);
	}
	
	public void loadFrequentlyPerformed(){
		this.isLoading = true;
		this.showSearchOnlinePromp = false;
		this.showNoResultsMessage = false;
        this.setExercises(null);

        ModelManager.getFavoriteExercises(this.timeOfDay,this);

	}
	
	public void loadCustomExercises(){
		this.isLoading = true;
		this.showSearchOnlinePromp = false;
		this.showNoResultsMessage = false;
        this.setExercises(null);

        ModelManager.getCustomExercises(this);
	}
	
	public void loadExercisesFromLocalSearch(String searchStr){
        this.isLoading = true;
        this.showSearchOnlinePromp = true;
		this.showNoResultsMessage = false;
        this.setExercises(null);
        this.mSearchString = searchStr;
        ModelManager.getExercisesLike(searchStr,this);
	}
	
	public void loadExercisesFromServerSearch(String searchStr){
        this.isLoading = true;
        this.showSearchOnlinePromp = false;
		this.showNoResultsMessage = false;
        this.mSearchString = searchStr;
		this.setExercises(null);
        ModelManager.sharedModelManager().getExercisesLikeFromServer(searchStr,this);

    }

	@Override
	public int getCount() {
		if (this.isLoading){
			return 1;
		}

		int count = 0;
		if (this.exercises != null){
			count = this.exercises.size();
		}

		if (this.showSearchOnlinePromp){
			count++;
		}
		return count;
	}

	@Override
	public Object getItem(int position) {
		if (this.isShowingSearchOnlinePrompt() && position >= this.exercises.size()){
			return null;
		}
		
		return (this.exercises == null) ? 0 : this.exercises.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Context context = MyPlateApplication.getContext();
		
		if (this.isLoading){
			convertView = LayoutInflater.from(context).inflate(R.layout.list_item_loading, null);
			return convertView;
		} else if (this.showSearchOnlinePromp && position == this.exercises.size()){
			convertView = LayoutInflater.from(context).inflate(R.layout.list_item_message, null);
			TextView message = (TextView) convertView.findViewById(R.id.messageTextView);
			if (CTWebserviceRequest.isOnline(MyPlateApplication.getContext())){
				message.setText("Tap to search online...");	
			} else {
				message.setText("Offline - only previously tracked items available.");
			}
			return convertView;
		} else if (this.showNoResultsMessage && position == this.exercises.size()){
			convertView = LayoutInflater.from(context).inflate(R.layout.list_item_message, null);
			((TextView)convertView.findViewById(R.id.messageTextView)).setText("No Results Found.");
			return convertView;
		}
		
		
		if (convertView == null || convertView.getId() != R.id.listItemFood) {
			convertView = LayoutInflater.from(context).inflate(R.layout.list_item_food, null);			
		}
		
		// Retrieve exercise at position
		Exercise exercise = (Exercise) getItem(position);
		if (exercise != null){
			TextView exerciseNameTextField = (TextView)convertView.findViewById(R.id.foodNameTextView);
			exerciseNameTextField.setText(exercise.getTitle());
			
			TextView exerciseDescriptionTextField = (TextView)convertView.findViewById(R.id.foodDescriptionTextView);
			exerciseDescriptionTextField.setText(exercise.getDescription());
	
			ImageView imageView = (ImageView)convertView.findViewById(R.id.foodImageView);
			imageView.setImageResource(R.drawable.icon_fitness);
			this.imageLoader.DisplayImage(exercise.getSmallImage(), imageView);
		} 
		
		return convertView;
	}
	
	public void setExercises(List<Exercise> exercises){
		this.exercises = exercises;
		notifyDataSetChanged();
	}
	
	public Boolean isShowingSearchOnlinePrompt(){
		return this.showSearchOnlinePromp;
	}
	
	public Boolean isLoading(){
		return this.isLoading;
	}

    @Override
    public void finishedSyncThreaded(ModelSyncType syncType, Object data) {
        this.isLoading = false;
        this.showNoResultsMessage = false;
        this.showSearchOnlinePromp = false;

        List<Exercise> likeExercise = (List<Exercise>)data;
        this.setExercises(likeExercise);
        if (likeExercise.size() == 0){
            this.showNoResultsMessage = true;
        }
        this.notifyDataSetChanged();

    }

    @Override
    public boolean errorOccurredThreaded(ModelSyncType syncType, Exception error, String errorMessage) {
        this.isLoading = false;
        this.notifyDataSetChanged();

        return false;
    }

    @Override
    public void dataReceivedThreaded(Method methodCalled, Object data) {
        this.isLoading = false;
        this.showSearchOnlinePromp = false;
        this.showNoResultsMessage = false;
        List<Exercise> exercises = (List<Exercise>)data;
        if (exercises.size() == 0 ){
            if(this.mSearchString != null && this.mSearchString.length() > 0){
                this.showSearchOnlinePromp = true;
            }
            else{
                this.showNoResultsMessage = true;
            }

        }
        this.setExercises(exercises);
    }

    @Override
    public boolean errorOccurredThreaded(Method methodCalled, Exception error, String errorMessage) {
        this.isLoading = false;
        this.setExercises(new ArrayList<Exercise>());
        this.notifyDataSetChanged();

        return false;
    }
}
