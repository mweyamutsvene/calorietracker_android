package com.livestrong.calorietracker.adapters;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.livestrong.calorietracker.MyPlateApplication;
import com.livestrong.calorietracker.R;
import com.livestrong.calorietracker.back.db.LiveStrongDisplayableListItem;
import com.livestrong.calorietracker.back.db.ModelManager;
import com.livestrong.calorietracker.back.db.gen.DiaryEntry;
import com.livestrong.calorietracker.utilities.ImageLoader;
import com.livestrong.calorietracker.utilities.PinnedHeaderListView;
import com.livestrong.calorietracker.utilities.PinnedHeaderListView.*;


public class DiaryAdapter extends BaseAdapter implements SectionIndexer, OnScrollListener,PinnedHeaderAdapter {
		
	private List<LiveStrongDisplayableListItem> mEntries = new ArrayList<LiveStrongDisplayableListItem>();
	private List<Integer> mEntrySection = new ArrayList<Integer>();
	private String[] mSections = new String[]{"Breakfast", "Lunch", "Dinner", "Snack", "Exercise", "Water", "Weight", "Net Calories"};
	private List<Integer> mSectionCalorieCounts = new ArrayList<Integer>();
	private Date mDate;
    private Activity mActivity;
	private List<DiaryEntry> mDiaryEntries;
	private ImageLoader imageLoader;
	
	// cached values for performance improvement
	private int[] mPositionForSection;
	private int[] mPositionForNextSection;
	
	public DiaryAdapter(Activity activity) {
		this.mActivity = activity;
		this.imageLoader = new ImageLoader(activity);
	}

	private void loadDiaryEntry(){
		this.mEntries.clear();
		this.mEntrySection.clear();
		this.mSectionCalorieCounts.clear();
		
		// TODO This can be made async by sending this as the 2nd argument
		this.mDiaryEntries = ModelManager.getDiaryEntriesForDay(this.mDate);

		// Get Food entries
        List<DiaryEntry> foodEntries = new ArrayList<DiaryEntry>();
        List<DiaryEntry> exerciseEntries = new ArrayList<DiaryEntry>();
        List<DiaryEntry> weightEntries = new ArrayList<DiaryEntry>();
        List<DiaryEntry> waterEntries = new ArrayList<DiaryEntry>();
        List<DiaryEntry> breakfastEntries =  new ArrayList<DiaryEntry>();
        List<DiaryEntry> lunchEntries =  new ArrayList<DiaryEntry>();
        List<DiaryEntry> dinnerEntries =  new ArrayList<DiaryEntry>();
        List<DiaryEntry> snackEntries =  new ArrayList<DiaryEntry>();

        for(DiaryEntry entry : mDiaryEntries){
            switch(DiaryEntry.DiaryEntryType.values()[entry.getDiaryType()]){
                case DIARY_FOOD_ENTRY:{
                    foodEntries.add(entry);
                    switch (DiaryEntry.DiaryEntryCategoryEnum.values()[entry.getCategory()]){
                        case CTDiaryEntryTypeBreakfast:{
                            breakfastEntries.add(entry);
                            break;
                        }
                        case CTDiaryEntryTypeLunch:{
                            lunchEntries.add(entry);
                            break;
                        }
                        case CTDiaryEntryTypeDinner:{
                            dinnerEntries.add(entry);
                            break;
                        }
                        case CTDiaryEntryTypeSnacks:{
                            snackEntries.add(entry);
                            break;
                        }
                    }
                    break;
                }
                case DIARY_EXERCISE_ENTRY:{
                    exerciseEntries.add(entry);
                    break;
                }
                case DIARY_WATER_ENTRY:{
                    if(entry.getOuncesConsumed()>0){
                        waterEntries.add(entry);
                    }
                    break;
                }
                case DIARY_WEIGHT_ENTRY:{
                    weightEntries.add(entry);
                    break;
                }
            }
        }

		// Add Breakfast entries
		Integer count = 0;
		for (DiaryEntry foodDiaryEntry : breakfastEntries) {
			this.mEntries.add(foodDiaryEntry);
			this.mEntrySection.add(0);
			count += foodDiaryEntry.getCalories().intValue();
		}
		this.mSectionCalorieCounts.add(0, count);
		
		// Add Lunch entries
		count = 0;
		for (DiaryEntry foodDiaryEntry : lunchEntries) {
			this.mEntries.add(foodDiaryEntry);
			this.mEntrySection.add(1);
			count += foodDiaryEntry.getCalories().intValue();
		}
		this.mSectionCalorieCounts.add(1, count);
		
		// Add Dinner entries
		count = 0;
		for (DiaryEntry foodDiaryEntry : dinnerEntries) {
			this.mEntries.add(foodDiaryEntry);
			this.mEntrySection.add(2);
			count += foodDiaryEntry.getCalories().intValue();
		}
		this.mSectionCalorieCounts.add(2, count);
		
		// Add Snack entries
		count = 0;
		for (DiaryEntry foodDiaryEntry : snackEntries) {
			this.mEntries.add(foodDiaryEntry);
			this.mEntrySection.add(3);
			count += foodDiaryEntry.getCalories().intValue();
		}
		this.mSectionCalorieCounts.add(3, count);
		
		// Add Exercise Entries
		count = 0;
		for (DiaryEntry exerciseDiaryEntry: exerciseEntries) {
			this.mEntries.add(exerciseDiaryEntry);
			this.mEntrySection.add(4);
			count += exerciseDiaryEntry.getCalories().intValue();
		}
		this.mSectionCalorieCounts.add(4, count);

        // Add Water Entry
        if(waterEntries.size() > 0){
            DiaryEntry waterDiaryEntry = waterEntries.get(0);
            if (waterDiaryEntry != null) {
                this.mEntries.add(waterDiaryEntry);
                this.mEntrySection.add(5);
            }
        }
		this.mSectionCalorieCounts.add(5, null);

        // Add Weight Entry
        if(weightEntries.size() > 0){
            DiaryEntry weightDiaryEntry = weightEntries.get(0);
            if (weightDiaryEntry != null) {
                this.mEntries.add(weightDiaryEntry);
                this.mEntrySection.add(6);
            }
        }
		this.mSectionCalorieCounts.add(6, null);

        // Add entry for Net Calories entry
        if (this.mEntrySection.size() > 0){
            this.mEntrySection.add(7);
        }

        Map<DiaryEntry.DiaryEntryCategoryEnum, Float>diaryEntry = ModelManager.getDiarySummaryPerType(mDate);
        Float breakfastCalories = diaryEntry.get(DiaryEntry.DiaryEntryCategoryEnum.CTDiaryEntryTypeBreakfast);
        Float lunchCalories = diaryEntry.get(DiaryEntry.DiaryEntryCategoryEnum.CTDiaryEntryTypeLunch);
        Float dinnerCalories = diaryEntry.get(DiaryEntry.DiaryEntryCategoryEnum.CTDiaryEntryTypeDinner);
        Float snacksCalories = diaryEntry.get(DiaryEntry.DiaryEntryCategoryEnum.CTDiaryEntryTypeSnacks);
        Float exerciseCalories = diaryEntry.get(DiaryEntry.DiaryEntryCategoryEnum.CTDiaryEntryTypeExercise);

        this.mSectionCalorieCounts.add(7,  (int)(breakfastCalories + lunchCalories + dinnerCalories + snacksCalories + exerciseCalories));

        this.loadPositionForSectionArray();
        this.loadPositionForNextSectionArray();

    }


	
	public void setDate(Date date){
		this.mDate = date;
		this.loadDiaryEntry();
		notifyDataSetChanged();			
	}
	
	@Override
	public int getCount() {
		return (this.mEntrySection == null) ? 0 : mEntrySection.size();
	}

	@Override
	public Object getItem(int position) {
		if (position >= 0 && position < mEntries.size()){
			return mEntries.get(position);	
		} else {
			return null;
		}
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Context context = MyPlateApplication.getContext();
		
		LiveStrongDisplayableListItem entryObject = (LiveStrongDisplayableListItem)getItem(position);
		
		if (entryObject != null){
			if (convertView == null || convertView.getId() != R.id.listItemDiary){
				convertView = LayoutInflater.from(context).inflate(R.layout.list_item_diary, null);				
			}
			TextView nameLabel = (TextView)convertView.findViewById(R.id.foodNameTextView);
			TextView descriptionLabel = (TextView)convertView.findViewById(R.id.foodDescriptionTextView);	
			nameLabel.setText(entryObject.getTitle());
			descriptionLabel.setText(entryObject.getDescription());
			
			ImageView imageView = (ImageView) convertView.findViewById(R.id.foodImageView);
			
			switch (getSectionForPosition(position)) {
				case 0:
					imageView.setImageResource(R.drawable.icon_breakfast);
					break;
				case 1:
					imageView.setImageResource(R.drawable.icon_lunch);				
					break;
				case 2:
					imageView.setImageResource(R.drawable.icon_dinner);
					break;
				case 3:
					imageView.setImageResource(R.drawable.icon_snacks);
					break;
				case 4:
					imageView.setImageResource(R.drawable.icon_fitness);
					break;
				case 5:
					imageView.setImageResource(R.drawable.icon_water);
					break;
				case 6:
					imageView.setImageResource(R.drawable.icon_weight);
					break;
			}
			
			imageView.setTag(entryObject.getSmallImage());
			this.imageLoader.DisplayImage(entryObject.getSmallImage(), imageView);
						
			RelativeLayout layout = (RelativeLayout) convertView.findViewById(R.id.listItemFood);
			layout.setBackgroundResource(R.drawable.list_item_diary_background_selector);
		} else {
			convertView = LayoutInflater.from(context).inflate(R.layout.list_item_diary_totals, null);

            Map<DiaryEntry.DiaryEntryCategoryEnum, Float>diaryEntry = ModelManager.getDiarySummaryPerType(this.mDate);
            Float breakfastCalories = diaryEntry.get(DiaryEntry.DiaryEntryCategoryEnum.CTDiaryEntryTypeBreakfast);
            Float lunchCalories = diaryEntry.get(DiaryEntry.DiaryEntryCategoryEnum.CTDiaryEntryTypeLunch);
            Float dinnerCalories = diaryEntry.get(DiaryEntry.DiaryEntryCategoryEnum.CTDiaryEntryTypeDinner);
            Float snacksCalories = diaryEntry.get(DiaryEntry.DiaryEntryCategoryEnum.CTDiaryEntryTypeSnacks);
            Float exerciseCalories = diaryEntry.get(DiaryEntry.DiaryEntryCategoryEnum.CTDiaryEntryTypeExercise);

            int currentCalorieIntake = (int)( breakfastCalories + lunchCalories + dinnerCalories + snacksCalories + exerciseCalories);

			int caloriesConsumed = (int)( breakfastCalories + lunchCalories + dinnerCalories + snacksCalories);// this.mDiaryEntry.getCaloriesConsumed(this.mDate);
			int caloriesBurned = (exerciseCalories.intValue());//this.mDiaryEntry.getCaloriesBurned(this.mDate);
			int netCalories = (int)( breakfastCalories + lunchCalories + dinnerCalories + snacksCalories+exerciseCalories);//this.mDiaryEntry.getNetCalories(this.mDate);
			
			TextView caloriesConsummedTextView = (TextView) convertView.findViewById(R.id.caloriesConsumed);
			TextView caloriesBurnedTextView = (TextView) convertView.findViewById(R.id.caloriesBurned);
			TextView netCaloriesTextView = (TextView) convertView.findViewById(R.id.netCalories);
			
			caloriesConsummedTextView.setText(caloriesConsumed + "");
			caloriesBurnedTextView.setText(caloriesBurned + "");
			netCaloriesTextView.setText(netCalories + "");
		}
		
		bindSectionHeader(convertView, position);
		
		return convertView;
	}
	
    private void bindSectionHeader(View itemView, int position) {
    	final View headerView = itemView.findViewById(R.id.listItemDiaryHeader);
    	final View dividerView = itemView.findViewById(R.id.list_divider);
    	   	
		final int section = getSectionForPosition(position);
        if (getPositionForSection(section) == position) {
        	this.configurePinnedHeader(itemView, position, 1); // layout content of header
            headerView.setVisibility(View.VISIBLE);
	    	dividerView.setVisibility(View.GONE);
        } else {
        	headerView.setVisibility(View.GONE);
	    	dividerView.setVisibility(View.VISIBLE);
        }

        // remove the divider for the last item in a section
        if (getPositionForNextSection(section) - 1 == position) {
        	dividerView.setVisibility(View.GONE);
        } else {
	    	dividerView.setVisibility(View.VISIBLE);
        }
    }
    
    private void loadPositionForSectionArray(){
    	this.mPositionForSection = new int[this.mSections.length];
    	for (int sectionIndex = 0; sectionIndex < this.mSections.length; sectionIndex++){
    		this.mPositionForSection[sectionIndex] = -1; // Initialize entry
    		
    		for (int i = 0; i < this.mEntrySection.size(); i++){ // Search for for entry with current sectionIndex
    			if (this.mEntrySection.get(i) == sectionIndex){ 
    				this.mPositionForSection[sectionIndex] = i;
    				break;
    			}
    		}    		
    	}
    }
    
	public int getPositionForSection(int sectionIndex) {
		return this.mPositionForSection[sectionIndex];
    }
	
	private void loadPositionForNextSectionArray(){
		this.mPositionForNextSection = new int[this.mSections.length];
    	for (int sectionIndex = 0; sectionIndex < this.mSections.length; sectionIndex++){
    		this.mPositionForNextSection[sectionIndex] = -1; // Initialize entry
    		
    		int nextSectionIndex = sectionIndex + 1;
    		
    		while (nextSectionIndex < this.mSections.length){ // Make sure nextSectionIndex has elements in list, if not get next section
    			if (this.mEntrySection.contains(nextSectionIndex)){
    				break;
    			} 
    			nextSectionIndex++;
    		}
    		    		
    		for (int i = 0; i < this.mEntrySection.size(); i++){ // Search for for entry with nextSectionIndex
    			if (this.mEntrySection.get(i) == nextSectionIndex){
    				this.mPositionForNextSection[sectionIndex] = i;
    				break;
    			}
    		}
    		 		
    	}
	}
	
	public int getPositionForNextSection(int sectionIndex) {
		return this.mPositionForNextSection[sectionIndex];
	}
	
	public int getSectionForPosition(int position) {
		if (position < 0 || position >= this.mEntrySection.size()){
			return -1;
		}
		
		return this.mEntrySection.get(position);
	}
	
	@Override
	public Object[] getSections() {
		return this.mSections;
	}
      
	@Override
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (view instanceof PinnedHeaderListView) {
            ((PinnedHeaderListView) view).configureHeaderView(firstVisibleItem);
        }		
	}

	@Override
	public void onScrollStateChanged(AbsListView arg0, int arg1) {
	}

	@Override
	public int getPinnedHeaderState(int position) {
        if (getCount() == 0) {
            return PINNED_HEADER_GONE;
        }

        if (position < 0) {
            return PINNED_HEADER_GONE;
        }

        // The header should get pushed up if the top item shown
        // is the last item in a section for a particular letter.
        int section = getSectionForPosition(position);
        int nextSectionPosition = getPositionForNextSection(section);
        
        if (nextSectionPosition != -1 && position == nextSectionPosition - 1) {
            return PINNED_HEADER_PUSHED_UP;
        }

        return PINNED_HEADER_VISIBLE;
	}

	@Override
	public void configurePinnedHeader(View v, int position, int alpha) {
		TextView sectionLabel = (TextView)v.findViewById(R.id.sectionHeaderTextView);
		TextView caloriesTextView = (TextView)v.findViewById(R.id.caloriesTextView);
		TextView caloriesLabel = (TextView)v.findViewById(R.id.caloriesLabel);
		
		final int section = getSectionForPosition(position);
		if (section >= 0 && section < this.mSections.length){
			final String title = (String) getSections()[section];
			sectionLabel.setText(title);
			Integer caloriesCount = this.mSectionCalorieCounts.get(section);
			if (caloriesCount != null){
				caloriesTextView.setVisibility(View.VISIBLE);
				caloriesTextView.setText(caloriesCount + "");
				caloriesLabel.setVisibility(View.VISIBLE);
			} else {
				caloriesTextView.setVisibility(View.INVISIBLE);
				caloriesLabel.setVisibility(View.INVISIBLE);
			}
		}
	}	
}
