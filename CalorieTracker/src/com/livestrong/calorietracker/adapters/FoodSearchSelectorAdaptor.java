package com.livestrong.calorietracker.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.livestrong.calorietracker.MyPlateApplication;
import com.livestrong.calorietracker.R;
import com.livestrong.calorietracker.back.db.ModelManager;
import com.livestrong.calorietracker.back.db.gen.DiaryEntry;
import com.livestrong.calorietracker.utilities.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thomashanks on 7/18/13.
 */
public class FoodSearchSelectorAdaptor extends BaseAdapter{


    private Activity activity;
    private List<String> listItems = new ArrayList<String>();
    private ImageLoader imageLoader;
    private DiaryEntry.DiaryEntryCategoryEnum timeOfDay;
    private Boolean isLoading = false;
    private Boolean showSearchOnlinePromp = false;
    public Boolean showNoResultsMessage = false;
    private String mSearchString;

    public FoodSearchSelectorAdaptor(Activity activity, DiaryEntry.DiaryEntryCategoryEnum timeOfDay) {
        this.activity = activity;
        this.imageLoader = new ImageLoader(this.activity);
        this.timeOfDay = timeOfDay;

        this.isLoading = true;
        this.showSearchOnlinePromp = false;
        this.showNoResultsMessage = false;
        this.setListItems(null);
    }

    /**** Load list data functions ****/


    public void loadFoodFromLocalSearch(String searchStr){
        mSearchString = searchStr;
        this.isLoading = true;
        this.showSearchOnlinePromp = true;
        this.showNoResultsMessage = false;
        List<String> localDatabase = ModelManager.loadSearchForFood(searchStr);

        List<String> items = new ArrayList<String>();
        for(int i = 0; i < localDatabase.size(); i++){
            String foodname = localDatabase.get(i);
            boolean foodExists = false;
            for(String existingFoodName: items){
                if(existingFoodName.toLowerCase().equals(foodname.toLowerCase())){
                   foodExists = true;
                    break;
                }


            }
            if(!foodExists){
                items.add(foodname);

            }
            if(items.size() >= 5)break;
        }
        if(items.size() < 5){
            List<String> localTxt = ModelManager.consumeLocalFoodNames(searchStr);
            for(int i = items.size(); i < localTxt.size(); i++){

                String foodname = localTxt.get(i);
                boolean foodExists = false;
                for(String existingFoodName: items){
                    if(existingFoodName.toLowerCase().equals(foodname.toLowerCase())){
                        foodExists = true;
                        break;
                    }
                }
                if(!foodExists){
                    items.add(foodname);
                }
                if(items.size() >= 5)break;

            }
        }
        this.setListItems(items);

    }



    @Override
    public int getCount() {
        if(listItems == null){
            return 0;
        }

        return this.listItems.size();
    }

    @Override
    public Object getItem(int position) {
        return (this.listItems == null) ? 0 : this.listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Context context = MyPlateApplication.getContext();

        if (this.showNoResultsMessage && position == this.listItems.size()){
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item_message, null);
            ((TextView)convertView.findViewById(R.id.messageTextView)).setText("No Results Found.");
            return convertView;
        }

        if (convertView == null || convertView.getId() != R.id.listItemFood) {
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item_search, null);
        }

        // Retrieve food at position
        String name = (String)getItem(position);

        if (name != null){
            TextView foodNameTextField = (TextView)convertView.findViewById(R.id.foodNameTextView);
            foodNameTextField.setText(name);
        }

        return convertView;
    }

    @SuppressWarnings("unchecked")
    public void setListItems(List<?> listItems){
        this.listItems = (List<String>) listItems;
        notifyDataSetChanged();
    }

    public void setTimeOfDay(DiaryEntry.DiaryEntryCategoryEnum timeOfDay){
        this.timeOfDay = timeOfDay;
    }

    public Boolean isShowingSearchOnlinePrompt(){
        return this.showSearchOnlinePromp;
    }

    public Boolean isLoading(){
        return this.isLoading;
    }




}