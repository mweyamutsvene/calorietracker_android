package com.calorietracker.daogenerator;


import java.util.Date;
import java.util.List;
import java.util.Map;

import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;
import de.greenrobot.daogenerator.ToMany;

/**
 * Version 1 of the Schema definition
 * 
 * @author Tommy
 */
public class Version1 extends SchemaVersion {
 
	public static Schema schema;
    /**
     * Constructor
     * 
     * @param current
     */
    public Version1(boolean current) {
        super(current);
         
        schema = getSchema();

        Entity diaryEntity = addDiaryEntity();
        
        Entity waterDiaryEntity = addWaterDiaryEntity(diaryEntity);
       
        Entity weightDiaryEntity = addWeightDiaryEntity(diaryEntity);
           
        Entity exerciseDiaryEntity = addExerciesDiaryEntity(diaryEntity);
        
        Entity foodDiaryEntity = addFoodDiaryEntity(diaryEntity);
       
        Entity profileEntity = addProfileEntity(schema);

        addCommunityMessage(schema);
        
    }
 
    /**
     * {@inheritDoc}
     */
    @Override
    public int getVersionNumber() {
        return 1;
    }

    private static Entity addProfileEntity(Schema schema) {
        Entity profile = schema.addEntity("Profile");
        profile.addStringProperty("username");
        profile.addDoubleProperty("height").notNull();
        profile.addDoubleProperty("weight").notNull();
        profile.addDoubleProperty("activityLevel").notNull();
        profile.addDoubleProperty("calories").notNull();
        profile.addDoubleProperty("goal").notNull();
        profile.addIntProperty("age").notNull();
        profile.addIntProperty("gender").notNull();
        profile.addIntProperty("mode").notNull();
        profile.addBooleanProperty("isSynchronized").notNull();
        profile.addIdProperty().autoincrement();
      

         
        return profile;
    }
    private static Entity addCommunityComment(Schema schema) {
        Entity message = schema.addEntity("CommunityMessageComment");
        message.addIntProperty("dareCommentId").notNull();
        message.addStringProperty("post");
        message.addDateProperty("dateCreated");
        message.addIntProperty("comments").notNull();
        message.addStringProperty("image");
        message.addIdProperty().autoincrement();
        return message;
    }
    private static Entity addCommunityMessage(Schema schema) {
        Entity message = schema.addEntity("CommunityMessage");
        message.addIntProperty("postId").notNull();
        message.addStringProperty("post");
        message.addStringProperty("author");
        message.addStringProperty("summary");
        message.addStringProperty("thumbnailURL");
        message.addDateProperty("dateCreated");
        message.addDateProperty("dateUpdated");
        message.addIntProperty("numberOfComments").notNull();
        message.addIdProperty().autoincrement();

               
        return message;
    }
   
    private static Entity addExerciseEntity(Schema schema) {
        Entity exercise = schema.addEntity("Exercise");
        exercise.addFloatProperty("caloriesPerHour");
        exercise.addFloatProperty("caloriesBurned");
        exercise.addDoubleProperty("calFactor");
        exercise.addDateProperty("dateCreated");
        exercise.addDateProperty("dateDeleted");
        exercise.addBooleanProperty("fullyPopulated");
        exercise.addStringProperty("imageUrl");
        exercise.addStringProperty("name");
        exercise.addStringProperty("remoteId");
        exercise.addBooleanProperty("requiresDistance");
        exercise.addIdProperty().autoincrement();
        exercise.implementsInterface("GenericModelObject");
        exercise.implementsInterface("LiveStrongDisplayableListItem");
        exercise.implementsInterface("Serializable");

        //Add to-one 
         
        return exercise;
    }
    private static Entity addFoodEntity() {
        Entity food = schema.addEntity("Food");
        
        food.addIdProperty().autoincrement();
        food.addStringProperty("brand");
        food.addIntProperty("calories");
        food.addIntProperty("caloriesFromFat");
        food.addFloatProperty("carbs");
        food.addFloatProperty("cholesterol");
        food.addDateProperty("dateCreated");
        food.addDateProperty("dateDeleted");
        food.addDateProperty("dateModified");
        food.addFloatProperty("dietaryFiber");
        food.addFloatProperty("fat");
        food.addBooleanProperty("fullyPopulated");
        food.addStringProperty("imageUrl");
        food.addStringProperty("name");
        food.addFloatProperty("percentCaloriesFromCarbs");
        food.addFloatProperty("percentCaloriesFromFat");
        food.addFloatProperty("percentCaloriesFromProtein");
        food.addFloatProperty("protein");
        food.addStringProperty("remoteId");
        food.addFloatProperty("saturatedFat");
        food.addStringProperty("servingSize");
        food.addFloatProperty("sodium");
        food.addFloatProperty("sugars");
        food.addBooleanProperty("verified");
        food.implementsInterface("GenericModelObject");
        food.implementsInterface("LiveStrongDisplayableListItem");
        food.implementsInterface("Serializable");
        return food;
    }
    
    private static Entity addMealEntity(Entity food) {
        //Meal (1:M) MealFood (M:1) Food

    	//Create Meal entity
        Entity meal = schema.addEntity("Meal");
        meal.addIdProperty().autoincrement();
        meal.addIntProperty("calories");
        meal.addDateProperty("dateCreated");
        meal.addDateProperty("dateDeleted");
        meal.addDateProperty("dateModified");
        meal.addStringProperty("name");
        meal.addStringProperty("remoteId");
        meal.implementsInterface("GenericModelObject");
        meal.implementsInterface("LiveStrongDisplayableListItem");
        meal.implementsInterface("Serializable");

        Entity mealItem = schema.addEntity("MealItem");
        mealItem.addIdProperty().autoincrement();
        mealItem.addFloatProperty("servings");
        mealItem.implementsInterface("LiveStrongDisplayableListItem");
        mealItem.implementsInterface("Serializable");

        Property foodId = mealItem.addLongProperty("foodId").getProperty();
        mealItem.addToOne(food, foodId);
       
        Property mealId = mealItem.addLongProperty("mealId").getProperty();
        mealItem.addToOne(meal, mealId);

        Property itemId = mealItem.addLongProperty("mealItemId").getProperty();
        meal.addToMany(mealItem, itemId);
        food.addToMany(mealItem, itemId);

        return meal;
    }
    
    private static Entity addDiaryEntity() {
        Entity diary = schema.addEntity("DiaryEntry");
        diary.addIntProperty("diaryType");
        diary.addFloatProperty("calories");
        diary.addIntProperty("category");
        diary.addStringProperty("comments");
        diary.addDateProperty("dateCreated");
        diary.addDateProperty("dateDeleted");
        diary.addDateProperty("dateModified");
        diary.addDateProperty("entryDate");
        diary.addIntProperty("entryDay");
        diary.addBooleanProperty("isSynchronized");
        diary.addStringProperty("remoteId");       
        diary.addIdProperty().autoincrement();
        diary.implementsInterface("GenericModelObject");
        return diary;
    }
    private static Entity addWaterDiaryEntity(Entity diary) {
        diary.addFloatProperty("ouncesConsumed");
        
        return diary;
    }
    
    private static Entity addWeightDiaryEntity(Entity diary) {
        diary.addFloatProperty("calorieGoal");
        diary.addFloatProperty("weight");
        return diary;
    }
    
    private static Entity addExerciesDiaryEntity(Entity diary) {
        diary.addDoubleProperty("duration");
    	
        Entity exercise = addExerciseEntity(schema);
        Property exerciseId = diary.addLongProperty("exerciseId").notNull().getProperty();
        diary.addToOne(exercise,exerciseId);

        return diary;
    }
    
    private static Entity addFoodDiaryEntity(Entity diary) {
    	diary.addFloatProperty("carbsConsumed");
    	diary.addFloatProperty("fatConsumed");
    	diary.addFloatProperty("proteinConsumed");
    	diary.addFloatProperty("servings");
    	diary.addStringProperty("timePeriod");
    	 
    	Entity food = addFoodEntity();
        Property foodId = diary.addLongProperty("foodId").notNull().getProperty();
        diary.addToOne(food, foodId);
      
        List<Property> diaryProperties = diary.getProperties();
        
        
        ToMany diaryEntities = food.addToMany(diary, diaryProperties.get(11));
        diaryEntities.setName("foodDiaryEntries");
        
    	addMealEntity(food);
    	return diary;
    }
}
